import React from "react";
import Header from "../components/Header.jsx";
import DropDownLinks from "../components/DropDownLinks.jsx";
import Footer from "../components/Footer.jsx";
import { linkClick } from "../actions/FixedHeaderAction.jsx";
import {connect} from 'react-redux';

@connect((store) =>{
    return{
        toggleBackgroundFilter: store.backgroundAction.toggleBackgroundFilter
    }
})

class Layout extends React.Component{
    constructor(props) {
        super(props);
        this.backgroundFilter = this.backgroundFilter.bind(this);
    }

    backgroundFilter() {
        let filter;
        if (this.props.toggleBackgroundFilter) {
            filter =
                <div className="backgroundFilter">
                    &nbsp;
            </div>;
        } else {
            filter = null;
        }
        return filter;
    }

    render() {
        let children = React.Children.map(this.props.children, (child)=>{
            return React.cloneElement(child, {})
        })
        return(
            <div className = "root-element">
                {this.backgroundFilter}
                <Header/>
                <DropDownLinks/>
                {children}
                <Footer/>
            </div>
        )
    }
}

module.exports = Layout
