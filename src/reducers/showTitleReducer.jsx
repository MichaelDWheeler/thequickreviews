export default function reducer(state={
    showTitle: false,
    currentLocation: document.location.href,
}, action, payload){
    switch(action.type){
        case "SHOW_TITLE":{
            return{...state, showTitle: true}
            break;
        }
    }
    switch(action.type){
        case "HIDE_TITLE":{
            return{...state, showTitle: false}
            break;
        }
    }
    switch(action.type){
        case "GET_LOCATION":{
            return{...state, currentLocation: action.payload}
            break;
        }
    }
    return state;
}
