import React from "react";
import MetaTags from 'react-meta-tags';

import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";
import JumpTo from "../../components/JumpTo.jsx";
import Video from "../../components/Video.jsx";

let url = "best-safety-razor",
title ="Best Safety Razors",
image = "../../js/img/bathroom.jpg",
mobileImage = "../js/img/bathroom.jpg",
parallaxAlt = "Bathroom where safety razor is used",
subject = "Safety Razor",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of Safety Razers We Compared</h3>
        <p>
            When we set out to find the best safety razor, we looked for safety razors that were built to last, simple to use, easy to maintain, extremely effective, and offered adequate protection against nicks and cuts. While this last criteria is subjective, we looked for safety razors that we felt were affordable as well. All of the safety razors we considered are made out of a metal, and not plastic. They are all reusable and not disposable. In terms of operation they functioned the same way a disposable blade did for the most part. Changing the blades on each of the items is as easy as unscrewing the handle from the head, separating the blade from the head piece, replacing the blade, and then putting it back together. The whole action could be carefully accomplished in under 30 seconds even when done carefully.
        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Do We Feel Makes The Best {subject}?</h3>
        <p>
            We believe that the best safety razor is one that positions the razor at the proper angle to easily cut through hair whether it is fine or course while offering protection against nicks and cuts. Safety razors serve a basic purpose which is to cut hair extremely close to the skin while offering protection against slicing deeply into the skin as a straight razor can. All of the models we considered do this easily by only allowing a portion of the blade to be exposed. Don't misunderstand, a safety razor is not entirely safe. They can still give you a nasty cut if you are not careful, but the best safety razor will minimize the risk when used properly. The length of the handle and the weight of the item played a role in helping us to determine what we felt was the best safety razor. This is because using a safety razor is often a daily occurance and ease of use is also determined by comfort. For facial use, safety razors that have smaller handles but decent weight scored higher because it was easier to navigate the contours of the face without having to apply pressure. For body use, safety razors with a longer handle length made reaching hard to reach areas easier. In all cases, we feel that the best safety razors use double edge blades and this is due to affordability. A double edge razor blade will get twice the use of a single edge. Though the double edge razor blades can cost more, the cost difference can be so minimal that it makes more sense to purchase double edge razor blades. 
        </p>
    </div>
],

item1Title = "VIKINGS BLADE The Godfather Safety Razor with 5 Swedish Platinum Super Blades and Gift Box",
item1Description = "The Godfather Safety Razor by VIKINGS BLADE is our top pick for the best safety razor. It is a heavy duty safety razor that is crafted from premium high end Swedish materials. It does not use any cheap plastics or pot metals and is built to last. For convenience, it comes with a luxurious travel case that is made from leatherette and suede as well as 5 platinum coated dual edge razor blades. The head of the GodFather is the traditional heavy duty head that includes an advanced open comb system to help prevent cuts from accidentally occurring while offering an extremely close shave. Although the handle is approximately 4.5 inches, long it is easy to hold to navigate the contours of the face, allows access to hard to reach body areas, and is heavy enough that extra pressure is not necessary to cut hair. The manufacturer offers a very solid warranty on this product which is an international lifetime warranty against manufacturer defects. This safety razor is great for both beginners and the advanced who are looking for an aggressive shave, simply choose the blade to determine your experience.",
href1 ="http://amzn.to/2pPedlo",
item1url = url + "/" + encodeURIComponent('#')+"image1",
item1ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019QLSRT4&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B019QLSRT4",
item1ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019QLSRT4&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item1ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019QLSRT4&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item1WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Extremely well built with high quality Swedish materials`,
    },
    {
        "id": "2",
        "positiveFeature": `Longer handle is easy to use on face and allows hard to reach areas on body`,
    },
    {
        "id": "6",
        "positiveFeature": `Advanced open comb design offers more protection than many other safety razors that do not include this feature`,
    },
    {
        "id": "3",
        "positiveFeature": `Travel case is included`,
    },
    {
        "id": "4",
        "positiveFeature": `5 platinum coated double edge blades are included`,
    },
    {
        "id": "5",
        "positiveFeature": `Makes a great gift and a gift box is included`,
    },

],

item2Title = "Merkur Heavy Duty Double Edge Razor (Blade Included)",
item2Description = "The Merkur Heavy Duty safety razor is beautifully crafted tool that is capable of giving an incredibly close shave. This safety razor is chrome plated zinc alloy (Zamak). Although it is not our top pick for the best safety razor it is a close second due to the durability, function, beauty, practicality, and ease of use. It is a heavier safety razor with a short handle that offers a comfortable grip when following the lines of the face. The handle is finely machine knurled which presents both beauty and function. The knurled grooves give a luxurious appearance, but better yet it provides a great grip for your fingertips when using in wet conditions. The weight of the safety razor is adequate so that you do not need to apply additional pressure when shaving with a sharp blade, a properly prepared beard, and shaving cream or shaving soap. As with our other top picks for the best safety razor, the aggressiveness of this safety razor can be tailored to each users needs simply by selecting the type of double edge blade used with it. When used properly, this safety razor can give an incredibly close, nick free experience resulting in extremely smooth skin. One double edge blade is included with this safety razor",
href2 ="http://amzn.to/2q1KN2O",
item2url = url + "/" + encodeURIComponent('#')+"image2",
item2ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000QYEK88&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B000QYEK88",
item2ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000QYEK88&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item2ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000QYEK88&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item2WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Fantastic look and design`,
    },
    {
        "id": "2",
        "positiveFeature": `Machine knurled handle gives additional grip which is especially important when using in wet conditions`,
    },
    {
        "id": "3",
        "positiveFeature": `Short and heavier handle makes handling easy and allows for nimble movements`,
    },
    {
        "id": "4",
        "positiveFeature": `Built to last - solid construction`,
    },
    {
        "id": "5",
        "positiveFeature": `2-piece design makes swapping blades easier than a 3-piece design, especially for those with shaky hands`,
    },


],

item3Title = "Smoothere Double Edge Safety Razor Kit for Men + 5 Premium Blades, Travel Case & Mirror.",
item3Description = "The Smoothere Double Edge Safety Razor is a quality safety razor constructed from chrome-plated zinc allow. This razor has been expertly weighted to provide the optimum balance to achieve the best shave possible. The handles of this safety razor is textured to provide good grip and to help prevent it from slipping away from you in wet conditions. The channels in the handle help to divert the water away from the surface as well as keeping the handle from becoming a smooth surface that offers poor grip. The head of this safety razor includes a micro comb that helps to prevent cuts and also helps to keep the blade from clogging as easily. As an added bonus, this kit comes with 5 premium stainless steel blades, a travel case, as well as a mirror. After using a safety razor, using a hotel's free razor doesn't ever seem to do the job as well. If you purchase a safety razor without a case, it makes transporting the razor inconvenient. You need to remove the blade for safety and then bring additional blades in a safe container so you do not cut yourself or anyone else. A travel case makes it extremely simple.",
href3 ="http://amzn.to/2p6Z1T5",
item3url = url + "/" + encodeURIComponent('#')+"image3",
item3ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B014VNN62U&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B014VNN62U",
item3ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B014VNN62U&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item3ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B014VNN62U&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item3WhatPeopleLike = [
    {
        "id": "2",
        "positiveFeature": `Manufacturer's life-time warranty`,
    },
    {
        "id": "1",
        "positiveFeature": `Chrome-plated brass and bronze alloy is unlikely to rust when used under normal conditions.`,
    },
    {
        "id": "3",
        "positiveFeature": `Texture handle provides grip in wet conditions`,
    },
    {
        "id": "4",
        "positiveFeature": `3-piece system makes cleaning individual parts simple and allows user to use after-market or different handles.`,
    },
    {
        "id": "5",
        "positiveFeature": `5 stainless steel dual edge blades, travel case, and mirror included`,
    },

],

item4Title = "Shaveology Double Edge Safety Razor Wet Shave Kit",
item4Description = "The Shaveology safety razor is designed with a luxury appearance and utilizes a dual edge razor blade. The head of the safety razor displays a crown image which is the branding, and the handle is a non-slip back acrylic handle that is weighted. This shaving kit comes with a chrome finished, closed comb, double edge safety razor which will not rust under normal use, 5 platinum steel razor blades, a leather blade guard for travel or home safety, a polishing towel to keep the appearance of the razor looking exceptional, and great high end packaging that helps to make this a great gift. The manufacturer of this safety razor supports a national non profit organization known as Soldier's Angels that provides help to the men and women of United States Army, Navy, Air Force, Marines, Coast Guard and veteran population.",
href4 ="http://amzn.to/2pPlW2Y",
item4url = url + "/" + encodeURIComponent('#')+"image4",
item4ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00LC94I8I&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B00LC94I8I",
item4ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00LC94I8I&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item4ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00LC94I8I&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Combination of acrylic black handle with chrome-plated head give the Shaveology Double Edge Safety Razor a great luxurious look`,
    },
    {
        "id": "2",
        "positiveFeature": `Closed comb design gives close shave while helping to prevent nicks and cuts`,
    },
    {
        "id": "3",
        "positiveFeature": `Acrylic black handle is textured for added grip in wet conditions`,
    },
    {
        "id": "4",
        "positiveFeature": `Perfectly weighted for both balance and control of safety razor`,
    },
    {
        "id": "6",
        "positiveFeature": `Manufacturer supports Soldier's Angels`,
    },
    {
        "id": "5",
        "positiveFeature": `"90 Day Love Your Shave" guarantee`,
    },
],

item5Title = "Merkur Long Handled Safety Razor (MK 23C)",
item5Description = "The Merker Model 180 Long Handled safety razor is an excellent razor for using on the face, head and body. It is another beautifully crafted safety razor that is chrome-plated over zinc alloy that looks outstanding on the bathroom counter or in the shower. It will not rust when used properly. The long handle provides better access to those hard to reach areas such as the back of the head or back of the legs. The handle is finely machined knurled which provides an excellent grip in wet conditions. It is a 3-piece head which allows for easy blade changes and cleaning. As with our other top picks for the best safety razor, the aggressiveness of the safety razor is determined by the blade used.",
href5 ="http://amzn.to/2p36gJp",
item5url = url + "/" + encodeURIComponent('#')+"image5",
item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000NL0T1G&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B000NL0T1G",
item5ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000NL0T1G&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000NL0T1G&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Beautifully crafted safety razor with finely knurled machine handle for grip in wet conditions`,
    },
    {
        "id": "2",
        "positiveFeature": `Long handle allows for easier access to hard to reach areas`,
    },
    {
        "id": "4",
        "positiveFeature": `A great safety razor for shaving legs for baby smooth legs`,
    },
    {
        "id": "3",
        "positiveFeature": `3-piece head allows easy cleaning by simply loosening and running water through it`,
    },

],
moreInfo = [
    <div>
        <a name="how-to-use-a-safety-razor"></a>
        <h3>How To Use A Safety Razor</h3>
        <p>Using a safety razor is only slightly different than using a disposable razor. They both operate the same way and accomplish the same goal. The difference is in how you handle each type of razor and the angle you hold them against your skin. Although the name safety razor implies it is the safer razor to use, this is not the case when comparing it against disposable razors. Safety razors were created long before the modern disposable razors we use today. They were created to offer a safe alternative to the straight razor which is a razor sharp, fully exposed blade. Even when used with best intentions, it is not uncommon for those using the straight razor to get cuts or nicks. The <JumpTo title="best safety razor" href="#picks"/> offers an easier and more forgiving shaving experience.</p>
        <p>To use the safety razor properly, follow these steps:</p>
        <ol>
            <li><strong>Always start by replacing old or worn blades.</strong> The best shave will be achieved with a sharp blade, and blades often lose thier edge after only a few uses. Blades are <a href="http://amzn.to/2pqjvHh">affordable enough</a> that they can be replaced often, so be sure to replace them to start your shave off right. </li>
            <li><strong>Clean the area well with soap and water.</strong> Not only will this remove anything that could create a barrier between the blade and your skin, it will clean the area to help prevent infections if you get nicks or cuts.</li>
            <li><strong>Warm the hair with moist heat.</strong> For facial use, you can use a warm-hot towel. For the body or head you can use warm-hot water from the shower. The heat will cause the hair to expand and help to open the cuticle layer. This makes cutting it much easier.</li>
            <li><strong>Lubricate the area to shave well by either using soap, shaving cream or gel, or even warm water.</strong> What you use will depend on your skin. For best results it is recommended that you use a product that was specifically made for shaving as it will further enhance the ability of the blade to cut the hair. It will also allow the blade to glide much easier across the skin. Some people get good results with only water while others need to lather up before hand. We recommend lathering up with some type of lubricant whether it be soap, shaving soap, gel, or foam. If you are just starting out, for the best experience use a shaving product until you know how your skin reacts to the various methods.</li>
            <li><strong>Determine the proper angle to hold the razor against your skin.</strong> This is usually around a 30% angle, but depends on which safety razor you are using. With most safety razors, you'll find that the proper angle is one where the portion above the blade that touches your skin, the blade, and the portion below the blade that touches your skin all contact the skin at the same time with the same amount of pressure. </li>
            <li><strong>Determine the direction of hair growth on each section you will be shaving.</strong> Beginners should always start shaving in the direction of the hair growth. This allows the blade to move across the skin easier and results in a better shaving experience for those who are not sure how their skin will react to shaving in another direction. Once you become familiar with shaving in the direction the hair grows, you might want to try shaving across the grain. Shaving across the grain means you are not shaving in the opposite direction of the hair growth, rather you are shaving across the direction the hair grows. If the hair is growing up or down, you would be shaving sideways. This can give you a closer shave, but it can also cause more sensitivity issues, nicks, and even cuts. Going against the grain is typically not recommended, and neither is pulling the skin taut while shaving. While doing both can get you a closer shave because it exposed more of the hair to the blade, it is often the cause of razor burn, nicks, cuts, and general irritation.</li>
            <li><strong>Use only enough pressure to move the blade across the skin.</strong> Often, this means using almost no additional pressure at all. Many of the safety razors are weighted and the only pressure necessary is the force gravity exerts on the blade as it glides across the skin. If you use a fresh blade each time, you will find that it requires less pressure. If you use too much pressure, you will get a nick or even a nasty cut. Starting off, always use the least amount of pressure necessary to slide it across your lubricated skin. As you become more experienced, you will find what areas require different handling.</li>
            <li><strong>Rinse the blade often between strokes to keep it free from clogging up.</strong> If necessary, rinse and safely shake with a good grip so as not to throw or drop the safety razor.</li>
            <li><strong>Rinse your skin and check the results. </strong>If you missed areas or you want even smoother skin, repeat the process on those areas.</li>
            <li><strong>To minimize infections from cuts or nicks that you may not see or even feel, use an after-shave.</strong> After-shaves contain an anti-septic, usually alcohol, which helps prevent infections. If you are prone to razor burn, do not use a traditional after-shave and instead use an after-shave lotion which does not contain alcohol.</li>
        </ol>
        <Video source="https://www.youtube.com/embed/ALceIkWKurE" altText="how to use a safety razor video" tagName="video-how-to-use-safety-razor" videoTitle="How To Use A Safety Razor"/>
        <a name="how-to-prevent-razor-burn"></a>
        <h3>How To Prevent Razor Burn And Razor Bumps</h3>
        <p>Nothing ruins a nice shave faster than the appearance of unsightly razor bumps. Even with the best intentions and careful strokes of the blade, razor bumps seem to appear more often for some people than others. Razor bumps, also called razor burn is a condition where the skin creates tiny to small areas of inflammation that can itch, appear irritated, and cause general discomfort. It can appear as a red rash or small irritated bumps. Razor burn is not only uncomfortable, it can also cause the person to become very self conscience and affect their self esteem. Razor burn is common on areas of skin that may be sensitive such as the the face, underarm or bikini area but it can also appear on areas of the skin that are not as sensitive such as the legs.</p>
        <p>To help prevent razor burn, try the following tips:</p>
        <ol>
            <li>Ditch the disposable razor. Preventing razor burn is always preferable than curing razor burn. To prevent razor burn use the <JumpTo title="best safety razor" href="#picks" /> instead of disposable razors. The best safety razor with a fresh blade will cut closer and more cleanly. Blades that are not as sharp can lift the skin when snagging on the hair. This pulls the skin up into the blade and scrapes it against the razor. The result is injured skin that becomes inflamed and presents itself as a red rash. The best safety razors with a brand new, razor sharp blade is less likely to snag against the skin if you are <JumpTo title="shaving properly" href="#video-how-to-use-safety-razor" />. If you often get razor burns and don't have a safety razor, get one now. You will be glad that you did.</li>
            <li>Prior to shaving, always clean the skin properly with soap and warm water to remove as much irritants as possible. In the event you do give yourself a nick, cut, or scrape, you don't want those areas to get infected from germs that are present on the skin or hair.</li>
            <li>Consider using a pre-shave oil. Pre-shave oils differ from shaving creams and gels. They work by moisturizing the skin and helping the hair to be easier to cut. If you have tried everything except a pre-shave oil and still get razor burn give this optional product a try.</li>
            <li>Always use a lubricant such as shaving gel or shaving cream.</li>
            <li>Do not dry shave. Dry shaving is a great way to give yourself razor burn or even nick yourself. Dry skin doesn't allow the blade to glide across the surface easily. The hair also has not absorbed any water and softened which will cause it to be harder to cut. Hair that is harder to cut will pull the skin up underneath it into the path of the blade.</li>
            <li>Stay hydrated. Drink appropriate amounts of water throughout the day. Dehydrated skin loses it's elasticity. It doesn't pull back into place easily when stretched. Dehydrated skin and even the best safety razor does not make a good combination for a great shave.</li>
            <li>Do not pull the skin tight or taut while shaving. While this practice can get you a closer shave, it can also expose and cut more of the hair that is below the skin causing it to grow into the skin when the hair grows back.This can cause ingrown hairs and everything else that goes along with getting ingrown hairs.</li>
            <li>Shave in the direction that the hair is growing. This will minimize pulling the skin up towards the blade.</li>
            <li>After shaving, pat the skin dry and then apply an after-shave lotion. After-shave lotions help to prevent razor bumps and do not dry out the skin</li>
            <li>Thoroughly clean your shaving brush and shaving tools. These can house germs which can help cause infection if kept in poor condition.</li>
            <li>Do not leave your razor blade wet. The water can damage the blade causing it to dull. A dull blade is more likely to give you razor burn than a sharp blade. You can have the best safety razor money can buy but it is only as good as it's blade. If you are using a safety razor to shave, blades are more than affordable in comparison to disposable razors. Be kind to your skin and switch your blade often.</li>
        </ol>
    </div>
],
links = [
    {
        "id": 1,
        "title": "Our Top Picks",
        "tag": "#picks",
    },
    {
        "id": 2,
        "title": `The Types Of ${subject} We Compared`,
        "tag": "#types",
    },
    {
        "id": 3,
        "title": `What Do We Feel Makes The Best ${subject}`,
        "tag": "#features",
    },
    {
        "id": 4,
        "title": "How To Use A Safety Razor",
        "tag": "#how-to-use-a-safety-razor",
        "extra": "Additional Information",
    },
    {
        "id": 5,
        "title": `How To Prevent Razor Burn And Razor Bumps`,
        "tag": "#how-to-prevent-razor-burn",
    },
    {
        "id": 6,
        "title": "Watch A Video On How To Use A Safety Razor",
        "tag": "#video-how-to-use-safety-razor",
        "extra": "How To Video",
    },
    {
        "id": 100,
        "title": `What Do You Think Makes The Best ${subject}?`,
        "tag": "#discussion",
        "extra": `${subject} Discussion`,
    },
];

class PageDisplayed extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount() {
        ga('create', 'UA-92142805-1', 'auto');
        ga('send', 'pageview');
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>

                <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links}/>

                <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>


                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike}/>

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />
            </div>
        )
    }
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-92142805-1', 'auto');
ga('send', 'pageview');

module.exports = PageDisplayed
