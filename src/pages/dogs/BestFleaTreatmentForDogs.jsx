import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

import JumpTo from "../../components/JumpTo.jsx";

let url = "best-flea-treatment-for-dogs",
    buttonText = "See Additional Sizes",
    title = "Best Flea Treatment for Dogs",
    image = "../../js/img/dogs.jpg",
    mobileImage = "../js/img/dogs.jpg",
    parallaxAlt = "Dogs running happy after the best flea treatment for dogs kept them free from fleas",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>The Types Of Flea Treatment For Dogs We Compared</h3>
            <p>When we started looking for the best flea treatment for dogs, we decided to compare products that were effective in removing fleas that were already present on the dog. We were not looking for products that were focused primarily on the prevention of a flea infestation, rather we searched from products that were highly effective against an active flea infestation. If the treatment also helped to prevent a flea infestation or re-infestation (which all but one of our top picks do) that was an added bonus but not our main criteria. We looked for products that were generally safe to use and did not require special precautions or preperation other than bathing and drying your dog.</p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What Do We Feel Makes The Best Flea Treatment For Dogs?</h3>
            <p>Our number one indicator of the best flea treatment for dogs was that the product worked effectively at killing fleas.  We feel these types of products have a very specific purpose and should not have a gray area. Either it works effectively when used correctly for it's intended purpose or it does not. Flea treatments that worked quickly were also important to us. Fleas are able to reproduce so rapidly that time is of the essence to prevent a full blown infestation. Although it was not the main characteristic that we looked for, flea treatments that prevented infestations also scored highly with us. There is a saying that the best medicine is prevention. Obviously during an infestation that "medicine" cannot be applied, but prior to an infestation or directly afterwards that saying rings true. Some flea treatments require more caution when using than others, and we favored treatments that did not require an abundance of caution. When handling insecticides or chemicals it is always important to be careful but those that required less safety precautions than others scored higher with us.</p>
        </div>
    ],

    item1Title = "Bayer K9 Advantix II Flea And Tick Control Treatment For Dogs",
    item1Description = "We chose K9 Advantix II as our top pick because it is convenient, simple to use, and offers protection against more than just fleas. When we were looking for the best flea treatment for dogs, we wanted something that wouldn't be difficult or time consuming to apply and we wanted it to be effective. While we were looking specifically for fleas, many of the best flea treatment for dogs protects against much more. Bayer K9 Advantix II is no exception. It will also work on ticks, mosquitoes, biting flies and lice. Many people do not know but mosquitoes and ticks present problems for our 4 legged family members too. Both of these parasites can infect our dogs with disease causing organisms. This flea and tick treatment contains imidacloprid, permethrin, and pyriproxyfen.",
    href1 = "http://amzn.to/2oj4kz6",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004QRHRIQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B004QRHRIQ",
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004QRHRIQ&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004QRHRIQ&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Once a month, easy to apply topical application`,
        },
        {
            "id": "2",
            "positiveFeature": `Broad spectrum protection against fleas, ticks, mosquitoes, biting flies, and lice`,
        },
        {
            "id": "3",
            "positiveFeature": `Insect growth regulator has been added allowing this flea treatment to kill fleas in all stages of life`,
        },
        {
            "id": "4",
            "positiveFeature": `After being treated the fleas on a dog will not re-infest the home`,
        },
        {
            "id": "5",
            "positiveFeature": `Waterproof formula will not require reapplication after weekly swimming or bathing with non-detergent shampoo`,
        },

    ],

    item2Title = "Merial Frontline Plus Flea and Tick Control For Dogs",
    item2Description = "Our second pick for the best flea treatment for dogs is Merial Frontline Plus. This flea and tick killer should be applied once per month and will kill fleas in as little 12 hours. It contains 2 ingredients to fight fleas and ticks. The first ingredient fipronil is responsible for killing adult fleas and multiple species of ticks. The second ingredient (S)-methoprene prevents the flea eggs, larvae and pupae from developing to get rid of the threat while they are in an earlier life cycle. Combined, these two ingredients are able to effectively provide tough killing action for the entire 30 days. Because this flea treatment focuses on the entire lifecycle, it aids in the prevention of a flea infestation in your house. As with our top pick for best flea treatment for dogs, this one is waterproof as well. This flea and tick treatment contains fipronil and (S)-methoprene.",
    href2 = "http://amzn.to/2mTg0IE",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0002J1FOE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B0002J1FOE",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0002J1FOE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0002J1FOE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Simple to apply topical application is good for 30 days`,
        },
        {
            "id": "2",
            "positiveFeature": `Effective against all stages of the flea life cycle `,
        },
        {
            "id": "3",
            "positiveFeature": `Helps to prevent a flea infestation in home`,
        },
        {
            "id": "4",
            "positiveFeature": `Waterproof formula`,
        },
        {
            "id": "5",
            "positiveFeature": `You can enter your email address in the manufacturer's website to get a reminder to apply flea treatment for continued protection`,
        },
    ],

    item3Title = "Novartis Capstar Flea Tablets for Dogs and Cats",
    item3Description = "Capstar flea treatment tables are an oral treatment that begins to work in as little as 30 minutes and will kill 90% or more of the adult fleas on your dog in 4 hours. This flea treatment is a great preventative measure when introducing a dog into a clean environment such as a kennel or even rescue. It can be used on puppies that are as young as 4 weeks old which makes it one of the flea treatments that can be used on such a young animal directly. For this reason it has earned our number 3 spot in our top picks. Fleas are not particular when it comes to age and puppies get infested, but often do not have a safe treatment. The tablets are not flavored so they are easy to mix with other treats that your dog likes. These tablets are safe for dogs that are pregnant or nursing as the insecticide in these tablets disrupts the nerve transmission in fleas without effecting your dog. It is important to note that these tablets are effective for 24 hours only and will not control fleas in the environment. Because of this, it won't be effective in keeping animals free from fleas in infested environment. This flea treatment contains nitenpyram.",
    href3 = "http://amzn.to/2nEi7ys",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B005Z6UL1M&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B005Z6UL1M",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B005Z6UL1M&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B005Z6UL1M&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Oral tablet can be used on puppies as young as 4 weeks old`,
        },
        {
            "id": "2",
            "positiveFeature": `Kills more than 90% of the fleas in 4 hours`,
        },
        {
            "id": "3",
            "positiveFeature": `Uses an insecticide that kills fleas but is harmless to dogs`,
        },
        {
            "id": "4",
            "positiveFeature": `Great flea treatment for dogs that need to be disinfected before being introduced to a clean environment such as a kennel or rescue`,
        },

    ],

    item4Title = "Vectra 3D Flea Treatment",
    item4Description = "Vectra 3D is one of the next generation flea treatments on the market and has one of the strongest chemical formulations on the market. It uses 3 active ingredients in it's flea treatment while many other flea treatments only use two. Many fleas and ticks that are located in certain regions are becoming resistant to the previous generation of flea and tick treatments. This flea treatment offers broad spectrum protection against fleas, ticks, mosquitoes, biting and sand flies, lice and certain mites for dogs and begins working in as little as 5 minutes. Although this does not kill all parasites, it offers repellent protection as those parasite will not bite or attach to your dog and this may help to prevent the spread of a transmittable disease. As a flea treatment, Vector 3D kills adult fleas and prevents the development of fleas in an earlier life cycle. Each application lasts for one month and it continues to work after swimming or bathing. The ingredients include Permethrin, Dinotefuran, and Pyriproxifen.",
    href4 = "http://amzn.to/2nkiFGC",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EFEU4YY&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel4 = "",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EFEU4YY&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EFEU4YY&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Formulated with one of the strongest chemical formulations on the market for fleas and ticks that have become resistant to typical flea treatment`,
        },
        {
            "id": "2",
            "positiveFeature": `Begins working in as little as 5 minutes`,
        },
        {
            "id": "3",
            "positiveFeature": `Waterproof formula is effective after bathing or swimming`,
        },
        {
            "id": "4",
            "positiveFeature": `Broad spectrum formula`,
        },
        {
            "id": "5",
            "positiveFeature": `Helps to prevent re-infestation of home for up to 4 weeks after application`,
        },

    ],

    item5Title = "Sergeant's Pet Products PetArmor Plus for Dogs Flea and Tick Squeeze- On",
    item5Description = "Pet Armor plus for dogs is a topical application flea and tick treatment with 6 way protection that lasts for 30 days. It is formulated with the same active ingredients fipronil and (S)-methroprene that is used in another name brand flea treatment. It is used to treat fleas, flea eggs, flea larvae, ticks and chewing lice. It also disrupts the development of flea pupae. This formula is able to control mites that cause sarcoptic mange and the deer tick which is responsible for spreading Lyme disease. Each applicatinon on your dog helps to prevent flea infestation of your home for four weeks. Once applied to your dog, it takes around 12 hours to begin killing fleas and up to 48 hours to begin killing ticks. This formula is waterproof and is still effective after weekly bathing or swimming. Fipronil and (S)-methoprene are the two active ingredients in this flea treatment.",
    href5 = "http://amzn.to/2ntNQzY",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00WWP1U7I&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel5 = "",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00WWP1U7I&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00WWP1U7I&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Easy to apply topical treatment`,
        },
        {
            "id": "2",
            "positiveFeature": `Uses the same active ingredients as other well known brand`,
        },
        {
            "id": "3",
            "positiveFeature": `Effective for 30 days after treatment`,
        },
        {
            "id": "4",
            "positiveFeature": `Can withstand weekly bathings or swimming`,
        },
        {
            "id": "5",
            "positiveFeature": `Helps to reduce infestation of home for 4 weeks after use`,
        },
    ],
    subject = "Flea Treatment For Dogs",
    moreInfo = [
        <div>
            <section>
                <a name="ingredients"></a>
                <h3>What Ingredients Are Used In The Best Flea Treatments?</h3>
                <p>What makes the best flea treatments effective? Are the ingredients that are used harmful to our pets or to us? When we put these flea treatments on our pets, are they going to transfer to our furniture or even our skin when we pet them and if so what dangers do they present? These are all very valid concerns. The following list of ingredients are ingredients that may be used in the best flea treatments for dogs. Some treatments contain more than others, while others may not contain any of these ingredients at all.  Be sure to check the manufacture's label to determine exactly what the active ingredient is in the flea treatment you are using.</p>
                <ul>
                    <li>Imidacloprid: This is the most widely used insecticide in the world and is a neurotoxin to insects. In a study in which laboratory rats were used, the World Health Organization rates it as moderately toxic if ingested and low toxicity if applied to the skin. In our list of the best flea treatment for dogs this insecticide is used in a topical application (applied to the skin).</li>
                    <li>Permethrin: A medication as well as an insecticide. When used as a medication it is applied to the skin and is used in treating scabies and lice. As an insecticide it is used to kill without regard to the type of insect it kills. In flea and tick treatments it will also kill ticks on contact. Permetherin is considered to be safe for topical use for people who are over 2 months old. Studies that involve animals show no ill effects on fertility or cause for abnormal development but studies have not been performed on humans. The FDA has given it a pregnancy category B and the EPA has classified it as a likely human carcinogen. The EPA has listed it as such due to studies which involved feedin mice permethrin and the development of liver and lung tumors<sup><JumpTo href="#ref1" classes="inline-block" title="[1]" /></sup> </li>
                    <li>Pyriproxfen: An insect growth regulator in which one of the uses is for flea treatment of household pets<sup><JumpTo href="#ref2" classes="inline-block" title="[2]" /></sup>. It is listed as low acute toxicity<sup><JumpTo href="#ref3" classes="inline-block" title="[3]" /></sup>. It prevents the larvae of fleas from developing into adulthood causing them to be unable to reproduce. This effectively breaks the life cycle of the flea.</li>
                    <li>Fipronil: A broad spectrum insecticide causing toxicity in the nervous system of insects. When used against fleas, it kills virtually all of the fleas in as little as one to two days. The World Health Organization classifies Fipronil as a class II moderately hazardous pesticide<sup><JumpTo href="#ref4" classes="inline-block" title="[4]" /></sup>.</li>
                    <li>(S)-methoprene: This is a juvenile hormone mimic that is effective against insects because it does not kill them, but it stunts their growth. This does not allow the insect to reach adulthood when it would be able to reproduce. This effectively breaks the flea's life cycle. It presents itself to be extremely unlikely for human toxicity via oral, dermal, ocular or inhalation methods of exposure<sup><JumpTo href="#ref5" classes="inline-block" title="[5]" /></sup>.</li>
                    <li>Nitenpyram: An insecticide used in agriculture and also used to kill external parasites that often plague pets. It is a central nervous system toxin that causes quick death to insects. When used for pets it is administered orally and begins to work in as little as 10 minutes. While extremely toxic to insect, as a general rule dogs and cats tolerate nitenpyram very well.<sup><JumpTo href="#ref6" classes="inline-block" title="[6]" /></sup></li>
                    <li>Dinotefuran: An insecticide that disrupts the insects nervous system. It is used in veterinary medicine against fleas and ticks and is often used combined with other insecticides to be more effective overall. In humans, it has low toxicity by means of oral, dermal and inhalation routes<sup><JumpTo href="#ref7" classes="inline-block" title="[7]" /></sup>.</li>
                </ul>
                <p>Based upon the above information and the sources they references, the flea treatments that we placed in our top picks for the best flea treatment for dogs are generally safe when used as directed. This means after proper application using the treatments and allowing them to dry you can pet your dogs without worry of causing yourself harm. If the dog is a house pet and is in contact with furniture you need not worry as none of the products show concerning levels of toxicity from topical application. If however you have insects as companions (being entirely serious, some people do have insects as companions), please keep in mind that the flea treatments often contain insecticides that kill indiscriminately and won't distinguish between "good" vs "bad" insects.</p>
            </section>
            <section>
                <a name="prevent-flea-infestation"></a>
                <h3>How To Prevent A Flea Infestation In Your Home</h3>
                <p>You can be a very clean person and still get a flea infestation in your home. While your dog scratching his body due to flea bites has left you scratching your head on why this happened, remember that although you take precautions your dog doesn't. Your dog will run places and get into things you wouldn't dream of. Many of these places may also be frequented by other animals that carry fleas. When preventing a flea infestation from occurring in your home, you have to take preventative measures for your dog's environment as well.</p>
                <ol>
                    <li>The first line of defense is keeping your yard free from fleas. No matter how well you clean your house, if the surrounding environment is infested with fleas they are bound to be brought indoors eventually. Fleas like to occupy areas where they can hide. If your yard is overgrown with grass, shrubbery, or even weeds it provides a place for fleas and ticks to hide. Not only does it provide the fleas a place to hide, but it provides wild and feral animals a place to hide which could be bringing in the fleas. By maintaining your yard, you reduce the chance for those carriers to take up residence in your yard. This reduces the occurrence of fleas in your yard. Keep lawns manicured and remove overgrowth. Flea larvae are not able to survive if there is plenty of air movement and sunlight so be sure that the areas immediately surrounding your house give plenty of both if possible. What gives a house curb appeal to a buyer does the opposite effect for fleas and their carriers.</li>
                    <li>Use a flea treatment on any animal prior to introducing it into your home. If you are doing your best to prevent a flea infestation then you should assume any animal you bring into your home that is a potential carrier of fleas, has fleas. This means using a flea treatment on it prior to introducing it or immediately upon introducing it into your home. <a href="http://amzn.to/2nuxsz8" target="_blank">Tablets</a> such as these are safe for puppies as young as 4 weeks old and will kill more than 90% of the fleas in 4 hours.</li>
                    <li>Groom your dog often. With short haired dogs it may be easier to spot fleas but even if your dog has long hair, brush through it and pay attention to the area you are brushing. Not only is it great for your dog, it is great for detecting if fleas are present. You can do a visual inspection while grooming your dog. Fleas are large enough that you can spot them with your naked eye if you are making the effort.</li>
                    <li>Keep your house cleaned. A clean house won't stop fleas from taking up residence but a dirty house is much easier for them to thrive in. In the early stages of life, immature fleas often live within the carpet fibers. Vacumming frequently can help to remove these immature fleas which will help in preventing a flea infestation. Be sure to pay attention to areas your dog likes to visit. Fleas tend to prefer low traffic areas to hide, so don't just vacuum or clean high traffic areas. Check under the couch cushions, the couch, coffee tables, corners, baseboards, and any other place that can potentially harbor these pesky parasites.</li>
                    <li>Use a flea treatment on your pet that also prevents infestations from occurring after use. Many of the flea treatments on the market have a dual purpose. They kill fleas and keep them from infesting. 4 of the 5 best flea treatments we have as our top picks are also effective for up to 4 weeks after application. The one that doesn't is listed because it can be used on puppies as young as 4 weeks whereas these others cannot. For effective treatment be sure to take preventative precautions such as using a flea treatment that keeps infestations from occurring.</li>
                </ol>
            </section>
            <section>
                <a name="how-to-treat-fleas"></a>
                <h3>How To Treat Fleas</h3>
                <p>A number of different methods exist to treat fleas. The most common ones are:</p>
                <ul>
                    <li>Topical Medication: These medicines are often used by parting your dog's hair and using an applicator squeezing the medicine directly onto the dog's skin. These medicines contain various insecticides that are toxic to fleas and may repel or be toxic to other external parasites such as ticks, mites and mosquitoes. These insectide work up to 4 weeks and can withstand weekly washing and swimming. Dogs that have topical medications that contain the ingredients we have listed <a href="ingredients">above</a> should be kept away from other animals and children while drying, but once dried they are safe to pet.</li>
                    <li>Oral Medication: These flea treatments are fed to your dog and can begin to work incredibly quickly. They can start to kill fleas in as little as 30 minutes and are generally considered to be safe although there have been some occurrences of animals having unwanted side effects. Oral medications such as nitenpyram can be used to quickly kill fleas off an dog that will be boarded, kenneled, or introduced into a new environment but it does not prevent re-infestation and should be used in combination with products for long term effects if an infestation is present. The oral medication nitenpryam is also safe for pregnant and nursing dogs.</li>
                    <li>Sprays: This method is an easy way to spray the flea treatment onto the dog and especially the environment they frequent such as bedding and carpets. They often contain more than just insecticides that kill fleas, they may also contain insect regulators which mimic juvinile hormones that do not allow the flea to develop to a reproduction stage. These treatments are excellent for breaking the life cycle of fleas where your dog rests.</li>
                    <li>Powders: This type of application is the same as a spray but in a powder form. They usually play the same role, and are used on the dog itself as well as they environment they stay in. They are useful for areas where the exposure to moisture may be a concern and spraying is not an option.</li>
                    <li>Shampoos: Most shampoo flea treatments will kill adult fleas on contact but do little to prevent an infestation from occurring. These are applied and lathered liberally and then allowed to sit on the dog for 10 to 15 minutes before rinsing. If your dog enjoys shampoos then it may enjoy this form of flea treatment, but it will need to be done more frequently than topical applications that continue to work for 4 weeks.</li>
                    <li>Dips: A dip is a concentrated form of medicine that is mixed with water and then applied to the dog. It can be poured directly onto the dog or it can be applied with a sponge or wash cloth. After application it is left to dry on the skin and coat. These flea treatments require more caution when using, both in preparation and application than some of the other methods. Dips can be perform at the veterinarians office if the dog's owner is uncomfortable doing it on their own.</li>
                    <li>Collars: This form of flea treatment can be the longest lasting but can also give a false reassurance if used improperly. Flea collars contain medication that is released over several months time. If left on too long, they become ineffective however as the medication is no longer present at the strength it was when first used. Flea collars should be replaced according to the manufacturer's specifications. Some of the flea collars can cause irritation to the dogs and should be monitored to make sure that the dog is not suffering ill effects from wearing it. They can also be smell strongly to some people. For those who have a sensitive nose, they may find the smell too strong to share a room with their dog.</li>
                </ul>
            </section>
            <section>
                <a name="references"></a>
                <h3>References</h3>
                <ol>
                    <li><a name="ref1"></a><a href="https://nepis.epa.gov/Exe/ZyNET.exe/P1005J0O.TXT?ZyActionD=ZyDocument&Client=EPA&Index=2006+Thru+2010&Docs=&Query=&Time=&EndTime=&SearchMethod=1&TocRestrict=n&Toc=&TocEntry=&QField=&QFieldYear=&QFieldMonth=&QFieldDay=&IntQFieldOp=0&ExtQFieldOp=0&XmlQuery=&File=D%3A%5Czyfiles%5CIndex%20Data%5C06thru10%5CTxt%5C00000012%5CP1005J0O.txt&User=ANONYMOUS&Password=anonymous&SortMethod=h%7C-&MaximumDocuments=1&FuzzyDegree=0&ImageQuality=r75g8/r75g8/x150y150g16/i425&Display=p%7Cf&DefSeekPage=x&SearchBack=ZyActionL&Back=ZyActionS&BackDesc=Results%20page&MaximumPages=1&ZyEntry=1&SeekPage=x&ZyPURL" target="_blank">Permethrin Facts</a>, US EPA, June 2006.</li>
                    <li><a name="ref2"></a>Maddison, Jill E.; Page, Stephen W.; Church, David (2008). <a href="http://amzn.to/2nuoYZ5" target="_blank">Small Animal Clinical Pharmacology.</a> Elsevier Health Sciences. p. 226. ISBN 9780702028588.</li>
                    <li><a name="ref3"></a><a href="http://inchem.org/documents/jmpr/jmpmono/v99pr12.htm" target="_blank">Toxicological evaluations.</a> inchem.org. Retrieved 2016-04-13.</li>
                    <li><a name="ref4"></a><a href="http://www.who.int/ipcs/publications/en/pesticides_hazard.pdf" target="_blank">The WHO Recommended Classification of Pesticides by Hazards and Guidelines to Classification 2000-2002</a> p. 55.</li>
                    <li><a name="ref5"></a>EPA Pesticide Fact Sheet<a href="https://www3.epa.gov/pesticides/chem_search/reg_actions/reregistration/fs_PC-105401_1-Jun-01.pdf" target="_blank"> June 2001 Update of the March 1991 Methoprene R.E.D. Fact Sheet</a> p. 2.</li>
                    <li><a name="ref6"></a>NITENPYRAM: Safety Summary for Veterinary Use<a href="http://parasitipedia.net/index.php?option=com_content&view=article&id=2689&Itemid=3011" target="_blank"> parasitipedia.net</a></li>
                    <li><a name="ref7"></a>EPA Pesticide Fact Sheet<a href="http://pollinatorstewardship.org/wp-content/uploads/2013/08/fs_PC-044312_01-Sep-04-Dinotefuran-Fact-Sheet.pdf" target="_blank"> September 2004 Dinotefuran Fact Sheet</a> p. 2.</li>
                </ol>
            </section>
        </div>
    ],
    links = [
        {
            "id": 1,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 2,
            "title": "The Types Of Flea Treatment For Dogs We Compared",
            "tag": "#types",
        },
        {
            "id": 3,
            "title": "What Do We Feel Makes The Best Flea Treatment For Dogs?",
            "tag": "#features",
        },
        {
            "id": 4,
            "title": "What Ingredients Are Used In The Best Flea Treatments?",
            "tag": "#ingredients",
            "extra": "Additional Information",
        },
        {
            "id": 5,
            "title": "How To Prevent A Flea Infestation In Your Home",
            "tag": "#prevent-flea-infestation",
        },
        {
            "id": 6,
            "title": "How To Treat Fleas",
            "tag": "#how-to-treat-fleas",
        },
        {
            "id": 7,
            "title": "References",
            "tag": "#references",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ];

class BestFleaTreatmentForDogs extends React.Component {
    constructor(props) {
        super(props);
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }

    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links} />

                <PageDescription date={this.date()} pageDescription={mainDescription[0]} />

                <Description buttonText={buttonText} itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />


                <Description buttonText={buttonText} itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />

                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}


module.exports = BestFleaTreatmentForDogs
