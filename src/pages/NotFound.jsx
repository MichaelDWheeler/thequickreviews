import React from "react";
import ReviewedItems from "../components/ReviewedItems.jsx";
import { ReactBootstrap, Col, Row, Image } from "react-bootstrap";


class NotFound extends React.Component {
    render() {
        return (
            <div>
                <div className="container">
                    <Row>
                        <div className="not-exist">
                            The page you are looking for does not exist, but don't worry because all of these ones do!
                    <ReviewedItems />
                        </div>
                    </Row>
                </div>
            </div>
        )
    }
}

module.exports = NotFound