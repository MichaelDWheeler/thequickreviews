import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class AmazonButton extends React.Component{
    render() {
        let buttonText;
        if (this.props.buttonText === "" || this.props.buttonText == undefined) {
            buttonText = "SEE ADDITIONAL INFORMATION";
        } else {
            buttonText = this.props.buttonText;
        }
        return(
            <div>
                <div className="winners">
                    <div className="placement">
                        <div className="amazon-button-container">
                            <div className="place-number text-center">{buttonText}</div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
module.exports = AmazonButton;
