import React from 'react';
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class ShareIcons extends React.Component{
    facebook(){
        return("https://www.facebook.com/sharer/sharer.php?u=http://www.thequickreviews.com/" + this.props.url)
    }
    google(){
        return("https://plus.google.com/share?url=http://www.thequickreviews.com/" + this.props.url)
    }
    twitter(){
        return("https://twitter.com/intent/tweet?text=http://www.thequickreviews.com/" + this.props.url)
    }
    linkedIn(){
        return("https://www.linkedin.com/shareArticle?mini=true&url=http://www.thequickreviews.com/" + this.props.url)
    }
    render(){
        return(

                            <div className="share-icons-container">
                                <a target="_blanke" href={this.facebook()}>
                                    <img className="social-icons" src="../js/img/facebook-icon.svg" />
                                </a>
                                <a target="_blanke" href={this.google()}>
                                    <img className="social-icons" src="../js/img/google+.svg" />
                                </a>
                                <a target="_blanke" href={this.twitter()}>
                                    <img className="social-icons" src="../js/img/twitter.svg" />
                                </a>
                                <a target="_blanke" href={this.linkedIn()}>
                                    <img className="social-icons" src="../js/img/linkedin.svg" />
                                </a>
                            </div>

        )
    }
}

module.exports = ShareIcons;
