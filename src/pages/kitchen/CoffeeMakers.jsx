import React from "react";
import MetaTags from 'react-meta-tags';
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import Placement from "../../components/Placement.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";



let url = "coffeeMakers",
title ="Best Coffee Makers",
image= "../../js/img/kitchen.jpg",
mobileImage="../js/img/kitchen.jpg",
parallaxAlt="Modern open kitchen with off white cabinets and center island",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of Coffee Makers We Compared</h3>
        <p>
            There are many different types of coffee makers on the market. For this particular comparison we really focused on one type which is the electric automatic drip coffee maker which is capable of producing multiple cups of coffee at a time. We did not review any coffee makers that take pods. In addition, we also included a percolator as it is used to brew cold coffee and is gaining in popularity. We felt it would be a great option that many people may not have considered or knew existed.
        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Features Should A Coffee Maker Have?</h3>
        <p>
            When choosing our top picks, we looked out for certain features that we felt coffee makers should have. We kept in mind that our top picks were definitely our personal preference so we made sure that we also included coffee makers that may not be exactly to our specifications but still added great benefit to the coffee maker. Among the features that helped to make our choices are:
        </p>
        <ul>
            <li>
                <strong>
                    Overall capacity:&nbsp;
                </strong>
                We did not choose any coffee maker that was single serving only. This is where our personal preference really stood out because we drink a lot of coffee. We drink so much that we don't have enough experience with single serving coffee makers. We wanted to make sure the coffee maker was capable of only brewing a single serving if necessary, but not as the only option. We have used coffee makers that require pods to brew, but feel that is an entirely different class of coffee makers. We don't mean to say that we feel pod type coffee makers are a better class, just a different class.  So much so that we feel that class requires it's own evalutation so we did not include those here either. For a really quick overview of why we feel those coffee makers belong in their own class, it is because those who are looking for pod coffee makers are generally lookling for that type of coffee maker and not these types.
            </li>
            <li>
                <strong>
                    Programmable brewing times:&nbsp;
                </strong>
                This was high on our list. Coffee makers that included this feature scored stronger with us because being able to wake up to the aroma of freshly brewed coffee is wonderful. Even better is the ability to immediately indulge in that coffee without having to wait until it is done brewing.
            </li>
            <li>
                <strong>
                    Ability to remove carafe while brewing:&nbsp;
                </strong>
                Even though we like programmable times, we also often make coffee on the fly and don't want to wait for the entire pot to have brewed before enjoying. Coffee makers that allow us to grab some coffee mid-brew scored well. Not all of our top picks have this feature, but that means they were strong in another feature which kept them in the running.
            </li>
            <li>
                <strong>
                    Auto off feature:&nbsp;
                </strong> We make a pot of coffee, grab it mid brew, and then forget about it. There is little worse than drinking coffee that has been overheated to the point of tasting burned and overly acidic. For energy savings, safety, and redrinkability, we like coffee makers that will automatically shut off the warmer after a few hours.
            </li>
            <li>
                <strong>
                    Permanent mesh screen filter:&nbsp;
                </strong>
                Paper filters can be a hassle. Not only do you have to constantly replace them, you also have to deal with them after using them. I know what you may be thinking, you have to dump the used grounds in the trash regardless. Dumping the grounds in the trash from a mesh filter versus a paper filter are two different beasts though. There have been many mornings that I grab a wet paper filter and pick it up to bring it to the trash only to somehow lose grip on one of the corners and have it spill all over. Maybe I bumped something. Perhaps I was too half asleep to pay attention properly. Maybe I am not using my entire hand to fully grab the soaked filter, and it's probably because I don't like the feeling. There have been numerous occasions I have spilled used coffee grounds due to my grip and paper filters. It is bad enough when it is midday, but first thing in the morning can be really upsetting. Not much worse than trying to clean up wet coffee grounds on the floor that smear while scooping and sweeping them up. I don't have that issue with a reusable mesh filter. Most of them have handles, they keep their shape, and they dump easily.
            </li>
            <li>
                <strong>
                    Brew Strength:&nbsp;
                </strong>
                Some days we want it strong and maybe later that day we don't need it quite as strong. Sure we could add less grounds but not only does that reduce the caffiene it makes it justifiably seem as if the brew has been watered down. No, instead we prefer to be able to select the strength of our brew from a setting on the coffee maker itself. Coffee makers with this setting available will reduce the amount of water that flows through the shower head each minute which allows the water to absorb more of the flavor and caffeine as it passes through.
            </li>
            <li>
                <strong>
                    Single serve or less cups feature:&nbsp;
                </strong>
                Just because we did not include any single serve coffee makers doesn't mean that we don't appreciate the ability to properly make a single cup or a few more at a time. Coffee makers with this feature usually reduce the heat to the hotplate so as not to burn your coffee.
            </li>
        </ul>
    </div>
],
subject = "Coffee Makers",
moreInfo = [
    <div>
        <a name="clean"></a>
        <h3>How To Clean Your Coffee Maker</h3>
        <p>
            Approximately half of the coffee makers which were tested in a <a href="http://www.nsf.org/newsroom/kitchen-is-germiest-place-in-home-according-to-recent-study-by-nsf/" target="_blank">NSF International Study</a> were found to have yeast and mold growing in the reservoirs and we find that to be really disgusting.  Approximately 10 percent had a bacteria called coliform growing in them. When compared to things we usually consider dirty such as bathroom door handles and toilet seats it was found that the reservoir in coffee makers were germier. It may come as a surprise because most of us would never eat off a toilet seat yet we drink from a reservoir that has more germs. If you think about it, the reason is pretty clear. Bathrooms are cleaned far more often than the water reservoir for a coffee maker usually is.
        </p>
        <p>
            Many bacteria and mold thrive in an environment such as a coffee maker reservoir. The reservoirs are incredibly moist. They get filled with water, the water is removed when the coffee is brewed, and the lid is shut over the top preventing evaporation. What is left is a humid, moist, warm or room temperature environment that bacteria and mold can multiple in. Additional bacteria can be introduced into the reservoir when we do clean it with a dirty sponge. Though people may commonly use less than sanitized sponges on dishes, when the dishes dry they kill off the majority of bacteria. The reason we do not get immediately sick from the contaminated water is because of our immune systems, but over time with enough contamination it can effect us and cause us to get sick. To combat these germs it is important to keep a consistent cleaning cycle on our coffee makers just as we do in our bathrooms.
        </p>
        <p>
            To properly clean your coffee makers reservoir, one should use good old vinegar. Not only will it sanitize your coffee maker, but it will descale or decalcify it as well. There are better alternatives to descaling which is addressed below, but for disinfecting your reservoirs this is a great solution. If you live in an area where your water is especially hard, you will want to do this more often as it will keep your coffee maker working properly for longer. For the rest of the coffee maker, you should give it a daily cleaning just as you would give your utensils or dishes a daily cleaning. The difference is the type of cleaning, and a coffee maker need only be given a light cleaning. There won't be caked on food or grime, so you don't need to give it massive amounts of elbow grease. Instead, using a clean sponge and warm soapy water give the carafe, lid, permanent filter, and filter basket a gently cleaning. For the reservoir, use a paper towel to dry the interior and then be sure to leave the lid open so the residue water can evaporate. Doing this will not create an environment that will promote mold and bacteria growth as easily.
        </p>
        <a name="descale"></a>
        <h3>How To Descale Your Automatic Coffee Maker</h3>
        <p>
            You went out and purchased the very best coffee maker that you could find. You read reviews, read technical specs, pictured that particular coffee maker in your house, on your counter, making coffee for you in the mornings, and you agree that this is the best coffee maker! You have enjoyed it immensely for the past few months. It has been a faithful friend to you in the mornings. One day, you decide to make some coffee in the afternoon and while you are waiting you notice that it is dripping slowly. You don't remember it taking this long, but how could you? In your quest for the best coffee maker you purchased one that automatically brews before you even wake up. This is the first time you have waited for it, and it is taking a long time. Too long. The kind of long that says "It is broken" long. Did you get a defective unit? Does it really take this long and you were unaware? Did you waste your money and you need to purchase another one, or spend money shipping to the manufacturer for replacement? Probably not. If you have not descaled your coffee maker, this is likely the reason why it is dripping slowly or perhaps not at all.
        </p>
        <p>
            Water has minerals and impurtities in it. We can't really see them with our naked eyes but they are there. Some water has a higher mineral content in it, and this is known as hard water. In areas with hard water, the coffee maker will suffer the results of calcification faster than in areas that do not have it. These impurities travel through your coffee maker where they can begin to accumulate. Not only will they accumulate but they will harden over time. As they grow, they block off the passageway that the water travels to make your fresh brewed coffee. This is no fault of your coffee maker. You probably did a great job picking the best one for you, and any other coffee maker would most likely experience the same failure over time. What you need to do is descale or decalcify your coffee maker.
        </p>
        <p>
            Fortunately descaling your coffee maker is really simple. First check the manual. It is highly likely that it addresses this very issue and recommends a method for descaling it. If so, follow that. If not, at least once every six month you should descale it. To do so you can purchase products such as <a href="http://amzn.to/2mkCUqJ">Dezcal</a> or <a href="http://amzn.to/2mC0P5M">Essential Values Coffee Machine Descaler and Cleaner</a> and use those to get rid of that calcification buildup. These are a safe way to remove those hardened minerals from your coffee maker. Either one is a good purchases to make with the purchase of your coffee maker. You spent all this time reading reviews to find the best coffee maker you could, this is an easy way to maintain it and prolong the enjoyment that you get from it. While a well known home remedy involving vinegar works, these products do the job without leaving a residue, or a vinegar odor/taste that can linger.
        </p>
        <p>
            If you already have white vinegar in your house, and you don't want to spend any additional money then you can use this well known remedy. It is effective and a quick way to get the job done when you don't have the descaler products on hand.
        </p>
        <ol>
            <li>Prepare the coffee maker. Clean the filter basket by removing all grounds and making sure the basket itself is clean. Make sure the carafe is empty and clean.</li>
            <li>Using the carafe, fill it halfway full with faucet hot water. Do not boil water, just use hot water from the faucet. Fill the remainder of the carafe with the white vinegar.</li>
            <li>Pour the carafe into the reservoir.</li>
            <li>Start a brew cycle. If you use a glass carafe, when it is half full turn off the coffee maker. If your carafe is non transparent, you will need to either monitor the water level in the reservoir or monitor the water level indicator. Either way, you want to stop it from brewing after it has used half the liquid.</li>
            <li>Let it sit for an hour. This will give it time to descale the innards of your coffee maker.</li>
            <li>Once the hour has passed, finish the brew cycle. If your coffee maker was full of calcification, it should now be flowing a lot more freely. If not, repeat the cycle.</li>
            <li>Pour out the carafe and clean it well. Then fill it with water only and run it through your coffee maker at minimum 3 times to get rid of the vinegar scent and after taste it can leave.</li>
        </ol>
        <p>
            If the results have not improved after 3 descaling attempts there is more than likely something else going on such as the tubing being clogged. This can happen for a number of reasons, including coffee grounds being stuck in there. If this occurs, you will want to contact the manufacturer for guidance.
        </p>
    </div>
],

links = [
    {
        "id" : 3,
        "title" : "Our Top Picks",
        "tag" : "#picks",
    },
    {
        "id" : 1,
        "title" : "The Type Of Coffee Makers We Compared",
        "tag" : "#types",
    },
    {
        "id" : 2,
        "title" : "What Features Should A Coffee Maker Have",
        "tag" : "#features",
    },
    {
        "id" : 4,
        "title" : "How To Clean Your Coffee Maker",
        "tag" :  "#clean",
        "extra" : "Additional Information",
    },
    {
        "id" : 5,
        "title" : "How To Descale Your Automatic Coffee Maker",
        "tag" :  "#descale",
    },
    {
        "id": 100,
        "title": "What Do You Think Makes The Best " + subject + "?",
        "tag": "#discussion",
        "extra": subject + " Discussion",
    },
],

item1Title = "Moccamaster KBG 741 10-Cup Coffee Brewer with Glass Carafe, Polished Silver",
item1Description = "The Moccamaster KBG 741 is a 10 cup coffee maker that utilizes an auto drip-stop brew basket and a glass carafe. It contains an automatically adjusting dual-phase hot-plate element that will keep the temperature of the coffee between 176 - 185 degrees Fahrenheit for 100 minutes. This coffee maker is able to brew 10 cups of coffee in just 6 minutes. When brewing, it consistently stays in the temperature range of 196 - 205 degrees Fahrenheit with a pulse action that produces the perfect coffee bloom while the extraction process is occurring. It is ECBA, SCAA, SCAE approved.",
href1="https://www.amazon.com/Moccamaster-10-Cup-Coffee-Brewer-Polished/dp/B0055P70MQ/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487869284&sr=1-1&keywords=Moccamaster+KBG+741+10-Cup+Coffee+Brewer+with+Glass+Carafe,+Polished+Silver&linkCode=li3&tag=bestvoted-20&linkId=487ca05d9bb2c0897194277fd546979c",
pixel1="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B0055P70MQ",
item1url = url + "/" + encodeURIComponent('#')+"image1",
item1ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0055P70MQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item1ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0055P70MQ&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item1ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0055P70MQ&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item1WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `The heating element of the Moccamaster KBG 741 is made out copper and not out of aluminum`,
    },
    {
        "id": "2",
        "positiveFeature": `Incredible style and is easily distinguished as a high quality, higher priced model`,
    },
    {
        "id": "3",
        "positiveFeature": `Improves flavor of coffee`,
    },
    {
        "id": "4",
        "positiveFeature": `Glass tube allows user to monitor flow of water and gives a visual indication when build up has occurred`,
    },
    {
        "id": "5",
        "positiveFeature": `Easy to operate and easy to clean`,
    },
    {
        "id": "7",
        "positiveFeature": `Two stage heating plate adjusts based off volume in carafe`,
    },
    {
        "id": "8",
        "positiveFeature": `Safety features include automatic shut off after 100 minutes`,
    },
    {
        "id": "9",
        "positiveFeature": `Built to last`,
    },
    {
        "id": "6",
        "positiveFeature": `Manufactured to meet the high standards of the Specialty Coffee Association of America`,
    },
],

item2Title = "Cuisinart DCC-3200 14-Cup Glass Carafe with Stainless Steel Handle Programmable Coffeemaker, Silver",
item2Description = "The Cuisinart DCC-3200 uses state of the art technology to deliver hotter coffee without sacrificing flavor or quality. This coffee makers has variable brew strength control that allows you to select between regular or bold coffee flavor. It is a programmable unit that can be automated to brew coffee at specified times so that it can be ready after you wake up. The unit has a self cleaning function, auto shut off that can be set between 0 and 4 hours, and the ability to self clean. The temperature can be adjusted and it comes standard with a gold tone, commercial style permanent filter so that you do not have to spend additional money of paper filters.",
href2="https://www.amazon.com/Cuisinart-DCC-3200-Stainless-Programmable-Coffeemaker/dp/B00MVWGQX0/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487869569&sr=1-1&keywords=Cuisinart+DCC-3200+14-Cup+Glass+Carafe+with+Stainless+Steel+Handle+Programmable+Coffeemaker,+Silver&linkCode=li3&tag=bestvoted-20&linkId=710c20fad623e7fbd00d540b5b793934",
pixel2="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00MVWGQX0",
item2url = url + "/" + encodeURIComponent('#')+"image2",
item2ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00MVWGQX0&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item2ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00MVWGQX0&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item2ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00MVWGQX0&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20" ,
item2WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Produces coffee that is more flavorful due to consistent flow and temperature`,
    },
    {
        "id": "2",
        "positiveFeature": `Adjustable temperature is able to get hotter than older Cuisinart DCC-1200 model`,
    },
    {
        "id": "3",
        "positiveFeature": `Can hold 2 more cups than the Cuisinart DCC-1200 model for a total of 14 cups`,
    },
    {
        "id": "4",
        "positiveFeature": `Brushed stainless steel on front and sides give it a richer and higher quality look in comparison to other coffee makers which lack this feature`,
    },
    {
        "id": "5",
        "positiveFeature": `Brushed stainless steel does not attract fingerprints as easily as polished stainless steel`,
    },
    {
        "id": "6",
        "positiveFeature": `Large shower head allows water to be distributed more evenly in comparison to other coffee makers with small shower heads`,
    },
    {
        "id": "7",
        "positiveFeature": `Coffeemaker can be programmed up to 24 hours in advance and is fully automatic`,
    },
    {
        "id": "8",
        "positiveFeature": `Self clean mode`,
    },
    {
        "id": "9",
        "positiveFeature": `Carafe can be removed while brewing to fill a cup of coffee for those in a hurry`,
    },
    {
        "id": "10",
        "positiveFeature": `No need to purchase coffee filters, this one comes with a gold tone filter for the coffee grounds and a charcoal water filter for the water`,
    },
],

item3Title = "Mr. Coffee BVMC-FM1 20-Ounce Frappe Maker",
item3Description = "The Mr. Coffee BVMC-FM1 is a cross between a coffee maker and a frappe maker. It is able to brew and blend at the touch of a button. It has a removable basket, a pulse button for control over the consistency of the blend, a recipe book, and a 20 ounce dishwasher safe blending pitcher. The maker has a small footprint and can fit in most any kitchen and the 4 cup basket uses standard coffee filters which are not included.",
item3url = url + "/" + encodeURIComponent('#') + "image3",
href3="https://www.amazon.com/Mr-Coffee-BVMC-FM1-20-Ounce-Frappe/dp/B002FWMF8G/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487869809&sr=1-1&keywords=Mr.+Coffee+BVMC-FM1+20-Ounce+Frappe+Maker&linkCode=li3&tag=bestvoted-20&linkId=bab4e558c19d96b032a46d04d4470a0b",
pixel3="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B002FWMF8G",
item3ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B002FWMF8G&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item3ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B002FWMF8G&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item3ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B002FWMF8G&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item3WhatPeopleLike = [
    {
        "id": "6",
        "positiveFeature": `Dual purpose unit will brew coffee and make frappes`,
    },
    {
        "id": "1",
        "positiveFeature": `Convenient way to make frappes from home saves time and money`,
    },
    {
        "id": "2",
        "positiveFeature": `Easy to use and clean`,
    },
    {
        "id": "3",
        "positiveFeature": `Comes with a permanent filter and a removable filter holder for easy cleaning`,
    },
    {
        "id": "4",
        "positiveFeature": `Blending jar is dishwasher safe`,
    },
    {
        "id": "5",
        "positiveFeature": `Customize frappes or coffee blends to your liking`,
    },
],


item4Title = "BUNN BX-D Velocity Brew 10-Cup Coffee Brewer, High Altitude",
item4Description = "The BUNN BX-D Velocityis a coffee maker that is specifically for high altitude brewing. Water boils at a lower temperature the higher altitude you are, and a normal coffee maker can result in the water being too hot for those in high altitude environments. It can brew up to 10 cups of coffee in approximately 3 minutes. This coffee maker was designed to stay on to keep the internal hot water tank heated and ready to go at a moments notice. It contains a unique sprayhead that gives even coverage over the coffee grounds and allows for full flavor extraction from the ground beans. The warming plate is porcelain-coated to help maintain temperature and combat rust. When the unit is on, the on/off switch is lighted to give visual indication.",
href4="https://www.amazon.com/BUNN-Velocity-10-Cup-Coffee-Altitude/dp/B000G9WK8K/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487870004&sr=1-1&keywords=BUNN+BX-D+Velocity+Brew+10-Cup+Coffee+Brewer,+High+Altitude&linkCode=li3&tag=bestvoted-20&linkId=5995b10c05b200a478e45767979c72fa",
pixel4="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B000G9WK8K",
item4url =url + "/" + encodeURIComponent('#') + "image4",
item4ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000G9WK8K&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item4ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000G9WK8K&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20" ,
item4ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000G9WK8K&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Specifically engineered for brewing coffee in high altitudes`,
    },
    {
        "id": "3",
        "positiveFeature": `Can brew 10 cups of coffee in approximately 3 minutes`,
    },
    {
        "id": "4",
        "positiveFeature": `Simple to use, no-frills design concentrates on producing good quality coffee at high altitudes`,
    },
    {
        "id": "2",
        "positiveFeature": `Lighted on/off switch gives immediate visual indication on when unit is on`,
    },
    {
        "id": "5",
        "positiveFeature": `Internal hot water tank is made from stainless steel`,
    },
    {
        "id": "6",
        "positiveFeature": `Simple design`,
    },

],

item5Title = "OXO Good Grips Cold Brew Coffee Maker",
item5Description = "The OXO Good Grips coffee maker produces a smooth, flavorful, low acid coffee concentrate. This concentrate is capable of creating both hot and cold coffee. Low acid coffee tends to be gentler on many users digestive system. The special distribution method allows water to be dispersed evenly over the entire grounds that helps maintain optimal flavor extraction. Hands are kept clean because this coffee maker uses an easy to access switch that activates the filtration process. Included with the OXO Good Grips is a silicone stopper. This helps the coffee to stay fresher for longer periods of time, particularly when stored in the refrigerator. The glass carafe is made from Borosilicate and displays measurement markings. Although this coffee maker has a small footprint, it can be disassembled for out of the way storage.",
href5="https://www.amazon.com/OXO-Good-Grips-Coffee-Maker/dp/B00JVSVM36/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487870230&sr=1-1&keywords=OXO+Good+Grips+Cold+Brew+Coffee+Maker&linkCode=li3&tag=bestvoted-20&linkId=97dc0bfbbe11e1dd252977ba5f7d2ad2",
pixel5="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00JVSVM36" ,
item5url = url + "/" + encodeURIComponent('#') + "image5",
item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00JVSVM36&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item5ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00JVSVM36&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00JVSVM36&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Great way to make cold brewed coffee`,
    },
    {
        "id": "2",
        "positiveFeature": `Coffee is less bitter and less acidic than standard brewed coffee`,
    },
    {
        "id": "3",
        "positiveFeature": `Uses a valve for dispensing instead of corks or rubber stoppers`,
    },
    {
        "id": "4",
        "positiveFeature": `Reusable mesh filter system`,
    },
    {
        "id": "5",
        "positiveFeature": `Capable of making very strong coffee`,
    },
    {
        "id": "6",
        "positiveFeature": `Unique rainmaker water dispersement system and transparent tank allows for even and balanced water coverage`,
    },
    {
        "id": "7",
        "positiveFeature": `Borosilicate glass carafe has a high tolerance to heat and is extremely resistant to severe temperature changes allowing it to go from hot brewed to refrigerated without cracking`,
    },
    {
        "id": "8",
        "positiveFeature": `Can keep cold coffee fresh in the refrigerator with included silicone seal`,
    },
    {
        "id": "9",
        "positiveFeature": `This coffee maker is versatile and can be used to make hot coffee or even tea`,
    },
    {
        "id": "10",
        "positiveFeature": `Stacks into itself for storage allowing it to take a small footprint when not in use`,
    },
];

class PageDisplayed extends React.Component{
    constructor(props) {
        super(props);
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links}/>
                <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>

                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike}/>

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}

module.exports = PageDisplayed
