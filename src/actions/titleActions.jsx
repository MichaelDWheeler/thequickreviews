export function showTitle(){
    return{
        type: "SHOW_TITLE"
    }
}
export function hideTitle(){
    return{
        type: "HIDE_TITLE"
    }
}
export function getLocation(pageLocation){
    return{
        type: "GET_LOCATION",
        payload: pageLocation,
    }
}
