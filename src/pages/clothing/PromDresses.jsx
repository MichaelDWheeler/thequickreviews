import React from "react";
import MetaTags from 'react-meta-tags';

import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";


var currentYear = function(){
    let d = new Date,
    thisYear = d.getFullYear();
    return thisYear;
};

let url = "promDresses",
title ="Best Prom Dresses",
image= "../../js/img/evening-dress.jpg",
mobileImage="../js/img/evening-dress.jpg",
parallaxAlt="Girl in red prom dress laying in chair waiting for her date",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of Prom Dresses We Compared</h3>
        <p>
            Make sure that you stand out on your special night and that you dazzle in all your pictures. With so many different styles to choose from, selecting a prom dress can quickly become overwhelming! Instead, take a look at the ones we have picked and picture yourself at the prom in each prom dress.  When we were deciding what types of prom dresses to look at, we did not have any particular style in mind. We realize that high school is full of unique individuals and that you most likely enjoy expressing your uniqueness. Of course, we know that most everyone likes to express themselves in their own special way but high school is where we feel it really shines. This is when teenagers are starting to explore their independence. Your own personal fashion is an easy way to get that self-expression across. But it can be daunting! When it comes to prom dresses, there are tens of thousands of dresses and color combination that a you can choose from. With that in mind, we scoured through thousands of dresses with no set preconceived notions so that we can present to you the top 5 prom dresses that we felt are the best for {currentYear()}. They vary in style. Some are more elegant while some are more playful. They all come in a variety of colors or patterns. To see additional colors or patterns for the various dresses you can visit the site directly.
        </p>
        </div>
    ],
    subject = "Prom Dresses",
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What To Look For In A Prom Dress</h3>
            <p>
                This night is about you, and you deserve to feel wonderful!  When selecting a prom dress, it is important to remember that the dress you select can be a huge factor in how you feel for the night. It is a special occasion and the memories you create can last a lifetime. Not only will you remember what you did, but you will remember how you felt! There is no better feeling than looking back and recalling how special you felt in your prom dress. With that in mind, consider the following:
            </p>
            <ul>
                <li>Make sure your prom dress fits you properly. If you are ordering online, be sure to order early enough so that you have time to make alterations if necessary. If you go to prom in a dress that is too large or too small, you will not be able to fully enjoy the night. The best prom dress is one that will fit you properly.</li>
                <li>Be certain that the dress is appropriate for your body shape. You know your body. Take that into account when ordering the dress. If you are self-conscience with certain parts of your body, be sure that the dress you order adequately addresses those parts. Some dresses will expose more body parts than others. Some are tighter and more form fitting on the waist or other areas. Unless you have the same body type as the model in the picture wearing the prom dress, do not mislead yourself into believing it will look identical on you. The shape of the body under the dress plays a huge role in determining how the overall dress will look. Let your body type help to determine the type of dress you pick so that it will be flattering on you. No matter what your body type, there is a dress that will accentuate it to help you look more beautiful.</li>
                <li>Do not ignore the manufacturer's sizing chart. There are a lot of popular dresses that are made in different countries. Often, they have sizing charts that run small in comparison sizing charts you may be familiar with. Do not let pride keep you from ordering the appropriate size based off measurements according to their sizing chart. If you typically wear a medium, but according to the sizing chart the manufacturer has provided it shows you need an extra-large, don't order the medium. You can always reach out to the manufacturer if you need clarification or reassurance that the sizing chart they have is correct by their standards.</li>
                <li>Choose a color that is flattering based upon your body shape, skin tone, your eye color, and your hair. The color of your dress can make the colors of your body stand out. For instance, a red dress on someone with blue eyes will make the blue eyes stand out much more. Determine what your best feature is, and then use the dress to make it stand out.</li>
                <li>Select a prom dress that will be appropriate for the activities you plan to do the most. If you know you will spend the entire night dancing, make sure you get a prom dress that will allow you to do it comfortably. If you tend to perspire more than other people, be sure the fabric is a breathable fabric. If you plan on sitting a good portion of the night, you want to be sure the dress is not so form fitting that it makes it uncomfortable to sit.</li>
                <li>Some of the prom dresses look the best with accessories that are being displayed in the picture. For instance, some prom dresses will show a fullness that caught your attention. This might be because a <a href="http://amzn.to/2npBTM7">pettitcoat</a> is underneath it.</li>
                <li>Don't assume you can remove an accessory that you do not like. For example, if you notice the prom dress has a belt and you would prefer not to wear it, do not order it assuming that it is removable. Some are sewn in. If you plan to wear the prom dress differently than pictured, make sure you can first.</li>
            </ul>
        </div>
    ],
    moreInfo = [
        <div>
            <a name="best-prom-dresses"></a>
            <h3>How To Look Your Best In Your Prom Dress</h3>
            <p>
                In this section we are going to give you some tips on how to look your best in your prom dress. These tips will be for the event as well as for pictures that you and others will look back on someday. Most of these tips are easily accomplished. Some of these tips require considerable preparations in advance, and may require a little bit of hard work. Rest assured that it will be worth it if you are successful.
            </p>
            <ol>
                <li>Exercise. For some, this may be the most difficult to accomplish. You should do this as far ahead in advance as losing weight and toning your body can take considerable time. The physical, mental and health benefits cannot be understated however. If you can stand to lose a few pounds and you do, this will give you an incredible boost of self-confidence. Self-confident individuals draw people to them. Self confidence is attractive. You will feel better, you will look better, and you will have more energy if you diet and exercise properly. You will be amazed at how much better you feel about yourself if you lose five or ten pounds if you need to. Even if you do not need to lose weight, exercising and eating properly will give you a mental boost and extra energy.</li>
                <li>Give your skin some extra attention. This is something that needs to be done in advance as well. Not only should you gently exfoliate and moisturize, you should also stay well hydrated and eat healthy foods as they affect the appearance of our skin. If the prom dress you are going to be wearing exposes sections of your body, be sure to take extra care on those sections. </li>
                <li>If you are going for a new look, make it a familiar look. This does not mean that you should look the way you always do, but it does mean you should become familiar with that look before you present it to the world. If you are trying on new make-up, or skin care products be sure you have used them in advance so you are familiar with the outcome. There are few things worse than going for a great new look only for it to be a disaster on your special day. Know the outcome before you show it to everyone.</li>
                <li>Going for a new hairstyle? Be certain the person who is styling it has plenty of experience with formal hair styles. Not all hair stylists can do updos. Some that can't will still take your money and attempt to. Last minute hair changes can be an absolute disaster. With the amount of strong hair spray and bobby pins that end up in your hair, it might not be possible to fix and make it to prom. If you are going for a new hair style, be sure the hair stylist is completely capable. Talk to them beforehand. Get referrals from friends who were happy with their hair stylist. Make sure your expectations are realistic. The picture of the model with the wavy hair may not work with hair that is unable to hold a curl.</li>
                <li>Practice smiling with your eyes as well as your mouth. The eyes give it away. If you are not comfortable smiling, practice a look that you are comfortable with that makes you look happy. Look in the mirror and practice so that you become familiar with how your face muscles feel when you are holding that facial pose. If all else fails, force yourself to laugh. Someone laughing in a picture looks like they are having a great time, and that is a great way to be remembered.</li>
                <li>Don't only take pictures at prom. Hopefully at prom, every picture looks perfect. But in reality, they may not. Take charge and make sure you have some perfect pictures by taking them at home. Keep taking them until you have a few that you love. You want to be able to look back on these pictures without negatively judging yourself. Even if it takes longer than you anticipated. Your future self will be thanking you.</li>
                <li>Stay away from products that produce shine. The camera flash is sure to catch it. Using makeup that contains translucent powder will help control the excessive oil that your skin will naturally produce as well as keep your makeup looking better for longer. <a href="http://amzn.to/2nkG27g">Buttercup Powder camera-ready with no ashy flashback</a> works incredibly well for those with medium to dark skin tones and <a href="http://amzn.to/2n7jhmV">BUTTERCUP LIGHT Powder, camera-ready with no ashy flashback</a> works incredibly well for those with light to medium skin tones.</li>
                <li>Above all else, have fun. Someone who is really having a good time and not faking it is attractive. Your personality can be the most attractive feature. Let yourself have a great time and let your personality shine on your special day.</li>
            </ol>
        </div>
    ],
    links = [
        {
            "id" : 3,
            "title" : "Our Top Picks",
            "tag" : "#picks",
        },
        {
            "id" : 1,
            "title" : "The Types Of Prom Dresses We Compared",
            "tag" : "#types",
        },
        {
            "id" : 2,
            "title" : "What To Look For In A Prom Dress",
            "tag" : "#features",
        },
        {
            "id" : 4,
            "title" : "How To Look Your Best In Your Prom Dress",
            "tag" :  "#best-prom-dresses",
            "extra" : "Additional Information",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ],
    item1Title = "Fllbridal Women's Beaded Sweetheart Lace Up Mermaid Prom Dresses",
    item1Description = "The top pick for this year is the Fllbridal Women's Beaded Sweetheart Lace Up Mermaid Prom Dress. The style is the classic mermaid style and this form fitting prom dress is blingbling beaded (rhinestones and sequins) to dazzle! The bottom material is organza, the back laces up and the remainder of the dress is satin. When ordering this dress, you can customize it to your specifications. If you want different color sparkles you can have them, you can request a zipper instead of the corset back, and you can even request to make the train bigger and puffier if you desire. It is a high-quality prom dress and is sure to make head turns. This prom dress does not come with a built-in bra. This prom dress is available in more colors than any other prom dress we have listed and can be purchased in 96 different colors. You can choose the color from a color swatch that is in the details page.",
    item1url = url + "/" + encodeURIComponent('#')+"image1",
    href1="https://www.amazon.com/Fllbridal-Sweetheart-Mermaid-XC009-Burgundy/dp/B01KNIBE5I/ref=as_li_ss_il?ie=UTF8&qid=1489420203&sr=8-1&keywords=B01KNIBE5I&linkCode=li3&tag=thequickreviews-20&linkId=4f35fb7c6f99f145b90f28c54f01851b",
    pixel1="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01KNIBE5I",
    item1ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KNIBE5I&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
    item1ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KNIBE5I&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
    item1ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KNIBE5I&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Stunningly beautiful dress can make you feel sexy yet elegant at the same time`,
        },
        {
            "id": "2",
            "positiveFeature": `High quality prom dress that is extremely well made`,
        },
        {
            "id": "3",
            "positiveFeature": `Prom dress appears expensive and is made with heavy materials`,
        },
        {
            "id": "4",
            "positiveFeature": `Form fitting prom dress compliments even curvy individuals`,
        },
        {
            "id": "5",
            "positiveFeature": `Can be worn for multiple formal occasions`,
        },
    ],

    item1WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Prom dress may be too tight for some and may be difficult to sit in depending on body type`,
        },
        {
            "id": "3",
            "negativeConcern": `This dress does not have straps`,
        },
    ],

    item2Title = "Miusol Women's Elegant Illusion Floral Lace Cap Sleeve Prom Dress",
    item2Description = "The Miusol Elegant prom dress can be purchased in navy blue, wine, or dark green. The inside of the prom dress is made from cotton, nylon, spandex. The outside is 100% nylon. It should be hand washed only. It is an imported dress that is elegant yet simplistically beautiful. It has a see-through design with cap sleeves, swing width, and it is knee length for that playful appearance. It does not have a built-in bra.",
    item2url = url + "/" + encodeURIComponent('#')+"image2",
    href2="https://www.amazon.com/Miusol-Womens-Elegant-Illusion-Bridesmaid/dp/B019IBMLXU/ref=as_li_ss_il?ie=UTF8&qid=1489424270&sr=8-1&keywords=B019IBMLXU&linkCode=li3&tag=thequickreviews-20&linkId=57e8a0d0658b81aa74b719d590bcdebb",
    pixel2="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B019IBMLXU",
    item2ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019IBMLXU&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019IBMLXU&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019IBMLXU&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Classic yet playfully styled prom dress`,
        },
        {
            "id": "2",
            "positiveFeature": `High quality materials and craftsmanship`,
        },
        {
            "id": "3",
            "positiveFeature": `Heavy and somewhat stretchy material`,
        },
        {
            "id": "4",
            "positiveFeature": `Beautiful overlay lace is sheer and light`,
        },
        {
            "id": "5",
            "positiveFeature": `Great prom dress for maintaining modesty and decency while not looking frumpy in the least`,
        },
    ],

    item3Title = "Dresstells Long Prom Dress Asymmetric Organza Gown",
    item3Description = "This stunning long prom dress comes in 22 different colors for this style. It is created with organza and lace and should be hand washed and then hung to dry. It has an A-line silhouette, is sleeveless, and has an elegant scoop neckline. The dresses upper section is gorgeously embellished with beads and the bodice is lacey. The spaghetti straps in back add to the sexy yet elegant appearance of this prom dress and the asymmetric hemline is a flattering feature. This beautifully crafted prom dress is certain to turn heads and get compliments. This dress has a built-in bra that can accommodate even DD size chests.",
    item3url = url + "/" + encodeURIComponent('#')+"image3",
    href3="https://www.amazon.com/Dresstells-Asymmetric-Blue-Size-22W/dp/B018G5C3ZU/ref=as_li_ss_il?ie=UTF8&qid=1489424403&sr=8-1&keywords=B018G5C3ZU&linkCode=li3&tag=thequickreviews-20&linkId=f5fbecd23c57cbfd2ed45b611e70f4f7",
    pixel3="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B018G5C3ZU",
    item3ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B018G5C3ZU&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B018G5C3ZU&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B018G5C3ZU&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Stunning, well crafted, long length dress`,
        },
        {
            "id": "2",
            "positiveFeature": `Designed with actual glass beads incorporated in this prom dress`,
        },
        {
            "id": "3",
            "positiveFeature": `High quality prom dress made with heavy material`,
        },
        {
            "id": "4",
            "positiveFeature": `Appears expensive`,
        },
        {
            "id": "5",
            "positiveFeature": `Attention to detail is spectacular such as the layered flowing skirt, open back with spaghetti straps, and the beads and sequins incorporated into design`,
        },
        {
            "id": "6",
            "positiveFeature": `Exceptional craftsmanship, prom dress appears to be made at specialty store`,
        },
        {
            "id": "7",
            "positiveFeature": `When placing an order, the company will accept custom requests`,
        },
    ],
    item3WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Alterations are possible but may be difficult due to the way this prom dress is made`,
        },
        {
            "id": "2",
            "negativeConcern": `Women with smaller chests should ask about customizing dress prior to ordering`,
        },
        {
            "id": "3",
            "negativeConcern": `A petticoat may be required as certain conditions may allow the top layers of the dress to be transparent`,
        },
    ],

    item4Title = "SeasonMall Women's Two Pieces Bateau Beaded Bodice Tulle Prom Dress",
    item4Description = "The SeasonMall prom dress is a throwback to a vintage error where sexy meets elegant. It is handmade dress that is exceptionally crafted and stunningly beautiful. The quality of this prom dress is guaranteed. This dress can be ordered in 6 different colors, and is a modern take on a vintage look. It is a 2 piece, sleeveless dress that zippers up on the side. It will take up to two weeks when ordering this dress so you want to make sure you order it with enough time left before prom.",
    item4url = url + "/" + encodeURIComponent('#')+"image4",
    href4="https://www.amazon.com/SeasonMall-Womens-Pieces-Beaded-Bodice/dp/B01CHZVUI6/ref=as_li_ss_il?ie=UTF8&qid=1489424675&sr=8-1&keywords=B01CHZVUI6&linkCode=li3&tag=thequickreviews-20&linkId=5ae2c10bcd3cb14e9009ab8b7967901c",
    pixel4="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01CHZVUI6",
    item4ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01CHZVUI6&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01CHZVUI6&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01CHZVUI6&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `High quality prom dress`,
        },
        {
            "id": "2",
            "positiveFeature": `Excellent quality and craftsmanship`,
        },
        {
            "id": "3",
            "positiveFeature": `Features such as the side zipper, finished and lined edges, and multiple layers of tulle add to the overall beauty of this prom dress`,
        },
        {
            "id": "4",
            "positiveFeature": `True to sizing chart`,
        },
    ],
    item4WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Skirt may run long for some and might need alterations`,
        },
    ],

    item5Title = "Dressystar Vintage Polka Dot Retro Cocktail Prom Dresses 50's 60's Rockabilly Bandage",
    item5Description = "This playful and stylish 1950's prom dress adds fun with form and comes in 27 different colors/patterns to allow your personality shine through. It is an extremely popular and well liked prom dress that works well with a petticoat or underskirt if extra fluff is desired. The middle band  of the prom dress acts somewhat as a belt, is removable, and allows this prom dress to fit any girls who are concerned with their midsection.",
    item5url = url + "/" + encodeURIComponent('#')+"image5",
    href5="https://www.amazon.com/Dressystar-Vintage-Cocktail-Rockabilly-XS/dp/B01FS9NMLQ/ref=as_li_ss_il?ie=UTF8&qid=1489424889&sr=8-1&keywords=B01FS9NMLQ&linkCode=li3&tag=thequickreviews-20&linkId=5abc1804c03d37cb060ca9f5041070d6",
    pixel5="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01FS9NMLQ",
    item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01FS9NMLQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01FS9NMLQ&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01FS9NMLQ&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Made from light-weight and incredibly comfortable material that is not transparent`,
        },
        {
            "id": "2",
            "positiveFeature": `Vintage style dress is great for many different occasions, not only for prom`,
        },
        {
            "id": "3",
            "positiveFeature": `Back lacing allows dress to be adjustable`,
        },
        {
            "id": "4",
            "positiveFeature": `Great design with multiple colors and patterns available`,
        },
    ],
    item5WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `To get full retro effect as pictured a petticoat is required`,
        },
        {
            "id": "2",
            "negativeConcern": `Sizes are not based on American sizing chart`,
        },
        {
            "id": "3",
            "negativeConcern": `Belt is just a ribbon and not sewn in`,
        },
    ];

    class PageDisplayed extends React.Component{
        constructor(props) {
            super(props);
        }

        date(){
            let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate =`${month} ${day}, ${year}`;
            return newDate;
        }

        year(){
            let d = new Date,
            thisYear = d.getFullYear();
            return thisYear;
        };

        quantity() {
            if (typeof item10Title !== 'undefined') {
                return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
            } else {
                return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
            }
        };

        render() {
            return (
                <div>
                    <MetaTags>
                        <title>{title}</title>
                        <meta id="meta-description" name="description" content={this.quantity()} />
                    </MetaTags>
                    <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                    <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                    <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                    <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                    <PageNavigation links={links}/>

                    <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                    <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                    <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                    <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                    <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                    <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                    <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                    <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                    <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>

                    <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                    <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike}/>

                    <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                    <AdrienneWheeler />

                    <Disqus url={url} id={url + "1"} subject={subject} />
                </div>
            )
        }
    }


    module.exports = PageDisplayed
