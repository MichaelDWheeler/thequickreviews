import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Redirect, IndexRoute, browserHistory } from 'react-router';
import ReactGA from 'react-ga';
import { Provider} from 'react-redux';
import Attribution from '../components/Attribution.jsx';
import BestCatToys from '../pages/cats/BestCatToys.jsx';
import BestCatHarness from '../pages/cats/BestCatHarness.jsx';
import BestFleaTreatmentForDogs from '../pages/dogs/BestFleaTreatmentForDogs.jsx';
import CoffeeMakers from '../pages/kitchen/CoffeeMakers.jsx';
import BestDogCrate from '../pages/dogs/BestDogCrate.jsx';
import EveningDresses from '../pages/clothing/EveningDresses.jsx';
import ElectricPressureCookers from '../pages/kitchen/ElectricPressureCookers.jsx';
import Featured from '../pages/Featured.jsx';
import GlutenFreeBread from '../pages/glutenFreeFood/GlutenFreeBread.jsx';
import GlutenFreeBreadMix from '../pages/glutenFreeFood/GlutenFreeBreadMix.jsx';
import GlutenFreeSnacks from '../pages/glutenFreeFood/GlutenFreeSnacks.jsx';
import GlutenFreeProteinBars from '../pages/glutenFreeFood/GlutenFreeProteinBars.jsx';
import GlutenFreePasta from '../pages/glutenFreeFood/GlutenFreePasta.jsx';
import Layout from '../components/Layout.jsx';
import NotFound from '../pages/NotFound.jsx';
import NaturalDeodorant from '../pages/personal/NaturalDeodorant.jsx';
import SafetyRazors from '../pages/personal/SafetyRazors.jsx';
import PromDresses from '../pages/clothing/PromDresses.jsx';

import store from '../stores/Store.jsx';

ReactGA.initialize('UA-92142805-1');

let requireAllImages = function(r){
    r.keys().forEach(r);
};

requireAllImages(require.context('../images/', true, /\.jpg|JPG|png|svg$/));

const app = document.getElementById('app');

let logPageView = function (){
    ReactGA.set({ page: window.location.pathname });
    ReactGA.pageview(window.location.pathname);
};

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory} onUpdate={logPageView}>
            <Route path="/" component={Layout}>
                <IndexRoute component={Featured}></IndexRoute>
                <Route path="attribution" component={Attribution}></Route>
                <Route path="best-dog-crate" component={BestDogCrate}></Route>
				<Route path="best-cat-toys" component={BestCatToys}></Route>
				<Route path="best-cat-harness" component={BestCatHarness}></Route>
                <Route path="best-flea-treatment-for-dogs" component={BestFleaTreatmentForDogs}></Route>
                <Route path="coffeeMakers" component={CoffeeMakers}></Route>
                <Route path="electricPressureCookers" component={ElectricPressureCookers}></Route>
                <Route path="eveningDresses" component={EveningDresses}></Route>
                <Route path="glutenFreeBread" component={GlutenFreeBread}></Route>
                <Route path="glutenFreeBreadMix" component={GlutenFreeBreadMix}></Route>
                <Route path="glutenFreeSnacks" component={GlutenFreeSnacks}></Route>
                <Route path="glutenFreeProteinBars" component={GlutenFreeProteinBars}></Route>
                <Route path="glutenFreePasta" component={GlutenFreePasta}></Route>
                <Route path="naturalDeodorant" component={NaturalDeodorant}></Route>
                <Route path="promDresses" component={PromDresses}></Route>
                <Route path="best-safety-razor" component={SafetyRazors}></Route>
                <Route path="*" component={NotFound}></Route>
            </Route>
        </Router>
    </Provider>
,
app);
