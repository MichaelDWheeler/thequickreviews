import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";


import JumpTo from "../../components/JumpTo.jsx";

let url = "glutenFreeProteinBars",
title ="Best Gluten Free Protein Bars",
image= "../../js/img/glutenFreeBackground.jpg",
mobileImage="../js/img/glutenFreeBackground.jpg",
parallaxAlt="Grains of various colors and types in small piles",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of Gluten Free Protein Bars We Compared</h3>
        <p>
            Protein bars come in all flavors, sizes, and can differ wildly in protein content and calories. Many of the protein bars that are on the market offer minimal protein and still market the bars as protein bars. For this particular review we compared 10 gluten free protein bars that were calorie dense, high protein bars that are often used by those who are trying to increase their protein intake. We did not include "protein" bars that used the term because the bar happened to have a bit of protein in it from the ingredients. The bar needed to have a considerable amount of protein in it to be considered for our comparison. All but 3 of the gluten free protein bars we compared have at least 20 grams of protein. To be included in our comparison, the protein bar had to contain at minimum 10 grams of protein per bar.
        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Do We Feel Makes The Best Gluten Free Protein Bars?</h3>
        <p>
            Our main criteria for selecting the best gluten free protein bar was the amount of protein each one actually contained, so those that had more scored higher. Some of the bars did not have as much protein but they still had enough that they were a great source of protein. They were very tasty and easy to eat, so we included them. Obviously taste played a huge role in our decision for what gluten free protein bar is the best, and it was the deciding factor when comparing similar protein content bars. Also important to us is digestibility. It is not fun eating a protein bar and having digestive issues as a result. Fortunately the bars being gluten free do help a bit in that area but bars that contain whey derived from milk can also cause issues to those who are lactose intolerant. If the bar was able to have a high content of protein while not using whey protein, we gave it higher marks.
        </p>
    </div>
],
item1Title = "PROBAR BASE Gluten Free Protein Bar, Cookie Dough - 12 Bars",
item1Description = "For us, this bar is the best gluten free protein bar because it had all the criteria we were looking for. First and foremost, it is gluten free. Next, it has 20 grams of protein. We only have one bar listed in our comparison that has more protein and it is only by 1 gram. We loved the flavor of this cookie dough bar, it almost felt guilty to us. It is one of the more calorie dense bars at 290 calories and can be used as a meal replacement or meal on the go. Many protein bars are difficult to digest for those of us who are lactose intolerant. This is an exception. This bar does not contain whey protein and it has 4 grams of fiber which can aid in digestion. As an added bonus, it is non-GMO verified.",
href1="https://www.amazon.com/PROBAR-Protein-Cookie-Dough-Ounce/dp/B00B97A4C2/ref=as_li_ss_il?ie=UTF8&qid=1489771313&sr=8-12&keywords=gluten+free+protein+bars&th=1&linkCode=li3&tag=thequickreviews-20&linkId=76f9f7bc5fe341b242fc2919c98379da",
item1url = url + "/" + encodeURIComponent('#')+"image1",
item1ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00B97A4C2&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel1="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00B97A4C2",
item1ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00B97A4C2&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item1ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00B97A4C2&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item1WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `20 grams of protein per bar`,
    },
    {
        "id": "2",
        "positiveFeature": `Easy to digest as it does not contain whey protein`,
    },
    {
        "id": "3",
        "positiveFeature": `Yummy cookie dough flavor`,
    },
    {
        "id": "4",
        "positiveFeature": `Calorie dense bar can be used as meal replacement/meal on the go`,
    },
    {
        "id": "5",
        "positiveFeature": `Contains 4 grams of fiber, not too much but not too little`,
    },
    {
        "id": "6",
        "positiveFeature": `Convenient way to have a high protein breakfast`,
    },
],
item2Title = "Nogii High Protein Bar Gluten Free, Peanut Butter and Chocolate - 12 Bars",
item2Description = "Our 2nd pick was the gluten free Nogii high protein bar due to the large content of protein, the great taste, and the lack of certain unhealthy ingredients. This high protein bar contains 20 grams of protein. It is a great bar for those who have an active and healthy lifestyle. They can be eaten as a high protein breakfast, between meals, and can be eaten after exercising for protein recovery. The best part to us is that it is a high protein gluten free bar that tastes great! Having a delicious and healthy reward to look forward to while working out can really help to push you those last few minutes because you know it is going to be worth it. This bar as a good balance ratio of fat at 8 grams, protein at 20 grams, and carbohydrates at 20 grams. This ratio is maintained without the use of trans fats, hydrogenated oils, or high-fructose corn syrup. This bar is a tasty mix of chocolate and peanut butter and you can really taste each flavor.",
href2="https://www.amazon.com/Protein-Gluten-Peanut-Butter-Chocolate/dp/B004FR6P30/ref=as_li_ss_il?ie=UTF8&qid=1489771544&sr=8-37&keywords=gluten+free+protein+bars&th=1&linkCode=li3&tag=thequickreviews-20&linkId=6a19ffb3258520a9b0a9daee59a7b68e",
item2url = url + "/" + encodeURIComponent('#')+"image2",
item2ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004FR6P30&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel2="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B004FR6P30",
item2ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004FR6P30&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item2ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004FR6P30&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item2WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `20 grams of protein per bar`,
    },
    {
        "id": "2",
        "positiveFeature": `Chocolate and peanut butter flavor`,
    },
    {
        "id": "3",
        "positiveFeature": `No trans-fats, hydrogenated oils, or high-fructose corn syrup`,
    },
    {
        "id": "4",
        "positiveFeature": `Each gluten free protein bar is made with all natural ingredients`,
    },
    {
        "id": "5",
        "positiveFeature": `Great bar for protein recovery`,
    },
    {
        "id": "6",
        "positiveFeature": `Only 230 calories per bar for those watching calories`,
    },
    {
        "id": "7",
        "positiveFeature": `Great for use as a meal on the go`,
    },

],
item3Title = "Promax Bar Gluten Free Nutty Butter Crisp Protein Bar - 12 Bars",
item3Description = "This box of 12 gluten free protein bars is certified to be gluten free, Kosher, and Vegetarian. Each bar is 300 calories and contains 20 grams of protein and 18 vitamins and minerals. Promax Nutty Butter Crisp bars have a 2:1 carbohydrate to protein ratio and are great for both energy and post workout recovery. They have a great nutty butter flavor with a chewy and crisp texture. The bars do not contain artificial sweeteners or preservatives. Although the bars use only natural sweeteners, they are higher on the sugar content coming in at 26 grams.",
href3="https://www.amazon.com/Promax-Nutty-Butter-Crispy-Count/dp/B001H0G0BK/ref=as_li_ss_il?ie=UTF8&qid=1489771426&sr=8-20&keywords=gluten+free+protein+bars&th=1&linkCode=li3&tag=thequickreviews-20&linkId=05469a64b3e77b65534884fca00a42c0",
item3url = url + "/" + encodeURIComponent('#')+"image3",
item3ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001H0G0BK&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel3="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B001H0G0BK",
item3ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001H0G0BK&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item3ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001H0G0BK&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item3WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Each gluten free bar contains 20 grams of protein`,
    },
    {
        "id": "2",
        "positiveFeature": `300 calories per bar`,
    },
    {
        "id": "3",
        "positiveFeature": `Great nutty butter flavor is chewy but crispy at the same time`,
    },
    {
        "id": "4",
        "positiveFeature": `No artificial sweeteners or preservatives are used`,
    },
    {
        "id": "5",
        "positiveFeature": `Can be used as an energy bar or as a recovery bar`,
    },
    {
        "id": "6",
        "positiveFeature": `Provides more than just protein. Contains 18 vitamins and minerals`,
    },

],
item4Title = "Quest Nutrition Gluten Free Protein Bar, Chocolate Chip Cookie Dough - 12 Bars",
item4Description = "The Quest Nutrition Chocolate Chip Cookie Dough gluten free bars have the most protein and the least calories out of all the bars we compared. This is an extremely popular gluten free protein bar. These bars have 21 grams of protein, only 20 grams of carbohydrates, and only 190 calories which makes them excellent as a post recovery workout snack. Instead of using sugar as a sweetener, these gluten free bars are sweetened with Erythritol, Stevia, and Sucralose. They are made with high quality whey and milk protein isolates and do not contain any soy. While they seem to be a bit smaller than some of the other bars we compared, we were pleasantly surprised that these bars could be used as a meal replacement because it did seem to hold us over for a few hours.",
href4 = "https://www.amazon.com/Quest-Nutrition-Protein-Chocolate-Cookie/dp/B00DLDH1N2/ref=as_li_ss_il?ie=UTF8&th=1&linkCode=li3&tag=thequickreviews-20&linkId=a3cfce744aa176ce624e9f539473015b",
item4url = url + "/" + encodeURIComponent('#')+"image4",
item4ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DLDH1N2&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel4="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00DLDH1N2",
item4ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DLDH1N2&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item4ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DLDH1N2&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `21 grams of protein - highest content of protein out of all the bars we compared`,
    },
    {
        "id": "2",
        "positiveFeature": `190 calories per bar`,
    },
    {
        "id": "3",
        "positiveFeature": `Soft and dense bar with great cookie dough flavor`,
    },
    {
        "id": "4",
        "positiveFeature": `All natural bar contains real chocolate, Cocoa Butter, Almonds `,
    },

],
item5Title = "Rise Bar Gluten-Free, High-Protein Bars, Almond Honey - 12 Bars",
item5Description = "These Rise gluten free protein bars are almond honey flavored and pack 20 grams of protein, 20 grams of carbohydrates and 16 grams of fat. They are great to use pre or post workout. They are made with natural ingredients, are non-GMO, soy free, peanut free, gluten free and kosher. The sweetness of the bar comes from natural flavors as there are no artificial flavors, no sugar alcohols, or preservatives. The protein from the bars are derived from whey and pea protein isolate. All Rise bars are made with 100% real food ingredients (ingredients you can recognize with names you can pronounce) and are made simple by using 5 or less ingredients.",
href5="https://www.amazon.com/Gluten-Free-High-Protein-Almond-Honey-12-Count/dp/B004X2LH9Y/ref=as_li_ss_il?ie=UTF8&th=1&linkCode=li3&tag=thequickreviews-20&linkId=11f711b6fd242f968e0f6be7ad151a45",
item5url = url + "/" + encodeURIComponent('#')+"image5",
item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X2LH9Y&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel5="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B004X2LH9Y",
item5ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X2LH9Y&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X2LH9Y&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Simple bar with minimal ingredients`,
    },
    {
        "id": "2",
        "positiveFeature": `All natural ingredients`,
    },
    {
        "id": "3",
        "positiveFeature": `Works well for holding you over between meals`,
    },
    {
        "id": "4",
        "positiveFeature": `Great way to start the day with a high protein breakfast bar`,
    },
    {
        "id": "5",
        "positiveFeature": `Not overly sweet, just sweet enough`,
    },
],

item6Title = "FitJoy Nutrition Gluten Free Protein Bar 6 Flavors Variety Pack - 6 Bars",
item6Description = "Do you get bored of eating the same food twice in a row? If you do, then these gluten free protein bars may be just what you are looking for. Some people like consistency, while others like adventure. Get both with these high protein bars. They all consistently pack 20 grams of protein, 220 - 230 calories, and 10 - 13 grams of dietary fiber in each bar. To spice it up theses gluten free protein bars come in a variety pack so you can have a different flavor 6 days out of the week. This 6 pack contains Chocolate Iced Brownie, Chocolate Chip Cookie Dough, Chocolate Peanut Butter, Frosted Cinnamon Roll, Mint Chocolate Crisp, and French Vanilla Almond. All of the bars are non-GMO, and do not contain any artificial colors, flavors or sweeteners.",
href6="https://www.amazon.com/FitJoy-Nutrition-Protein-Flavors-Variety/dp/B01MDRZB7V/ref=as_li_ss_il?ie=UTF8&qid=1489771426&sr=8-18&keywords=gluten+free+protein+bars&linkCode=li3&tag=thequickreviews-20&linkId=39045cc873234d64d2645fc4f888ba5f",
item6url = url + "/" + encodeURIComponent('#')+"image6",
item6ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MDRZB7V&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel6="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01MDRZB7V",
item6ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MDRZB7V&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item6ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MDRZB7V&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item6WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Variety pack allows you to sample different flavors`,
    },
    {
        "id": "2",
        "positiveFeature": `Great for post workout recovery`,
    },
    {
        "id": "3",
        "positiveFeature": `Only 7 - 8 grams of fat per bar`,
    },
    {
        "id": "4",
        "positiveFeature": `Only 220 - 230 calories per bar`,
    },
    {
        "id": "5",
        "positiveFeature": `Satisfying snack that can hold you over between meals`,
    },
    {
        "id": "6",
        "positiveFeature": `non-GMO`,
    },
],

item7Title = "Paleo Gluten Free Protein Bar, Organic SunButter - 12 Bars",
item7Description = "The Paleo Organic SunButter gluten free protein bar only contains 150 calories yet it delivers 20 grams of protein. The protein in these bars is derived from egg whites and not whey which can be beneficial to those with a lactose sensitivity. This bar is low in calories but it does an excellent job of curbing the appetite. Because this bar only contains 150 calories, it can easily be used for a snack in between meals or it can be used as a meal replacement altogether. The bar is simply made with only 4 ingredients and has a creamy peanut butter taste from those ingredients. We like this bar because it is low in fat, high in dietary fibers and contains 28 grams of carbs so it can work well for pre-workout energy and post-workout recovery.",
href7="https://www.amazon.com/Paleo-Protein-Organic-SunButter-White/dp/B01IBSYTIG/ref=as_li_ss_il?ie=UTF8&qid=1489771313&sr=8-11&keywords=gluten+free+protein+bars&linkCode=li3&tag=thequickreviews-20&linkId=971301faf23a52b4e9f262aa3b97b10c" ,
item7url = url + "/" + encodeURIComponent('#')+"image7",
item7ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IBSYTIG&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel7="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01IBSYTIG" ,
item7ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IBSYTIG&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item7ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IBSYTIG&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item7WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Only 150 calories per bar`,
    },
    {
        "id": "2",
        "positiveFeature": `23 grams of dietary fiber`,
    },
    {
        "id": "3",
        "positiveFeature": `Only 3 grams of fat`,
    },
    {
        "id": "4",
        "positiveFeature": `Low calorie bar can curb appetite up to 4 hours`,
    },
    {
        "id": "5",
        "positiveFeature": `Creamy peanut butter flavor`,
    },
    {
        "id": "6",
        "positiveFeature": `Non-GMO`,
    },
    {
        "id": "7",
        "positiveFeature": `Organic prebiotics help the digestive system`,
    },
    {
        "id": "8",
        "positiveFeature": `Protein is derived from egg whites`,
    },
    {
        "id": "9",
        "positiveFeature": `Only 4 ingredients (egg whites for protein, prebiotic fiber from tapioca for digestion, sunflower seed butter and monk fruit extract for texture and flavor)`,
    },
],

item8Title = "The GFB Gluten Free Protein Bars, Oatmeal Raisin - 12 Bars",
item8Description = "This oatmeal raisin protein bar contains 12 grams of protein in each delicious bar. It is made with wholesome and organic ingredients. Each healthy bar is gluten free, soy free, dairy free, trans-fat free, cholesterol free, vegan, Kosher and non-GMO. It is a great bar for giving you additional protein and energy and works excellent as a pre-workout snack or for anyone trying to increase their protein intake.",
href8="https://www.amazon.com/Gluten-Free-Non-GMO-Protein-Oatmeal/dp/B008NU5180/ref=as_li_ss_il?ie=UTF8&th=1&linkCode=li3&tag=thequickreviews-20&linkId=a6ee88072edcd381eef1df000dcb6520" ,
item8url = url + "/" + encodeURIComponent('#')+"image8",
item8ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B008NU5180&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
pixel8="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B008NU5180",
item8ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B008NU5180&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item8ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B008NU5180&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item8WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Delicious gluten free protein bar has the right mix of cinnamon and raisins`,
    },
    {
        "id": "2",
        "positiveFeature": `Plant based protein does not leave any type of whey protein after taste`,
    },
    {
        "id": "3",
        "positiveFeature": `220 calories per bar`,
    },
    {
        "id": "4",
        "positiveFeature": `Nice moist texture`,
    },
    {
        "id": "5",
        "positiveFeature": `Does not contain the common allergens that effect some people (soy, wheat, dairy, and eggs)`,
    },
    {
        "id": "6",
        "positiveFeature": `Can be included in breakfast as a healthy alternative to other protein sources`,
    },
],

item9Title = "Luna Protein Gluten Free Protein Bar, Chocolate Peanut Butter - 12 Bars",
item9Description = "These gluten free protein bars contain 12 grams of protein and are low glycemic which means they digest slowly for extended periods of energy. Each bar contains 3 grams of fiber and has a peanut butter taste that you would expect with a delicate crunch added. Each bar is made with wholesome and organic ingredients and contains vitamins and minerals that are are especially important for women's nutritional needs. The bars do not contain any trans-fats, hydrogenated oils, high-fructose corn syrup, artificial flavors, artificial preservatives, and are non-GMO." ,
href9="https://www.amazon.com/LUNA-PROTEIN-Gluten-Protein-Chocolate/dp/B0074B4H6Q/ref=as_li_ss_il?ie=UTF8&qid=1489771313&sr=8-7&ppw=fresh&keywords=gluten+free+protein+bars&linkCode=li3&tag=thequickreviews-20&linkId=50805d555a70428498d5d535c27a461b",
item9url = url + "/" + encodeURIComponent('#')+"image9",
item9ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0074B4H6Q&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel9="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B0074B4H6Q" ,
item9ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0074B4H6Q&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item9ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0074B4H6Q&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item9WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Smaller version of popular CLIF bar that is specifically formulated for women's nutritional needs`,
    },
    {
        "id": "2",
        "positiveFeature": `Low glycemic bar can give prolong levels of energy which makes it a great pre-workout bar`,
    },
    {
        "id": "3",
        "positiveFeature": `Low glycemic bar can give prolong levels of energy which makes it a great pre-workout bar`,
    },
    {
        "id": "4",
        "positiveFeature": `190 calorie bar can be used as a healthy snack in between meals`,
    },
],

item10Title = "NuGo Dark Gluten Free Protein Bar, Chocolate Chocolate Chip - 12 Bars",
item10Description = "Each NuGo Dark Chocolate Chip protein bar has 10 grams of protein which was the minimum to make our comparison. It also contains 5 grams of fat, and 29 grams of carbohydrates of which 14 grams are sugar. They are made with real cacao dark chocolate which is known to be a healthier chocolate because of the flavanol antioxidants it contains. These bars are vegan friendly, gluten free, completely natural and certified Kosher. These bars are a great snack for those who are looking to increase their protein intake",
href10="https://www.amazon.com/NuGo-Dark-Chocolate-Dairy-1-76-Ounce/dp/B001DGYKG0/ref=as_li_ss_il?ie=UTF8&qid=1489771313&sr=8-8&keywords=gluten+free+protein+bars&th=1&linkCode=li3&tag=thequickreviews-20&linkId=d452ea3b13d29eccee203c9041503035",
item10url = url + "/" + encodeURIComponent('#')+"image10",
item10ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001DGYKG0&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel10="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B001DGYKG0",
item10ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001DGYKG0&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item10ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001DGYKG0&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item10WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Made with cocao chocolate`,
    },
    {
        "id": "2",
        "positiveFeature": `Non-dairy protein bar`,
    },
    {
        "id": "3",
        "positiveFeature": `Great tasting snack that adds a good amount of protein`,
    },
],

subject = "Gluten Free Protein Bars",
moreInfo = [
    <div>
        <a name="store"></a>
        <h3>How Are The Best Gluten Free Protein Bars Different Than The Best Gluten Free Energy Bars?</h3>
        <p>
            Gluten free protein bars are not the same as gluten free energy bars. This applies to what we selected as the best gluten free protein bars as well as all protein bars in general. A common misconception about protein is that it gives you energy. Protein does not give you energy when you are sufficiently nourished. Foods that contain protein often also contain other nutrients that do give energy, and this may be where the confusion comes from. Under certain conditions the body will use the protein you have ingested to convert the amino acids it breaks down to into energy. These conditions aren't necessarily good for you either. Such conditions would be where your body is depleted of other energy sources and needs energy to sustain itself, so it resorts to the amino acids from the protein. Before your body can use the amino acids as an energy source it needs to remove the nitrogen to convert it to glucose or fatty acids which takes an extra step and is therefore longer. So, while technically protein can give you energy, when you eat a protein bar usually you are getting the energy from the other nutrients in the protein bar. If the protein bar has those other energy giving nutrients and the person who consumes it is calling it a protein bar, it is easy to see why they would believe protein gave them energy.
        </p>
        <p>
            While many of the best gluten free protein bars do have ingredients that give you energy, not all energy bars have protein in them. The best gluten free protein bars are often used pre-workout or for post-workout recovery. Before and after exercising your body may need some extra nutrition to work optimally and the makers of the protein bars know this so they include carbohydrates in the protein bar that will give you the energy you need. This does not hold true for the energy bars. The main goal of an energy bar is to provide you with energy, and since protein is not a great way for providing energy and can often be expensive it is not always included. More often than not, it is not included and is only present because the natural ingredients contain protein. The energy bar might not be advertised as a protein bar, but sometimes the label will market the amount of protein it has in it.
        </p>
        <a name="protein"></a>
        <h3>What Different Types Of Proteins Are Used In The Best Gluten Free Protein Bars?</h3>
        <p>
            The best gluten free protein bars contain the same protein found in other protein bars, although you may notice that certain protein sources being favored over others. The reason for this is because those who are sensitive to gluten may also be sensitive to dairy or have other allergies and the manufacturers realize this. By creating a protein bar that has as few ingredients as possible so as not to trigger health issues they can market to a specific group of people.
        </p>
        <p>
            Different sources of protein which are found in gluten free protein bars, or can be used when making your own gluten free protein bars include:
        </p>
        <ul>
            <li>
                Casein Protein - This protein is known for being a slowly digesting protein. It is a protein that is made from milk. One of the great advantages of casein protein in gluten free protein bars is that ounce for ounce, you should feel less hungry for longer periods of time when compared to other protein sources. A great time to eat a gluten free protein bar that uses casein would be right before going to bed. This would allow you to not go to bed hungry and allow your body to recover throughout the night as it is slowly digested. There are some people who are allergic to casein, and those who are would be advised to not use any product that use it as a protein source including gluten free protein bars.
            </li>
            <li>
                Egg Albumin - AKA egg white. This is the clear portion that is found in an egg. This clear substance makes up around 2/3 of a chicken's egg, and is approximately 90% water. A U.S. large egg contains roughly 3.6 grams of protein in the egg albumin. It is a great source of protein that has almost no fat and very little carbohydrates (less than 1%). It has a high content of amino acids and is often used in products when additional protein is required. When used in gluten free protein bars, egg albumin is easy to digest for most people. It is a great alternative for those who are allergic to dairy or milk proteins. It is considered to be a superior source of protein for humans because the amino acid pattern is one that almost matches the type us humans need to grow. While easy to digest, it also digests slower than some of the other protein sources which means it can keep your appetite curbed for longer periods of time. This makes it a great protein to be used in gluten free protein bars for those who are trying to build or maintain muscle mass while watching their calorie count.
            </li>
            <li>
                Hydrolysate Protein - These are high quality proteins that can be absorbed more rapidly than whey concentrate or isolate proteins. This type of protein comes from whey concentrate or isolate and then is hydrolyzed. This means that the protein has been exposed to an agent such as heat, acid, or enzymes which breaks apart the bonds which link the amino acids together. Because the protein is fast digesting, it will not satisfy your appetite longer than other forms of protein and the cost is higher than that of other sources. This type of protein is not common in gluten free protein bars although it can be found in protein powders which can be used to make your own gluten free protein bars.
            </li>
            <li>
                Soy Protein - Although this source of protein is not favored by many body builders, it is one of the lowest cost protein sources available and scored the highest score possible on the Protein Digestibility Correct Amino Acid score. For those who are lactose intolerant or allergic to dairy, soy protein might be the alternative you are looking for. The reason soy proteins are frowned upon by many people is because soy contains phytoestrogens. Phytoestregens are hormones present in plants, and they can bind to the estrogen receptors in humans when ingested. There are arguments to as whether this is beneficial or harmful. Many of the gluten free protein bars do not contain soy protein, but it is available and can be used when making your own.
            </li>
            <li>
                Whey Protein - Incredibly popular protein that is derived from milk. People who have milk sensitivity make not be able to digest whey protein properly, although it is low in lactose content. In terms of a protein, whey is considered to be a complete protein. It contains all 9 of the essential amino acids that humans require. It is also one of the more economically priced proteins that are added to gluten free protein bars when compared to the other sources. As with most products that have been around for a while, whey has evolved. It has gone from low grade concentrates, to extremely high grade concentrates, isolates and even hydrolysates. It is a popular protein used by body builders and other athletes.
            </li>
        </ul>
        <a name="bodybuilder"></a>
        <h3>What Are The Best Gluten Free Protein Bars For Bodybuilders?</h3>
        <p>Protein bars are made for everybody, not only for bodybuilders. Many people eat them because they want to increase their protein intake conveniently. For vegans, a protein bar is a great way to add the extra protein that they are not getting from meat. The average person who is eating a protein bar does not require the extra 20 grams of protein that a high content protein bar gives per sitting. It is because protein bars are not only focused on bodybuilders that the average protein bar is not sufficient for a bodybuilder. Bodybuilders need extra protein otherwise they will not be able to sufficiently repair and build their muscles.</p>
        <p>When choosing a protein bar, a bodybuilder should look for the following:</p>
        <ol>
            <li>Protein content: Each bar should contain a lot of protein. There are many bars that contain 4 grams of protein or less. Those protein bars are not the type of bars a bodybuilder should be using for recovery. All of the <JumpTo classes="inline-block" href="#picks" title="protein bars" /> we have listed are suitable for bodybuilding but the first 7 especially so. The first 7 that we have listed each contain at least 20 grams of protein. The final 3 contain less, but would still be much better than not consuming any protein supplement at all.</li>
            <li>Calorie count: Often protein bars have a lot of extra calories. This could be because they added a lot of sugar or fat to improve the flavor. If you are trying to tone your body or lean out, you will not be doing yourself any favors by eating a 400 calorie protein bar each day. Be conscience of the amount of calories each bar has. <JumpTo classes="inline-block" href="#image7" title="Here" /> is a bar with only 150 calories and 20 grams of protein! You do not need high calories to get high protein, you just need the right ingredients with the right type of protein. If you are on a strict diet and are only bringing in 1200 or 1600 calories per day, the protein bar you eat can potentially take a large portion of your daily allowance away. On the other hand, if you are trying to bulk up or have a difficult time putting on mass, you may very well want that 400 calorie protein bar as long as the calories come from the right sources. The important thing to remember is you want those calories to be good calories. You don't want them coming from a bunch of fast burning sugars that will offer you little benefit.</li>
            <li>Total carbohydrates and the source of those carbohydates: Pay attention to the number of carbohydrates your protein bar contains. Gluten free protein bars do not contain gluten but still often contain the same number of carbohydrates that standard protein bars do. If you eat too many carbohydrates and do not burn them off, they can be easily converted to fat to be used later as energy. Make sure the source of the carbohydrates is not predominantly sugars. Scan the ingredients and see if it contains high amounts of high-fructose corn syrup or other undesireable sugars. The higher on the ingredient list an ingredient is, the more it contains.</li>
            <li>Known allergins or intolerants. Protein can be derived from various sources, and some of those sources contain allergins such as peanuts, or intolerants such as lactose or gluten. If you known you are sensitive to a certain ingredient, be sure that the protein bar you are ingesting does not contain it. Many of the gluten free <JumpTo classes="inline-block" href="#picks" title="protein bars" /> available are also made for people who have other sensitivities as well.</li>
            <li>Fat content: Not all fat is bad. You don't want to ingest too much at any one sitting but the right type of fat under the right circumstances can be beneficial. For instance, if you are eating a gluten free protein bar for recovery after a hard workout then a bar with fat can help to slow the release of the carbohydrates into your blood stream. You want to avoid bad fats that are often used to improve the flavor of the protein bar. Fats to watch out for are saturated fats and trans fats. You want as little of those as possible. Although protein bars often taste like candy bars, you don't want to unknowingly eat something that nutritionally resembles a candy bar more than a protein bar.</li>
            <li>Ratio of carbohydrates to protein: For building lean muscle, ideally you will want at least a 2:1 ratio of protein versus carbohydrates. The high number of protein is what makes a protein bar what it is, which is a protein bar and not a "carbohydrate" or energy bar. If you are using it for energy during your workout a protein bar with a 1:1 ratio is acceptable. For those who are not looking to build lean muscle, and are trying to bulk up or use the protein bar post workout for recovery a 2:1 ratio is a bar that would be of benefit.</li>
        </ol>
    </div>
],
links = [
    {
        "id" : 1,
        "title" : "Our Top Picks",
        "tag" : "#picks",
    },
    {
        "id" : 2,
        "title" : "The Types Of Gluten Free Protein Bars We Compared",
        "tag" : "#types",
    },
    {
        "id" : 3,
        "title" : "What Do We Feel Makes The Best Gluten Free Protein Bars",
        "tag" : "#features",
    },
    {
        "id" : 4,
        "title" : "How Are The Best Gluten Free Protein Bars Different Than The Best Gluten Free Energy Bars?",
        "tag" :  "#store",
        "extra" : "Additional Information",
    },
    {
        "id" : 5,
        "title" : "What Different Types Of Proteins Are Used In The Best Gluten Free Protein Bars?",
        "tag" :  "#protein",
    },
    {
        "id": 6,
        "title": "What Are The Best Gluten Free Protein Bars For Bodybuilders?",
        "tag": "#bodybuilder",
    },
    {
        "id": 100,
        "title": "What Do You Think Makes The Best " + subject + "?",
        "tag": "#discussion",
        "extra": subject + " Discussion",
    },
];

class PageDisplayed extends React.Component{
    constructor(props) {
        super(props);
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav extended={true} state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  item6Image={item6ImageAComp} item7Image={item7ImageAComp} item8Image={item8ImageAComp} item9Image={item9ImageAComp} item10Image={item10ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} alt6tag={item6Title} alt7tag={item7Title} alt8tag={item8Title} alt9tag={item9Title} alt10tag={item10Title}/>

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links}/>

                <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>

                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike}/>

                <Description itemTitle={item6Title} description={item6Description} href={href6} pixel={pixel6} source={item6ImageA}  alttag={item6Title} url={item6url}  place="6" sup="th" name="image6"/>
                <WhatPeopleLike whatPeopleLike={item6WhatPeopleLike}/>

                <Description itemTitle={item7Title} description={item7Description} href={href7} pixel={pixel7} source={item7ImageA}  alttag={item7Title} url={item7url}  place="7" sup="th" name="image7"/>
                <WhatPeopleLike whatPeopleLike={item7WhatPeopleLike}/>

                <Description itemTitle={item8Title} description={item8Description} href={href8} pixel={pixel8} source={item8ImageA}  alttag={item8Title} url={item8url}  place="8" sup="th" name="image8"/>
                <WhatPeopleLike whatPeopleLike={item8WhatPeopleLike}/>

                <Description itemTitle={item9Title} description={item9Description} href={href9} pixel={pixel9} source={item9ImageA}  alttag={item9Title} url={item9url}  place="9" sup="th" name="image9"/>
                <WhatPeopleLike whatPeopleLike={item9WhatPeopleLike}/>

                <Description itemTitle={item10Title} description={item10Description} href={href10} pixel={pixel10} source={item10ImageA}  alttag={item10Title} url={item10url}  place="10" sup="th" name="image10"/>
                <WhatPeopleLike whatPeopleLike={item10WhatPeopleLike}/>

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}


module.exports = PageDisplayed
