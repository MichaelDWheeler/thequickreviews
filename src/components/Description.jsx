import React from "react";
import ShareIcons from "./ShareIcons.jsx";
import Placement from "./Placement.jsx";
import AmazonButton from "./AmazonButton.jsx";
import VisitSite from "./VisitSite.jsx";
import { ReactBootstrap, Col, Row, Image } from "react-bootstrap";

class Description extends React.Component {
	constructor(props) {
		super(props);
	}

	displayError(e) {
		e.target.src = "../js/img/image-on-site.png";
	}

    render() {
        const style = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
		}

        return (

            <div className="container">
                <Row>
                    <a name={this.props.name}></a>
                    <div className="location">&nbsp;</div>
                    <Col xs={12} sm={6} md={6} lg={4}>
                        <div className="pt-20">
                            <div class="image-container">
                                <Placement place={this.props.place} sup={this.props.sup} extraTitle={this.props.extraTitle} />
                                <a href={this.props.href} target="_blank">
                                    <div className="same-height">
										<Image style={{ border: '0' }} onError={this.displayError.bind(this)} className="image-item" responsive alt={this.props.alttag} src={this.props.source} />
                                    </div>
                                    <AmazonButton buttonText={this.props.buttonText} />
                                </a>
                                <Image src={this.props.pixel} style={{ border: '0' }} width="1" height="1" alt="" />
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} sm={6} md={6} lg={8}>
                        <Row>
                            <Col xs={12}>
                                <h2 className="item-title">{this.props.itemTitle}</h2>
                                <div className="btn-container">
                                    <VisitSite href={this.props.href} />
                                    <ShareIcons url={this.props.url} />
                                </div>
                            </Col>
                        </Row>
                        <div className="description">
                            {this.props.pageDescription}
                            {this.props.description}
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = Description;
