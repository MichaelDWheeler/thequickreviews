import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";


import JumpTo from "../../components/JumpTo.jsx";


let url ="electricPressureCookers",
title =`Best Electric Pressure Cooker`,
image= "../../js/img/kitchen.jpg",
mobileImage="../js/img/kitchen.jpg",
parallaxAlt="Modern open kitchen with off white cabinets and center island",
catagoryDescription = [
    <div>
        <a name="electric"></a>
        <h3>What Is An Electric Pressure Cooker?</h3>
        <p>
            Pressure cookers use the build up of highly heated air pressure and steam in a sealed container in order to cook food. The container being sealed does not allow the steam or air to escape. As the temperature inside of the container increases, the air inside the container attempts to expand but it cannot which creates high pressure. Because the unit is sealed, steam is able to reach higher than normal temperatures and the energy steam creates when condensing makes it hotter than boiling water. This results in the food cooking much more quickly in comparison to stove top or oven cooking. An electric pressure cooker is the electronic version of the stove top pressure cooker. It uses electricity to power the heating element, and because it is an electronic device it can also contain microprocessors that aid in cooking. The microprocessors control functions such as time, temperature, warming, automatic shut off, smart device connections, and more.
        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="benefits"></a>
        <h3>What Are The Benefits Of An Electric Pressure Cooker</h3>
        <p>
            There are many reasons to purchase an electric pressure cooker. Probably the number one reason that most people purchase them is for convenience. Electric pressure cookers are fast in comparison to oven or stove top cooking. Food such as a pulled pork can take up to 8 hours to cook when cooked in a slow cooker. Using an electric pressure cooker, you can get a tender and juicy pulled pork in as little as 1 hour. Refried beans can take more than 24 hours traditionally because they need to be soaked. In an electric pressure cooker, there is no soaking required and refried beans can be cooked in as little as 35 minutes. Many electric pressure cookers use pre-programmed settings and have a one touch operation. This means you can prepare your food and place it in the electric pressure cooker, seal the lid, select your desired setting and then walk away and continue with whatever else you were doing. Typically there is no need to remove the lid to monitor the food or to stir the contents. There are some exceptions but for the most part using electric pressure cookers are much more simplified than stove top or oven cooking. The units that have pre-programmed settings will automatically regulate the temperature and monitor the time.
        </p>
        <p>
            Electric pressure cookers are also extremely versatile. They are known to replace many electronic and stove top devices because they can be used for a variety of reasons. Depending on the model you choose, they can be used as a slow cooker, a pressure cooker, a porridge maker, a rice cooker, a steamer, a saute pan, a yogurt maker, and a serving/warming pot. As long as you have access to an electrical outlet, you can cook full meals in a portion of the time it would take using traditional methods.
        </p>
        <a name="our-criteria"></a>
        <h3>What We Looked For To Determine The Best Electric Pressure Cooker</h3>
        <p>When looking for the best electric pressure cooker, we looked for many of the same features that we recommended you should look for <JumpTo classes="inline-block" href="#look-for" title="here"/> with a few differences. In our advice to you we were informing you of the different uses of an electric pressure cooker that you might not have considered. Then, if those were appealing to you, we explained what features you should be on the look out for. For what we looked for though, it was pretty straight forward. We looked for an electric pressure cooker that could replace many other kitchen devices. We wanted one that was easy to operate, focused on safety, and offered multiple programmed settings. The electric pressure cookers that offered more of each our criteria scored higher with us.</p>
    </div>
],
subject = "Electric Pressure Cookers",
moreInfo = [
    <div>
        <a name="vs-slow-cooker"></a>
        <h3>Electric Pressure Cooker Versus Slow Cooker</h3>
        <ul>
            <li>Both produce very similar results in flavor and texture but reach those results in different ways.</li>
            <li>Slow cookers cook at a lower operating temperature so that they gradually cook without overcooking or burning the food. Because they use a lower temperature, they require longer periods of time. Meals cooked in a slow cooker usually require at minimum 4 hours.</li>
            <li>Electric pressure cookers operate at a much higher temperature that is able to exceed the boiling point. Although the temperature exceeds the boiling point, evaporation does not occur because the unit is sealed which does not allow the steam to escape. As the temperature increases, the air pressure increases as well. Since it is unable to escape, this forces it to penetrate the foods which results in faster with moist steam cooking. Meals cooked in an electric pressure cooker usually are cooked in less than one hour</li>
            <li>Slow cookers waste energy. Slow cookers are not pressure sealed nor are they insulated and suffer from heat loss. This is energy that is wasted as it is not used directly to cook your foods</li>
            <li>Electric pressure cookers are sealed and insulated. This allows it to be much more energy efficient. In comparison to a non insulated, non sealed unit, the amount of the heat escaping and warming the air around the electric pressure cooker is dramatically reduced. This allows it to directly cook the food inside of it.</li>
            <li>A natural toxin known as phytohaemagglutinin is present in many types of beans. This toxin can cause a person to become violently ill with as little as ingesting 3 beans, and can even cause death if ingested in large quantities. When cooked directly without taking special preparations in a slow cooker at a lower temperature the toxins can actually increase. An electric pressure cooker reaches high enough temperature to destroy the natural toxin.</li>
        </ul>
        <a name="vs-stove-top"></a>
        <h3>Electric Pressure Cooker Versus Stove Top Pressure Cooker</h3>
        <ul>
            <li>Both electric pressure cookers and pressure cookers use the same method to pressure cook food which is  high temperature, steam, pressure cooking.</li>
            <li>Stove top pressure cookers can reach temperature quicker and with higher pressure, but are not as safe as electric pressure cookers and lack many of the safety features.</li>
            <li>Electric pressure cookers are not as durable as stove top pressure cookers due to the electronics and may have to be replaced more often. Stove top pressure cookers may be passed down for generations.</li>
            <li>Most electric pressure cookers have pre-programmed settings that allow you to set the unit and then walk away while stove top pressure cooking will require a certain degree of monitoring and adjusting.</li>
            <li>Depending on the model you choose, electric pressure cookers can be 7 in 1 units and can replace many other electrical devices which will save room and the expense of purchasing additional items. If you are looking to replace items, Be sure to purchase a unit that supports all the features you will want as some do not replace all 7 devices.</li>
            <li>Cetain models of electric pressure cookers have a time delay can be prepared and then set to start cooking prior to anyone getting home so that the meal is done at the required time.</li>
            <li>Because the electric pressure cooker is considered a 7 in 1 tool, it can be used as a complete cooking tool for anyone with limited space or without the means to make multiple purchases</li>
            <li>Electric pressure cookers have built in safety features such as automatic shut off. This is benficial so that in the event you cannot make it home in time or you simply forget about it, it will turn itself off.</li>
        </ul>
        <a name="vs-rice-pot"></a>
        <h3>Electric Pressure Cooker Versus Cooking Rice In A Pot</h3>
        <ul>
            <li>Electric pressure cookers take less time to cook rice when compared to stove top cooking.</li>
            <li>Electric pressure cookers are able to kill more micro-organisms and destroy more natural toxins as they can operate at temperatures above the normal boiling point.</li>
            <li>Electric pressure cookers can deliver more intense flavors so less seasoning is often required.</li>
            <li>When compared to stove top rice cooking, electric pressure cookers require less supervision.</li>
            <li>Many models of electric pressure cookers will automatically stop cooking and start warming the rice when it has completed cooking.</li>
        </ul>
        <a name="vs-stove-top-steamer"></a>
        <h3>Electric Pressure Cooker Versus Stove Top Steamer</h3>
        <ul>
            <li>Electric pressure cookers and stove top steamers work in the same way, which is they use steam in order to heat and cook the foods.</li>
            <li>Electric pressure cookers are faster than than stove top steamers because they are pressurized and can reach higher temperatures.</li>
            <li>The pressure inside of an electric pressure cooker is much greater than that of a stove top steamer that is not pressurized and will penetrate the foods deeper and cook them more quickly.</li>
            <li>Because the electric pressure cooker operates in a sealed environment there is less steam that actually escapes which allows it to be more energy efficient. If too much steam escapes from a stove top steamer, more liquid will need to be added which will reduce the temperature and require additional heating.</li>
        </ul>
        <a name="sauteing"></a>
        <h3>Electric Pressure Cooker For Sauteing</h3>
        <ul>
            <li>If your electric pressure cooker supports sauteing, using it is very easy and convenient to use. Use it as you would a stove top saute pan. With the lid off, brown the meat. If you plan to use your electric pressure cooker for sauteing, be sure to purchase a model that supports this as some models will not turn on if the lid if not on. Once you are done sauteing, you can add it directly to the pot with some liquid and pressure cook it. This saves you having to use an additional pot.</li>
        </ul>
        <a name="yogurt"></a>
        <h3>Electric Pressure Cooker For Yogurt Making</h3>
        <ul>
            <li>If you are wanting to make your own yogurt, be sure to purchase a model that includes this pre-programmed feature as it can simplify the process. </li>
            <li>Yogurt made with your electric pressure cooker can be very satisfying. Not only will it save you money, but you can create your own custom creations to your own specifications. This includes flavor, thickness, fat content, etc...</li>
            <li>Yogurt that is pasteurized after probiotic fermentation will remove the probiotics beneficial effects. When you make your own yogurt, you can be certain that it is loaded with these healthy bacteria that are still alive.</li>
            <li>A byproduct of yogurt making is the whey it produces. You can keep this whey and consider it a free product since you are not paying additional for it. This whey can then be used in numerous ways such as soaking beans, cooking, rice, and even making bread. It is a healthy source of protein as well.</li>
        </ul>
        <a name="look-for"></a>
        <h3>What Should You Look For When Purchasing An Electric Pressure Cooker</h3>
        <p>
            When purchasing an electric pressure cooker you should first determine what you plan to use it for. You may already have ideas in mind, but don't just rush out there and purchase it without more thought. Realize that certain features may exist on certain models that may only cost you an amount more that you can easily justify. For instance, if you or your family tends to rapidly go through yogurt, or if you purchase expensive probiotic pills each month, you might want to consider an electric pressure cooker that makes yogurt. It might cost a bit more but that additional cost can easily be recouped once you start saving money. Furthermore, some models that make yogurt are not any more expensive than other brands which may not have that feature. In another example, lets suppose you frequently get home late from work and your family members eat microwave dinners each night. Instead of the added expense of those microwave dinners, you could purchase a model that allows you to delay the cooking until a time you desire. That way you can make nutritious meals and have them hot and ready to go when dinner time comes whether or not you are there. The features that may be important to you are:
        </p>
        <ul>
            <li>Safety - what safety features matter to you? We all want safety and different models focus more attention on safety than others.</li>
            <li>Do you want pre-programmed features that have the ability to fine tune the settings?</li>
            <li>Do you want this device to replace any other devices and if so what? In addition to pressure cooking, some electric pressure cookers have settings to slow cook, cook rice, brown and saute foods, make yogurt, make porridge, steam foods, and act as a food warmer.</li>
            <li>Do you want to be able to monitor your food from a smart device? Do you want to be able to use a reader to read the recipes out loud to you using a smart device?</li>
            <li>Do you want a smaller unit so you can take it on trips with you?</li>
            <li>Do you want a larger unit because you have a larger family</li>
            <li>Does the ability to prepare the meal in advance and then delay the cooking appeal to you?</li>
            <li>Do you prefer a non stick pot and if so, do you prefer one with ceramic coating?</li>
            <li>Is the lid to the pot being dishwasher safe matter to you?</li>
            <li>After the electric pressure cooker is done, should it maintain a temperature and keep the food warmed for a few hours?</li>
            <li>How often do you plan on using your electric pressure cooker? Do you plan on using it daily or even multiple times per day and leaving it on the counter? If so, does appearance matter to you? Will you be storing it most of the time and if so how important is the size and storage features such as removable cord?</li>
            <li>What is your budget for the electric pressure cooker</li>
        </ul>
        <p>
            What you look for should be determined by how you answered the above questions and any other questions you can think of.
        </p>
        <a name="easy"></a>
        <h3>Are Electric Pressure Cookers Easy To Use?</h3>
        <p>
            These devices may seem intimidating in comparison to traditional cooking but in reality they are incredibly simple to use. You do not need to be a tech head in order to get these to work well, and you don't need special training on using these. They are electronics but they have been simplified to the point where they are almost self explanatory. They have a plug, you plug it in. They have a pot that you put the food in, they have a lid that you put on, and they might have a button that says something like "Rice" that you push and minutes later you have made rice. Some may have certain other features such as a safety valve the you turn, but for the most part, that is how simple it is. Once you get the hang of it, they are easier to use as you don't have to monitor the food being cooked and you don't have to worry about overcooking the food when using pre-programmed settings. The only reason they might seem intimidating is because you may not be familiar with them. Once you get familiar, you may find they are less intimidating than an open flame under a pot that needs constant stirring to avoid scalding or burning.
        </p>
        <p>
            For those who are intimidating by cooking altogether or have never attempted to really cook, electric pressure cookers make it incredibly simple. Using one and following a recipe makes it very difficult to mess up. Thousands of recipes can be found online. If you can read, follow measurements, screw on lids, plug a plug into an outlet, and push a button then you can cook using one of these. Not only can you cook, but you can make some really savory meals that normally would take all day in the kitchen. The best part is, while it is cooking you can be doing something else entirely. Actually the best part may be the cleanup. It is minimal. Usually just the pot itself, the lid, the utensils you used and your plates. Meals that normally require two or even three pots or pans can be created successfully with this one cooker.
        </p>
    </div>
],

    item1Title = "Instant Pot IP-DUO60 7-in-1 Multi-Functional Pressure Cooker, 6Qt/1000W",
    item1Description = "This is a 7-in-1 multi-functional electric cooker. It allows you to pressure cook, slow cook, cook rice, brown and saute, make yogurt, steam foods, and keep foods warm. It has a large, easy to use control panel with 14 controlled programs controlled with it's microprocessor. It is a dual pressure electric cooker that automatically keeps food warm when done cooking and has 3 different temperatures that it uses when cooking, sauteing, and slow cooking. This unit is highly energy efficient and is UL and ULC certified with 10 proven safety mechanisms. Included with the Instant Pot IP-DUO060 is a 3 ply bottom stainless steel cooking pot, a stainless steel steam rack with handle, and a multi-language (English, Spanish, French and Chinese) manual and recipes. Using this electric cooker can save you up to 70% the time it takes when compared to normal stove top or over cooking.",
    item1url = url + encodeURIComponent('#')+"image1",
    href1=`https://www.amazon.com/dp/B00FLYWNYQ/ref=as_li_ss_il?ie=UTF8&qid=1487861171&sr=sr-1&keywords=Instant+Pot+IP-DUO60+7-in-1+Multi-Functional+Pressure+Cooker,+6Qt/1000W&linkCode=li3&tag=bestvoted-20&linkId=752dc16e3b2f9752ec85b0bff3960f2f`,
    pixel1=`https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00FLYWNYQ`,
    item1ImageA=`//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00FLYWNYQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20`,
    item1ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00FLYWNYQ&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item1ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00FLYWNYQ&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `10 safety mechanism which include audible alert when sealed and inability for pressure to build up if steam release handle is not engaged`,
        },
        {
            "id": "2",
            "positiveFeature": `No need to continuously monitor the food inside the electric pressure cooker as it is pre-programmed with 14 different settings to automatically cook`,
        },
        {
            "id": "3",
            "positiveFeature": `Automatically regulates and maintains temperature`,
        },
        {
            "id": "4",
            "positiveFeature": `Makes cooking a breeze even for inexperienced cooks`,
        },
        {
            "id": "6",
            "positiveFeature": `Cooking delay up to 24 hours allows you to prepare meals in advance and have them ready when you get home`,
        },
        {
            "id": "7",
            "positiveFeature": `Manual pressure setting allows you to cook foods for up to 2 hours at a time`,
        },
        {
            "id": "8",
            "positiveFeature": `The brushed stainless steel exterior looks great and is more resistant to fingerprints than polished stainless steel is`,
        },
        {
            "id": "9",
            "positiveFeature": `Multi-language manual and quick reference guide for those who read English, Spanish, French and Chinese`,
        },
        {
            "id": "10",
            "positiveFeature": `Stainless steel rack is low profile to allow for maximum cooking height and has handles to assist in lifting in and out of the pot`,
        },
        {
            "id": "11",
            "positiveFeature": `Comes with the following accessories: stainless steel low profile steam rack for steaming foods, condensation collector to keep water from dripping onto surrounding area, rice paddle to scoop rice, soup spoon for liquids and a measuring cup for exact quantities.`,
        },
        {
            "id": "5",
            "positiveFeature": `Easy to clean up, versatile and efficient`,
        },
        {
            "id": "12",
            "positiveFeature": `Automatically keeps food warm when done cooking`,
        },
    ],

    item2Title = "Instant Pot IP-LUX60 V3 Programmable Electric Pressure Cooker, 6Qt, 1000W",
    item2Description = "The Instant Pot IP-LUX60 V3 is all new for 2017 and is an upgraded version of the Pot IP-LUX60. I has a large control panel that is easy to use and allows cooking for up to 240 minutes instead of 120 minutes. It is a 6 in 1 multi-functional cooker that can pressure cook, slow cook, cook rice, steam, saute, and warm foods. It is both UL and ULC certified.  For those who are cooking during the night or any other time when silence is necessary, this electric pot has the ability to turn of the audible alarms and go into silent mode. It is a 6L/6.33 Quart container and uses up to 1000 watts when in operation.",
    item2url = url + encodeURIComponent('#')+"image2",
    href2="https://www.amazon.com/Instant-Pot-IP-LUX60-Programmable-Electric/dp/B01MFEBQH1/ref=as_li_ss_il?s=kitchen&ie=UTF8&qid=1487867574&sr=1-1&keywords=Instant+Pot+IP-LUX60+V3+Programmable+Electric+Pressure+Cooker,+6Qt,+1000W&linkCode=li3&tag=bestvoted-20&linkId=c920a9e192af72aa6086840f8d5aaf70" ,
    pixel2="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B01MFEBQH1" ,
    item2ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MFEBQH1&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MFEBQH1&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MFEBQH1&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2WhatPeopleLike = [
        {
            "id": "7",
            "positiveFeature": `10 safety features`,
        },
        {
            "id": "5",
            "positiveFeature": `Upgraded version of former popular model`,
        },
        {
            "id": "1",
            "positiveFeature": `Easy to use, clean, and efficient`,
        },
        {
            "id": "2",
            "positiveFeature": `Automatically sets time for cooking food`,
        },
        {
            "id": "3",
            "positiveFeature": `No need to monitor food while cooking`,
        },
        {
            "id": "4",
            "positiveFeature": `Small enough to be considered portable and can be taken on trips for use in hotel rooms or anywhere else there is an outlet`,
        },
        {
            "id": "6",
            "positiveFeature": `Audible alerts can be disabled for silent operation so as not to disturb`,
        },
        {
            "id": "8",
            "positiveFeature": `10 pre-programmed settings`,
        },
        {
            "id": "11",
            "positiveFeature": `Exterior is brushed stainless steel and is fingerprint resistant`,
        },
        {
            "id": "9",
            "positiveFeature": `Delay timer up to 24 hours`,
        },
        {
            "id": "10",
            "positiveFeature": `Multi-language manual and quick reference guide for those who read English, Spanish, French and Chinese`,
        },
        {
            "id": "12",
            "positiveFeature": `Once cooking is completed the cooker will switch to keep warm mode`,
        },
    ],

    item3Title = "Breville BPR700BSS The Fast Slow Pro, 6 Quart, Silver",
    item3Description = "The Breville BPR700BSS is a 4.5 quart maximum, 1 quart minimum capacity electric pressure cooker. It has a color changing LCD that will alert you to when the cooker is pressurizing, cooking, or releasing steam. It uses a 3 way safety system with locking lid and an automatic hands free steam release safety valve. This electric pressure cooker has 11 different settings plus the ability to use a custom setting in the event you want something it has not accounted for. The keep warm function will automatically turn on after it is done cooking and it utilizes a removable cooking bowl with a ceramic coating which is PTFE and PFOA free.",
    item3url = url + encodeURIComponent('#')+"image3",
    href3="https://www.amazon.com/Breville-BPR700BSS-Fast-Quart-Silver/dp/B013I40R8E/ref=as_li_ss_il?s=kitchen&ie=UTF8&qid=1487868498&sr=1-1&keywords=Breville+BPR700BSS+The+Fast+Slow+Pro,+6+Quart,+Silver&linkCode=li3&tag=bestvoted-20&linkId=d6e5eadd0c2e23b9ed0eb6b8a4367cc8",
    pixel3="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B013I40R8E",
    item3ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B013I40R8E&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B013I40R8E&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B013I40R8E&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Beautifully made product that looks great when displayed`,
        },
        {
            "id": "2",
            "positiveFeature": `Cord is detachable and can be stored in unit when not in use`,
        },
        {
            "id": "3",
            "positiveFeature": `Unit is smaller than most and takes up less storage space`,
        },
        {
            "id": "4",
            "positiveFeature": `Controls are basically self explanatory`,
        },
        {
            "id": "5",
            "positiveFeature": `Non-stick pot makes cleaning unit a breeze`,
        },
        {
            "id": "6",
            "positiveFeature": `Non-stick pot makes cleaning unit a breeze`,
        },
        {
            "id": "7",
            "positiveFeature": `Steam release mode will automatically adjust based upon program selected`,
        },
        {
            "id": "8",
            "positiveFeature": `Custom setting will allow you to manually adjust the temperature settings`,
        },
        {
            "id": "9",
            "positiveFeature": `Cooking bowl is coated with PTFE and PROA free ceramic coating`,
        },
        {
            "id": "10",
            "positiveFeature": `For improved accuracy and temperature control dual sensors are located at the top and bottom of the bowl`,
        },
        {
            "id": "11",
            "positiveFeature": `After food is cooked, the unit will automatically switch to keep warm mode`,
        },
    ],

    item4Title = "BLACK+DECKER PR100 6 Quart Programmable Pressure Cooker with 7 Pressure Functions, Stainless Steel Pressure Cooker",
    item4Description = "The BLACK+DECKER PR100 is a 7 in 1 Electric Cooking Pot. It has pre-programmed functions for white rice, brown rice, beans, meat, soup, steaming, and warming. Although this is a pressure cooker, it also allows for non-pressure functions so that you can warm food, slow cook food with either high or low hear, and brown food. This electric pressure cooker has been carefully designed with safety in mind and has 9 added safety features. It includes a cookbook and various accessories.",
    item4url = url + encodeURIComponent('#')+"image4",
    href4="https://www.amazon.com/BLACK-DECKER-PR100-Programmable-Functions/dp/B01LZZA5S4/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487868680&sr=1-1&keywords=BLACK+DECKER+PR100+6+Quart+Programmable+Pressure+Cooker+with+7+Pressure+Functions,+Stainless+Steel+Pressure+Cooker&linkCode=li3&tag=bestvoted-20&linkId=e4b5561458062174a43436767afe2722",
    pixel4="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B01LZZA5S4",
    item4ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01LZZA5S4&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01LZZA5S4&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01LZZA5S4&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4WhatPeopleLike = [
        {
            "id": "6",
            "positiveFeature": `9 safety mechanisms`,
        },
        {
            "id": "1",
            "positiveFeature": `Ships in a large box to prevent damage`,
        },
        {
            "id": "2",
            "positiveFeature": `Instructions for assembly are clear and easy to follow`,
        },
        {
            "id": "3",
            "positiveFeature": `Easy to clean non stick surface`,
        },
        {
            "id": "4",
            "positiveFeature": `Automatic and manual controls to perfect meals`,
        },
        {
            "id": "5",
            "positiveFeature": `Great for single or smaller families`,
        },
        {
            "id": "7",
            "positiveFeature": `Cooking bowl is non stick`,
        },
        {
            "id": "8",
            "positiveFeature": `Unit can be set to delay start so that food can be prepared in advance and then cooked prior to getting home or when it is needed`,
        },
        {
            "id": "10",
            "positiveFeature": `Cookbook is included to make delicious electric pressure cooked meals`,
        },
        {
            "id": "9",
            "positiveFeature": `Food will be kept warm automatically when unit is done cooking`,
        },
    ],

    item5Title = "Instant Pot IP-Smart Bluetooth-Enabled Multifunctional Pressure Cooker, Stainless Steel",
    item5Description = "The Instant Pot IP-Smart Bluetooth-Enabled Multifunctional Pressure Cooker is an 8 in 1 cooker. It can be used as a pressure cooker, slow cooker, rice cooker, porridge maker, steamer, saute/browning, yogurt maker and warmer. It differs from the other electric pressure cookers we have listed in that it can be programmed and monitored with a free app available on your Android, iPhone, or iPad. It's functionality can be further expanded upon by upgrading the free app on your device. This electric pressure cooker contains 14 pre-programmed functions and uses dual pressure, automatic warming and 3 different temperatures to give you the perfect meal. It has been UI and ULC certified with 10 proven safety mechanism and it is also highly energy efficient.",
    href5="https://www.amazon.com/BLACK-DECKER-PR100-Programmable-Functions/dp/B01LZZA5S4/ref=as_li_ss_il?s=appliances&ie=UTF8&qid=1487868680&sr=1-1&keywords=BLACK+DECKER+PR100+6+Quart+Programmable+Pressure+Cooker+with+7+Pressure+Functions,+Stainless+Steel+Pressure+Cooker&linkCode=li3&tag=bestvoted-20&linkId=e4b5561458062174a43436767afe2722" ,
    pixel5="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B01LZZA5S4",
    item5url = url + encodeURIComponent('#')+"image5",
    item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00N310CKG&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item5ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00N310CKG&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00N310CKG&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item5WhatPeopleLike = [
        {
            "id": "5",
            "positiveFeature": `10 safety features`,
        },
        {
            "id": "6",
            "positiveFeature": `UL and ULC certified`,
        },
        {
            "id": "1",
            "positiveFeature": `Temperature can be controlled manually`,
        },
        {
            "id": "2",
            "positiveFeature": `Use of scripts allow for great customization`,
        },
        {
            "id": "3",
            "positiveFeature": `Can monitor pot from various devices that have downloaded the app`,
        },
        {
            "id": "4",
            "positiveFeature": `Ability to download recipes and scripts onto app`,
        },
        {
            "id": "7",
            "positiveFeature": `Bluetooth 4.0 Smart Compliant connects to smart phones and devices allowing easy to control and monitor cooking progress`,
        },
        {
            "id": "8",
            "positiveFeature": `App is free and can be upgraded after purchase for more functionality`,
        },
        {
            "id": "9",
            "positiveFeature": `It comes with accessories which include a stainless steel steam rack with handles for ease of lifting in and out of pot, condensation collector for keeping water off surrounding area, rice paddle for easily scooping up rice, a soup spoon for liquids, a measuring cup for exact measurements, oven mitts made from silicone, and the instruction manuel with recipes included.`,
        },
    ],
    links = [

        {
            "id" : 3,
            "title" : "Our Top Picks",
            "tag" : "#picks",
        },
        {
            "id" : 12,
            "title" : "What Is An Electric Pressure Cooker",
            "tag" : "#electric",
        },
        {
            "id" : 1,
            "title" : "What Are The Benefits Of An Electric Pressure Cooker",
            "tag" : "#benefits",
        },
        {
            "id": 13,
            "title": "What We Looked For To Determine The Best Electric Pressure Cooker",
            "tag": "#our-criteria",
        },
        {
            "id" : 2,
            "title" : "Electric Pressure Cooker Versus Slow Cooker",
            "tag": "#vs-slow-cooker",
            "extra": "Additional Information",
        },
        {
            "id" : 4,
            "title" : "Electric Pressure Cooker Versus Stove Top Pressure Cooker",
            "tag" :  "#vs-stove-top",
        },
        {
            "id" : 5,
            "title" : "Electric Pressure Cooker Versus Cooking Rice In A Pot",
            "tag" :  "#vs-rice-pot",
        },
        {
            "id" : 6,
            "title" : "Electric Pressure Cooker Versus Stove Top Steamer",
            "tag" :  "#vs-stove-top-steamer",
        },
        {
            "id" : 7,
            "title" : "Electric Pressure Cooker For Sauteing",
            "tag" :  "#sauteing",
        },
        {
            "id" : 9,
            "title" : "Electric Pressure Cooker For Yogurt Making",
            "tag" :  "#yogurt",
        },
        {
            "id" : 10,
            "title" : "What Should You Look For When Purchasing An Electric Pressure Cooker",
            "tag" :  "#look-for",
        },
        {
            "id" : 11,
            "title" : "Are Electric Pressure Cookers Easy To Use",
            "tag" :  "#easy",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ];

    class Featured extends React.Component{
        constructor(props) {
            super(props);
        }

        date(){
            let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate =`${month} ${day}, ${year}`;
            return newDate;
        }

        year(){
            let d = new Date,
            thisYear = d.getFullYear();
            return thisYear;
        };

        quantity() {
            if (typeof item10Title !== 'undefined') {
                return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
            } else {
                return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
            }
        };

        render() {
            return (
                <div>
                    <MetaTags>
                        <title>{title}</title>
                        <meta id="meta-description" name="description" content={this.quantity()} />
                    </MetaTags>
                    <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                    <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                    <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                    <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                    <PageNavigation links={links}/>

                    <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                    <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                    <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>


                    <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                    <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>


                    <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                    <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>


                    <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                    <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>


                    <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                    <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                    <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                    <AdrienneWheeler />
                    <Disqus url={url} id={url + "1"} subject={subject} />

                </div>
            )
        }
    }


    module.exports = Featured
