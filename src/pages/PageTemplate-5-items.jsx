import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import JumpTo from "../../components/JumpTo.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

let url = "",
	buttonText = "See Additional Information",
    title = "",
    image = "../../js/img/*.jpg",
    mobileImage = "../js/img/*.jpg",
    parallaxAlt = "",
    subject = "",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of {subject} We Compared</h3>
        <p>

        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Do We Feel Makes The Best {subject}?</h3>
        <p>
        </p>
    </div>
],

item1Title = "",
item1Description = "",
href1="",
item1url = url + "/" + encodeURIComponent('#')+"image1",
item1ImageA = "",
item1ImageAComp = "",
item1ImageANav = "",
pixel1="",
item1WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item2Title = "",
item2Description = "",
href2="",
item2url = url + "/" + encodeURIComponent('#')+"image2",
item2ImageA = "",
item2ImageAComp = "",
item2ImageANav = "",
pixel2="",
item2WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item3Title = "",
item3Description = "",
href3="",
item3url = url + "/" + encodeURIComponent('#')+"image3",
item3ImageA = "",
item3ImageAComp = "",
item3ImageANav = "",
pixel3="",
item3WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item4Title = "",
item4Description = "",
href4="",
item4url = url + "/" + encodeURIComponent('#')+"image4",
item4ImageA = "",
item4ImageAComp = "",
item4ImageANav = "",
pixel4="",
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item5Title = "",
item5Description = "",
item5url = url + "/" + encodeURIComponent('#')+"image5",
href5="",
item5ImageA = "",
item5ImageAComp = "",
item5ImageANav = "",
pixel5="",
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],
moreInfo = [
    <div>
        <a name="store"></a>
        <h3></h3>
        <p>

        </p>
    </div>
],
links = [
    {
        "id": 1,
        "title": "Our Top Picks",
        "tag": "#picks",
    },
    {
        "id": 2,
        "title": `The Types Of ${subject} We Compared`,
        "tag": "#types",
    },
    {
        "id": 3,
        "title": `What Do We Feel Makes The Best ${subject}`,
        "tag": "#features",
    },
    {
        "id": 4,
        "title": "",
        "tag": "#store",
        "extra": "Additional Information",
    },
    {
        "id": 100,
        "title": `What Do You Think Makes The Best ${subject}?`,
        "tag": "#discussion",
        "extra": `${subject} Discussion`,
    },
];

class PageDisplayed extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount() {
        ga('create', 'UA-92142805-1', 'auto');
        ga('send', 'pageview');
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

	render() {
		return (
			<div>
				<MetaTags>
					<title>{title}</title>
					<meta id="meta-description" name="description" content={this.quantity()} />
				</MetaTags>
				<FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
				<Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
				<ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />

				<ItemIntroduction catagoryDescription={catagoryDescription[0]} />
				<PageNavigation links={links} />

				<PageDescription date={this.date()} pageDescription={mainDescription[0]} />

				<Description buttonText={buttonText} itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
				<WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
				<WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
				<WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
				<WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
				<WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />
				<AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
				<AdrienneWheeler />
				<Disqus url={url} id={url + "1"} subject={subject} />
			</div>
		)
	}
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-92142805-1', 'auto');
ga('send', 'pageview');

module.exports = PageDisplayed
