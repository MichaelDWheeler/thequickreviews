import React from "react";
import { connect } from "react-redux";
import { hideAwayFromView, resetInitialScroll, linkClick } from "../actions/FixedHeaderAction.jsx";

@connect((store) => {
    return {
        showFixedHeader: store.fixedHeader.showFixedHeader,
        initialScroll: store.fixedHeader.initialScroll,
        linkClick: store.fixedHeader.linkClick,
    }
})

class JumpTo extends React.Component {
    constructor(props){
        super(props);
        this.noDropDown = this.noDropDown.bind(this)
    }

    noDropDown(){
        this.props.dispatch(hideAwayFromView());
        this.props.dispatch(resetInitialScroll());
        this.props.dispatch(linkClick());
    }

    render(){
        return(
            <span className={this.props.classes}>
                <a onClick={this.noDropDown} href={this.props.href}>{this.props.title}</a>
            </span>
        )
    }
}

module.exports = JumpTo
