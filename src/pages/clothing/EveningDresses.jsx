import React from "react";
import MetaTags from 'react-meta-tags';

import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

let url = "eveningDresses",
    title = "Best Evening Dresses",
    image = "../../js/img/evening-dress.jpg",
    mobileImage = "../js/img/evening-dress.jpg",
    parallaxAlt = "Lady in red evening gown laying in luxurious chair",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>The Types Of Evening Dresses We Compared</h3>
            <p>When we decided we were going to find what we considered to be the best evening dresses, we first had to determine if we should select a single style or have a broad category. We chose the latter. We did not want to focus on only white tie event evening dresses or black tie events. We also did not want to narrow our selection down to a specific length. Rather, we wanted to find what we considered to be the best evening dresses overall and present our top picks to you.</p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What Are Evening Dresses?</h3>
            <p>
                Evening dresses are typically worn to a formal affair. Although the style and even the shape can vary, they most often look elegant and luxurious. It is easy to distinguish an evening dress by appearance alone, and if worn in an informal setting it would stand out as being inappropriate for the occassion. Although they look best in a formal setting, the styles can differ to such an extent that some evening dresses would not be appropriate for the most formal of occassions while other formal events wouldn't require the most luxurious of style. Not only can the decorative style vastly differ from one another, the length of the evening dresses can vary as well. They can be tea, ballerina, or full length dresses. The type of event usually determines the length of the dress that is worn. White tie events are considered more formal and dresses are usually longer with fuller silhouettes. At black tie events it is not uncommon to see what is known as tea length dresses that extend to the calf or ankle to ballerina length which stops right at the ankle. Full length dresses that extend all the way to the floor can also be worn at these events as well. Evening dresses are usually made from quality materials such as chifron, velvet, satin, organza, and silk, which is extremely popular. To stand out in the crowd, some evening dresses sparkle with sequins, rhinestones, or even thread that is reflective. For those who are not interested in being flashy but still want to look elegant and beautiful, there are countless other dresses without the added flair.
        </p>
        </div>
    ],
    subject = "Evening Dresses",
    moreInfo = [
        <div>
            <a name="best-evening-dresses"></a>
            <h3>Are the best evening dresses expensive?</h3>
            <p>
                It depends on many factors, first and foremost it would depend on your definition of expensive. Evening dresses can range dramatically in price. Some are priced under $50, while others cost hundreds or even thousands of dollars. For some, thousands of dollars might not be considered expensive by their definition. However, there are some evening dresses that are so outrageously expensive that they would be expensive by anyone's standards. For instance, there are evening dresses that cost millions of dollars. That does not necessarily make them the best however, it just makes them really expensive.
        </p>
            <p>
                So if price is not the only factor in determining what the best evening dresses are, we need to figure out what makes the best, the best. Try as we might, we can't make that decision for you because when it comes to fashion, it truly is a personal opinion. For many people, it is not only the appearance of the evening dress that matters but it is also the name of the designer on the label that is important. Evening dresses made by famous designers will obviously cost more and be considerably more expensive than those which do not have that label. While these dresses can cost tens of thousands of dollars, fake designer dresses can look almost identical and may cost under $100. If you would be thrilled to be in an evening dress styled after a designer evening dress then you may be able to find one without breaking your budget.
        </p>
            <p>
                Others may find that the best evening dresses are ones that are more versatile. This means that it can be easily accessorized and be suitable for different events. A versatile dress doesn't necessarily have to be more expensive, but when you factor all the different occasions that it can be worn to and all the different dresses that it replaces then it can cost a bit more but still save you money overall.
        </p>
            <p>
                An extremely comfortable evening dress may be what someone else is looking for.  Comfort is not only about how you feel physically in the dress, but mentally as well. Some of the evening dresses can be so form fitting that it can be difficult to sit down. Others can expose body parts if you position yourself a certain way which might make you self conscience and uncomfortable all night. There are evening dresses with so many sequins or decorations added to them that the inner arm can rub against them and cause irritation. Some evening dresses are made out of materials that wrinkle easier than others.
        </p>
            <p>
                Lastly, do you feel it is worth it? The evening dress may very well be expensive by your standards but if you feel it is worth it that takes the sting away a bit. If you look and feel beautiful in it, you are comfortable in it, it contributes to your fantastic night, and people compliment you all night long, is it worth it? How important is the event you are going to be attending? Are you going to be photographed? Will people whose opinion you value remember the way you looked? These are all important decisions that can influence your decision on whether or not it was worth it, and whether or not you choose to spend a bit more than you may be comfortable with if you decide it is necessary.
        </p>
        </div>
    ],
    links = [
        {
            "id": 3,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 1,
            "title": "The Types Of Evening Dresses We Compared",
            "tag": "#types",
        },
        {
            "id": 2,
            "title": "What Are Evening Dresses?",
            "tag": "#features",
        },
        {
            "id": 4,
            "title": "Are The Best Evening Dresses Expensive?",
            "tag": "#best-evening-dresses",
            "extra": "Additional Information",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ],

    item1Title = "PrettyGuide Women's 1920s Black Sequin Gatsby Maxi Long Evening Dress",
    item1Description = "The top pick for this year is the PrettyGuide Women's 1920 Black Sequin Gatsby Long Evening Dress. It is made from 100% polyester and should be hand washed only. This evening dress has cap sleeves, a beautiful v-neck back, and is a floor length evening dress for that elegant yet sexy look. It has a zipper closure up the center of the back. This dress is covered in sequin lucky cloves which are either black, gold, or silver depending on the color of the dress. The colors that are available are black, gold, burgundy, navy and green. The bottom portion of the dress is see through and the tulle portion on the bottom is sheer and is not lined. It does not come with a built in bra.",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    href1 = "https://www.amazon.com/PrettyGuide-Women-Sequin-Gatsby-Evening/dp/B01IEC7800/ref=as_li_ss_il?ie=UTF8&qid=1488327720&sr=8-1&keywords=B01IEC7800&linkCode=li3&tag=thequickreviews-20&linkId=cb157b4083a735c84733b38b1d83b129",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01IEC7800",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IEC7800&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IEC7800&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IEC7800&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Beautiful dress that is very eye catching`,
        },
        {
            "id": "2",
            "positiveFeature": `Looks expensive`,
        },
        {
            "id": "3",
            "positiveFeature": `Sequin details is incredible`,
        },
        {
            "id": "4",
            "positiveFeature": `High quality dress has more weight than one might expect`,
        },
        {
            "id": "5",
            "positiveFeature": `Can allow you to feel sexy, elegant and comfortable at the same time`,
        },
        {
            "id": "6",
            "positiveFeature": `Flattering fitting dress`,
        },
    ],

    item1WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Depending on the body type of the wearer, the sheer portion may be up too high and can expose the buttocks.`,
        },
        {
            "id": "2",
            "negativeConcern": `Taller women may find that the dress runs too short overall.`,
        },
        {
            "id": "3",
            "negativeConcern": `If the inner arms have excess skin, it could rub against the sequins and cause discomfort.`,
        },
        {
            "id": "4",
            "negativeConcern": `Does not come with a built in bra.`,
        },
    ],

    item2Title = "MissMay Women's Vintage Floral Lace Long Sleeve Boat Neck Cocktail Formal Swing Dress",
    item2Description = "The MissMay Evening Dress is made from 70% Nylon and 30% cotton. It is a long sleeve boat neck formal swing dress that run right above the knee. While shorter than the typical evening dress, it is suitable for evening, cocktail, wedding parties, and other formal occasions. It has an elegant retro look and has a full length lace overlay. It comes in navy blue, red, black, and white.",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    href2 = "https://www.amazon.com/MissMay-Womens-Vintage-Floral-Cocktail/dp/B01N57VVFD/ref=as_li_ss_il?ie=UTF8&qid=1488328092&sr=8-1&keywords=B01N57VVFD&linkCode=li3&tag=thequickreviews-20&linkId=821d96d675e0cff2757c4fad3459776e",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01N57VVFD",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01N57VVFD&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01MRPYLEI&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01N57VVFD&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Great dress for women with broad shoulders`,
        },
        {
            "id": "2",
            "positiveFeature": `Sticky strips on shoulder keeps the dress from sliding down`,
        },
        {
            "id": "3",
            "positiveFeature": `The dresses material is very soft, stretches to accommodate, is breathable, and is very comfortable`,
        },
        {
            "id": "4",
            "positiveFeature": `Sexy meets classy style`,
        },
        {
            "id": "5",
            "positiveFeature": `The long sleeves of the dress are made from a material that stretches and allows for comfortable extended wear`,
        },
        {
            "id": "6",
            "positiveFeature": `The underskirt of the dress is the same length as the lace, so it prevents see through where expected`,
        },
        {
            "id": "7",
            "positiveFeature": `Inner camisole has built in rubber strip to keep it in place`,
        },
    ],

    item2WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Dress may not run true to size`,
        },
        {
            "id": "2",
            "negativeConcern": `This dress may not be formal enough for certain occasions where a full length dress is appropriate`,
        },
        {
            "id": "3",
            "negativeConcern": `Does not come with a built in bra`,
        },
    ],

    item3Title = "Babyonlinedress® Mermaid Evening Dress for Women Formal Long Dress",
    item3Description = "The Babyonlinedress® Mermaid Evening Dress is a sleeveless, stretched formal long dress that features a center back zipper. It is made from light and breathable materials and is a mixture of sexy and elegant that can be worn with a strapless bra or nipple covers.  to many different occasions such as a formal party, holiday party, prom, evening event and special occasion events. This dress comes in black, burgundy, dark purple, ivory, lavender, mint green, navy, nude pink, peacock green, red, rose, and royal blue. Dry cleaning is recommended for this dress.",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    href3 = "https://www.amazon.com/Womens-Sleeveless-Appliques-Mermaid-Evening/dp/B01HZ3YOGK/ref=as_li_ss_il?ie=UTF8&qid=1488328232&sr=8-1&keywords=B01HZ3YOGK&linkCode=li3&tag=thequickreviews-20&linkId=785e288556b47ea6a80ba4dbb1a04d6a",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01HZ3YOGK",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01HZ3YOGK&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01HZ3YOGK&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01HZ3YOGK&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `This is a quality made dress`,
        },
        {
            "id": "2",
            "positiveFeature": `Illusion neckline keeps dress up even for women with large chest`,
        },
        {
            "id": "3",
            "positiveFeature": `The top gold design is sewn on`,
        },
        {
            "id": "4",
            "positiveFeature": `Made with heavier material that allows stretching`,
        },
        {
            "id": "5",
            "positiveFeature": `Includes sewn in bra cup`,
        },
    ],
    item3WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `May appear slightly different than pictured`,
        },
        {
            "id": "2",
            "negativeConcern": `Zipper appears to be flimsy`,
        },
    ],

    item4Title = "MUXXN Womens 1950s Cowl Neck Fishtail Evening Dress",
    item4Description = "The MUXXN Womens 1950s cowl evening dress is a sexy v neck, slimming, long dress with a fishtail design. It is a dress that can be worn for most occasions and is very comfortable. The dress comes with a belt and it is available in burgundy, white, black, green, pure black, and pure green.",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    href4 = "https://www.amazon.com/MUXXN-Womens-Fishtail-Evening-Burgundy/dp/B01FTON030/ref=as_li_ss_il?ie=UTF8&qid=1488328387&sr=8-1&keywords=B01FTON030&linkCode=li3&tag=thequickreviews-20&linkId=ca0308fd78d5db330da2d5c4254e7d18",
    pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01FTON030",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01FTON030&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01FTON030&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01FTON030&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Thick material does not allow dress to be transparent`,
        },
        {
            "id": "2",
            "positiveFeature": `Satin like feel and appearance although heavier`,
        },
        {
            "id": "3",
            "positiveFeature": `Drapes nicely`,
        },
        {
            "id": "4",
            "positiveFeature": `Form fitting dress gives great coverage of all "lady parts"`,
        },
        {
            "id": "5",
            "positiveFeature": `Removable belt included`,
        },
        {
            "id": "6",
            "positiveFeature": `Zipper sits deep in the material to keep with form fitting appearance`,
        },
    ],
    item4WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Zipper is thin`,
        },
        {
            "id": "3",
            "negativeConcern": `Certain bras may be exposed at armpit area and may need special care to hide`,
        },
    ],

    item5Title = "REPHYLLIS Women's Retro Floral Lace Vintage Long Dress",
    item5Description = "The REPHYLLIS women's retro floral lace long dress is made from polyester, spandex, and lace. It is a cap sleeve, high waist, slim body-con dress. It has a sexy dress that is see-through on the shoulder area. This evening dress can be worn for many occasions such as cocktail parties, holiday parties, weddings, and even prom. It is recommended that this dress is hand washed only in low temperatures or dry cleaning. Never iron this dress.",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    href5 = "https://www.amazon.com/REPHYLLIS-Womens-Vintage-Wedding-Bridesmaid/dp/B01KX41C1I/ref=as_li_ss_il?ie=UTF8&qid=1488328554&sr=8-1&keywords=B01KX41C1I&linkCode=li3&tag=thequickreviews-20&linkId=c4626b2b611ef90215125556f41d2256",
    pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01KX41C1I",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KX41C1I&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KX41C1I&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KX41C1I&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Long, lacy dress is form fitting and maintains modesty`,
        },
        {
            "id": "2",
            "positiveFeature": `High quality material`,
        },
        {
            "id": "3",
            "positiveFeature": `Comfortable dress can be easily worn all day and night`,
        },
        {
            "id": "4",
            "positiveFeature": `Eye catching mermaid tail has a lot of movement`,
        },
        {
            "id": "5",
            "positiveFeature": `V-shaped back extends slightly below shoulder blades and is trimmed with lace`,
        },
        {
            "id": "6",
            "positiveFeature": `Form fitting dress fits well on even curvy individuals`,
        },
        {
            "id": "7",
            "positiveFeature": `Great evening dress for when you want to dress up without being overly dressed`,
        },
    ];

class PageDisplayed extends React.Component{
    constructor(props) {
        super(props);
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links}/>

                <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>

                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike}/>

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />

                <Disqus url={url} id={url + "1"} subject={subject}/>
            </div>
        )
    }
}


module.exports = PageDisplayed
