import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import WhatPeopleDislike from "../../components/WhatPeopleDislike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

let url = "",
	buttonText = "See Additional Information",
    title = "",
    image = "../../js/img/*.jpg",
    mobileImage = "../js/img/*.jpg",
    parallaxAlt = "",
    subject = "",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of {subject} We Compared</h3>
        <p>

        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Do We Feel Makes The Best {subject}?</h3>
        <p>
        </p>
    </div>
],

item1Title = "",
item1Description = "",
href1="",
item1url = url + "/" + encodeURIComponent('#')+"image1",
item1ImageA = "",
pixel1="",
item1ImageAComp = "",
item1ImageANav = "",
item1WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item2Title = "",
item2Description = "",
href2="",
item2url = url + "/" + encodeURIComponent('#')+"image2",
item2ImageA = "",
pixel2="",
item2ImageAComp = "",
item2ImageANav = "",
item2WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item3Title = "",
item3Description = "",
href3="",
item3url = url + "/" + encodeURIComponent('#')+"image3",
item3ImageA = "",
pixel3="",
item3ImageAComp = "",
item3ImageANav = "",
item3WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item4Title = "",
item4Description = "",
href4="",
item4url = url + "/" + encodeURIComponent('#')+"image4",
item4ImageA = "",
pixel4="",
item4ImageAComp = "",
item4ImageANav = "",
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item5Title = "",
item5Description = "",
href5="",
item5url = url + "/" + encodeURIComponent('#')+"image5",
item5ImageA = "",
pixel5="",
item5ImageAComp = "",
item5ImageANav = "",
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item6Title = "",
item6Description = "",
href6 = "",
item6url = url + "/" + encodeURIComponent('#') + "image6",
item6ImageA = "",
pixel6 = "",
item6ImageAComp = "",
item6ImageANav = "",
item6WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item7Title = "",
item7Description = "",
href7 = "",
item7url = url + "/" + encodeURIComponent('#') + "image7",
item7ImageA = "",
pixel7 = "",
item7ImageAComp = "",
item7ImageANav = "",
item7WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item8Title = "",
item8Description = "",
href8 = "",
item8url = url + "/" + encodeURIComponent('#') + "image8",
item8ImageA = "",
pixel8 = "",
item8ImageAComp = "",
item8ImageANav = "",
item8WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item9Title = "",
item9Description = "",
href9 = "",
item9url = url + "/" + encodeURIComponent('#') + "image9",
item9ImageA = "",
pixel9 = "",
item9ImageAComp = "",
item9ImageANav = "",
item9WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item10Title = "",
item10Description = "",
href10 = "",
item10url = url + "/" + encodeURIComponent('#') + "image10",
item10ImageA = "",
pixel10 = "",
item10ImageAComp = "",
item10ImageANav = "",
item10WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],
moreInfo = [
    <div>
        <a name="store"></a>
        <h3></h3>
        <p>

        </p>
    </div>
],
links = [
    {
        "id": 1,
        "title": "Our Top Picks",
        "tag": "#picks",
    },
    {
        "id": 2,
        "title": `The Types Of ${subject} We Compared`,
        "tag": "#types",
    },
    {
        "id": 3,
        "title": `What Do We Feel Makes The Best ${subject}`,
        "tag": "#features",
    },
    {
        "id": 4,
        "title": "",
        "tag": "#store",
        "extra": "Additional Information",
    },
    {
        "id": 100,
        "title": `What Do You Think Makes The Best ${subject}?`,
        "tag": "#discussion",
        "extra": `${subject} Discussion`,
    },
];

class PageDisplayed extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount() {
        ga('create', 'UA-92142805-1', 'auto');
        ga('send', 'pageview');
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav extended={true} state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} item6Image={item6ImageAComp} item7Image={item7ImageAComp} item8Image={item8ImageAComp} item9Image={item9ImageAComp} item10Image={item10ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} alt6tag={item6Title} alt7tag={item7Title} alt8tag={item8Title} alt9tag={item9Title} alt10tag={item10Title} />

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links}/>

                <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                <Description buttonText={buttonText} itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                <Description buttonText={buttonText} itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                <Description buttonText={buttonText} itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                <Description buttonText={buttonText} itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>


                <Description buttonText={buttonText} itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item6Title} description={item6Description} href={href6} pixel={pixel6} source={item6ImageA} alttag={item6Title} url={item6url} place="6" sup="th" name="image6" />
                <WhatPeopleLike whatPeopleLike={item6WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item7Title} description={item7Description} href={href7} pixel={pixel7} source={item7ImageA} alttag={item7Title} url={item7url} place="7" sup="th" name="image7" />
                <WhatPeopleLike whatPeopleLike={item7WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item8Title} description={item8Description} href={href8} pixel={pixel8} source={item8ImageA} alttag={item8Title} url={item8url} place="8" sup="th" name="image8" />
                <WhatPeopleLike whatPeopleLike={item8WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item9Title} description={item9Description} href={href9} pixel={pixel9} source={item9ImageA} alttag={item9Title} url={item9url} place="9" sup="th" name="image9" />
                <WhatPeopleLike whatPeopleLike={item9WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item10Title} description={item10Description} href={href10} pixel={pixel10} source={item10ImageA} alttag={item10Title} url={item10url} place="10" sup="th" name="image10" />
                <WhatPeopleLike whatPeopleLike={item10WhatPeopleLike} />

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />
            </div>
        )
    }
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-92142805-1', 'auto');
ga('send', 'pageview');

module.exports = PageDisplayed
