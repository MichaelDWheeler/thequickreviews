import React from "react";
import { toggleNavigation } from "../actions/DropDownActions.jsx";
import { backgroundAction } from "../actions/BackgroundAction.jsx";
import { change } from "../actions/SearchActions.jsx";
import { ReactBootstrap, Col, Row, Image } from "react-bootstrap";
import { connect } from "react-redux";

@connect((store)=> {
    return {
        showDropDownNavigation: store.dropDown.showDropDownNavigation,
        smallSearchGlass: store.searchGlass.smallSearchGlass,
        toggleBackgroundFilter: store.backgroundAction.toggleBackgroundFilter
    }
})

class Header extends React.Component {
    constructor(props){
        super(props);
        this.toggleLinks = this.toggleLinks.bind(this);
        this.mouseButtonChange = this.mouseButtonChange.bind(this);
    }

    mouseButtonChange(){
        this.props.dispatch(change());
        setTimeout(()=>{
            this.props.dispatch(change());
        },100);
    }

    toggleLinks(){
        this.props.dispatch(toggleNavigation());
        this.props.dispatch(backgroundAction());
    }

    render(){
        let searchGlass, smallerSearchGlass, backgroundFilter;
        if(this.props.toggleBackgroundFilter){
            backgroundFilter =
            <div className = "backgroundFilter">
                &nbsp;
            </div>
        }else{
            backgroundFilter = null;
        }
        if(!this.props.smallSearchGlass){
            searchGlass =
            <Col xs={2} sm={6} md={6} className = "show-mobile">
                <div onMouseDown={this.mouseButtonChange}>
                    <i className="fa fa-search fa-2x" aria-hidden="true"></i>
                </div>
            </Col>
        } else {
            smallerSearchGlass =
            <Col xs={2} sm={6} md={6} className="show-mobile">
                <div className="smaller-search-glass" onMouseDown={this.mouseButtonChange}>
                    <i className="fa fa-search fa-2x" aria-hidden="true"></i>
                </div>
            </Col>;
        }

        return(
            <div>
                {backgroundFilter}
                <div className="container-fluid border-bottom">
                    <Row>
                        <Col xs={10} sm={6} md={4}>
                            <div className="header">
                                <div className="separator flow-right">&nbsp;</div>
                                    <Image  onClick={this.toggleLinks} className="brand" src="../js/img/the-quick-reviews-logo-web.png" alt="The Quick Reviews Logo"/>
                                    <i class="fa fa-caret-down fa-2x"  aria-hidden="true" onClick={this.toggleLinks}></i>
                            </div>
                        </Col>

                        {/* Mobile Search Bar */}
                        {searchGlass}
                        {smallerSearchGlass}

                        {/* Desktop Search Bar */}
                        <Col xs={1} sm={4} md={7} className="hide-mobile">
                            <Col xs={2} sm={2}>
                                <i className="fa fa-search fa-2x" aria-hidden="true"></i>
                            </Col>
                            <Col xs={10} sm={9} md={10} className="pt-15">
                                <input className="search-input" type="text" placeholder=" What is the best "/>
                            </Col>
                        </Col>

                        <Col xs={1} sm={2} md={1} className="hide-mobile">
                            <div className="ask-btn text-center hvr-shutter-out-horizontal">
                                <div className="ask">ASK</div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>

        )
    }
}

module.exports = Header;
