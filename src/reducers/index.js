import { combineReducers } from "redux";

import dropDown from "./dropDownReducer.jsx";
import fixedHeader from "./fixedHeaderReducer.jsx";
import searchGlass from "./searchGlassSizeReducer.jsx";
import backgroundAction from "./backgroundReducer.jsx";
import showTitleInPicture from "./showTitleReducer.jsx";


export default combineReducers({
    dropDown,
    fixedHeader,
    searchGlass,
    backgroundAction,
    showTitleInPicture
});
