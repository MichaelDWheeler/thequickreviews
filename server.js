require('babel-register')({
    presets: ['react']
});
var express = require('express');
var app = express();
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var Index = require('./src/components/index.jsx');

app.use(express.static('src'))
app.get('/', function(req,res){
    let html = ReactDOMServer.renderToString(
        React.createElement(Index)
    )
    res.send(html);
})
app.get('*', function(req,res){
    let html = ReactDOMServer.renderToString(
        React.createElement(Index)
    )
    res.send(html);
})
var PORT = process.env.PORT || 1337;
app.listen(PORT, function(){
    console.log('http://localhost ' + PORT);
});
