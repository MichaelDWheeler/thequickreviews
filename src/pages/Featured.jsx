import React from "react";

import Title from "../components/Title.jsx";
import PageDescription from "../components/PageDescription.jsx";
import Description from "../components/Description.jsx";
import Placement from "../components/Placement.jsx";
import ProductImages from "../components/ProductImages.jsx";
import WhatPeopleLike from "../components/WhatPeopleLike.jsx";
import ComparisonImages from "../components/ComparisonImages.jsx";
import FixedHeaderNav from "../components/FixedHeaderNav.jsx";
import Header from "../components/Header.jsx";
import ReviewedItems from "../components/ReviewedItems.jsx";
import { Link } from 'react-router';

import { Row, Col, Image } from 'react-bootstrap';

let title = `We Make Your Buying Decisions Quick and Easy`,
    image = "../../js/img/best-reviews.jpg",
    mobileImage = "../js/img/best-reviews.jpg",
    parallaxAlt = "Man doing yardwork with shears",
    mainDescription = "Here at The Quick Reviews, we research popular products in each category we have displayed. We let you know what features you should look out for and which products have them. We compile our data and then display our top choices in each category to make your purchasing decision easier.";



class Featured extends React.Component {
    constructor(props) {
        super(props);
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }

    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    render() {
        return (
            <div>
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <PageDescription pageDescription={mainDescription} indexDescription="index-description" />
                <div className="container">
                    <Row>
                        <ReviewedItems />
                    </Row>
                </div>
            </div>
        )
    }
}

module.exports = Featured
