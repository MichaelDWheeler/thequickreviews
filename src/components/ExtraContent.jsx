import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class ExtraContent extends React.Component {

    render() {

        return (
            <div className="container">
                <Row>
                    <Col xs={12}>
                        <h2 className="pb-20 item-title">{this.props.extraContentTitle}</h2>
                        <p>{this.props.extraContentSummary}</p>
                        <div>
                            {this.props.extraContentInfo}
                        </div>
                    </Col>
                    <Col xs={12}>
                        <div className="horizontal-separator"></div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = ExtraContent;
