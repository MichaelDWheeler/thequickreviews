import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class AdditionalInformation extends React.Component{

    render() {
        let verbToUse, makePlural;
        if (this.props.about.endsWith("s")) {
            verbToUse = "are";
            makePlural = null;
        } else {
            verbToUse = "is";
            makePlural = "s";
        }
        if (this.props.about.endsWith("Men") || this.props.about.endsWith("Women")) {
            makePlural = null;
        }
        return(
            <div className="container">
                <Row>
                    <Col xs={12}>
                        <h2 className="pb-20 item-title">Additional Information About {this.props.about}{makePlural} You May Find Useful</h2>
                        <p>
                            The information in this section has nothing to do with helping you determine what the best <span className="lowercase">{this.props.about}</span> {verbToUse}. Instead, it is additional information that we thought you may find beneficial about <span className="lowercase">{this.props.about}{makePlural}</span>, whether or not you make a purchase.
                        </p>
                        <div>
                            {this.props.moreInfo}
                        </div>
                    </Col>
                    <Col xs={12}>
                        <div className="horizontal-separator"></div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = AdditionalInformation;
