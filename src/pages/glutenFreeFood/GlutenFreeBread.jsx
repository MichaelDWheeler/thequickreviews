import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";


let url = "glutenFreeBread",
title ="Best Gluten Free Bread",
image= "../../js/img/glutenFreeBackground.jpg",
mobileImage="../js/img/glutenFreeBackground.jpg",
parallaxAlt="Grains of various colors piled together",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of Gluten Free Bread We Compared</h3>
        <p>
            We did not limit ourselves to one particular type of gluten free bread such as wheat, white or whole grain bread. Instead, we selected the popular breads that consumers were purchasing in this category which included those types of bread. We did however limit our recommendations to gluten free sliced sandwich bread. There are other styles of bread such as biscuits, muffins, Focaccia, Ciabatta, bread rolls and much more but we stuck with sandwich bread.
        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Do We Feel Makes The Best Gluten Free Bread?</h3>
            <p>
                Gluten free bread requires more ingredients to make when compared to typical wheat based breads, and as a result it is usually more expensive. Gluten is the protein that binds the dough together and allows it to have an elastic texture. It also is partially responsible for the shape of the bread. There are two main proteins in gluten and the one that causes the issues in people is called Gliadin. Gliadin in bread can upset some people's stomachs or even cause worse side effects. All of the breads listed here are gluten free so that those who have issues with gluten are still able to enjoy bread without the worry. The lack of gluten can change both the flavor and texture. With that in mind, these are the features that we rated the bread on:
            </p>
            <ul>
                <li>
                    <strong>
                        Smell:&nbsp;
                    </strong>
                    Not all breads smell alike! Some gluten free breads have a sweeter scent, others have an unfamiliar scent, while the ones we rated highly have a more authentic scent. Scent plays a major role in the flavor of the foods you are eating. If you have ever eaten when your nose is completely stuffed up, you may recall that the food was difficult to taste and even harder to enjoy. Without a proper smell, the bread will not taste the same.
                </li>
                <li>
                    <strong>
                        Flavor:&nbsp;
                    </strong>
                    If you are transitioning to a gluten free diet, it is likely that one of the first products you will be replacing is bread. To make the transition easier we would recommend going for a gluten free bread that either tastes as authentic as possible to normal bread, or even superior for that matter. Let your experience be an enjoyable one. Gluten free breads that have superior or authentic flavor is very important to us.
                </li>
                <li>
                    <strong>
                        Texture:&nbsp;
                    </strong>
                    Gluten free bread often has a different texture than standard wheat based breads. This is because the gluten is the glue that holds the bread together and gluten free bread uses alternatives. These alternatives often change the texture, either making it more dense or lighter depending on the bread. A texture that is vastly different than what one is accustomed to can make the difference between loving a bread and throwing it out. Breads that have a more authentic texture rated higher with us.
                </li>
                <li>
                    <strong>
                        Structural Integrity:&nbsp;
                    </strong>
                    We are not talking about buildings here, we are talking about bread. It is a fact that many gluten free breads suffer from crumbling easily. This is especially true as the bread ages and dries. Some companies have figured out a way to keep their bread intact for longer periods of time, and those breads rated higher with us as well.
                </li>
                <li>
                    <strong>
                        Shelf Life:&nbsp;
                    </strong>
                    The longer the shelf life, the better. Almost all gluten free breads will need to be frozen or kept in the proper environment to improve the shelf life but some do it better than others. We kept an eye out for bread that could last for a few days in a bread box when fresh. And then we looked out for the breads that could be frozen and thawed with no noticeable negative effects. Not all of the breads we have listed can last in a bread box and need to be frozen immediately, but that was not the deciding factor on whether or not to include them in our review. It was however a big enough factor to change how we placed the bread overall in our recommendation.
                </li>
            </ul>
    </div>
],

item1Title = "Schar Gluten Free Multigrain Bread - 14.1 Oz",
item1Description = "This certified gluten free multigrain bread comes in a 6 pack. It is a new recipe. To us, this bread has a great flavor and texture and it scored high on our list because of this. This bread is a great bread for supporting celiac's disease. It is preservative free, gluten free, wheat free as well as lactose free which can be beneficial for those who have digestive issues. It is baked with care with a traditional European recipe which provides top nutrition with it's superior grains and honey ingredients. The company that bakes this bread has devoted more than 30 years of research in order to better serve those with gluten sensitivity as well as other digestive issues.",
item1url = url + "/" + encodeURIComponent('#')+"image1",
href1="https://www.amazon.com/Schar-Gluten-Free-Multigrain-Bread/dp/B003UMRNQE/ref=as_li_ss_il?ie=UTF8&qid=1487874526&sr=8-1&keywords=Schar+Gluten+Free+Multigrain+Bread+-+14.1+Oz&linkCode=li3&tag=bestvoted-20&linkId=a4b2cbe7d1e13a2e4ef47d686661540e",
pixel1="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B003UMRNQE",
item1ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003UMRNQE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item1ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003UMRNQE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item1ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003UMRNQE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item1WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Outstanding smell and flavor when compared to normal and gluten free bread`,
    },
    {
        "id": "2",
        "positiveFeature": `Does not fall apart or crumble easily when fresh compared to other gluten free breads`,
    },
    {
        "id": "3",
        "positiveFeature": `Upon first opening, bread can be eaten fresh out of the package without toasting and tastes great`,
    },
    {
        "id": "5",
        "positiveFeature": `Each slice contains vitamins, minerals and is a great source of fiber`,
    },
    {
        "id": "4",
        "positiveFeature": `Shelf life of this gluten free bread is longer than that of typical gluten free bread`,
    },
],

item2Title = "Schar Gluten Free Artisan White Bread - 14.1 Oz",
item2Description ="In second place is the white bread version of the bread that holds first place in the gluten free bread category and it also comes in a 6 pack. Schar Artisan White Bread is Europe's number 1 gluten free bread. This bread comes in a new recipe and has an improved texture which is nice and soft. This bread is baked with care with it's traditional European recipe which contains superior grains and honey and provides vitamins and minerals as well as being an excellent source of fiber. For those with digestive issues this bread was baked to be gluten free, wheat free, as well as lactose free. It is also non GMO and made with high quality ingredients. This gluten free bread does not contain any preservatives of any kind. As with the first place winner in the gluten free category, this bread is also certified to be gluten free. Because this bread does not contain any wheat, it has a texture that is less dense than gluten free wheat bread.",
item2url = url + "/" + encodeURIComponent('#')+"image2",
href2="https://www.amazon.com/Schar-Artisan-White-Bread-Gluten/dp/B00759X5I8/ref=as_li_ss_il?ie=UTF8&qid=1487874689&sr=8-1&keywords=Schar+Gluten+Free+Artisan+White+Bread+-+14.1+Oz&linkCode=li3&tag=bestvoted-20&linkId=81eebceca368b2edcba2247b31aa7290",
pixel2="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00759X5I8",
item2ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00759X5I8&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item2ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00759X5I8&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item2ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00759X5I8&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item2WhatPeopleLike =  [
    {
        "id": "1",
        "positiveFeature": `Can be enjoyed straight out of the package when fresh`,
    },
    {
        "id": "2",
        "positiveFeature": `Enjoyable for even those who do not have gluten sensitivity`,
    },
    {
        "id": "3",
        "positiveFeature": `Soft and delicious bread`,
    },
    {
        "id": "4",
        "positiveFeature": `Tastes like regular bread`,
    },
],

item3Title = "New Grains Gluten Free White Sandwich Bread, 32 oz Loaf",
item3Description = "This bread made by New Grains Gluten Free Bakery ships in a single loaf package. For those with sensitive stomachs or other digestive issues, this bread is gluten free, wheat free, dairy free, soy free, peanut free and it does not contain malt. It is also non GMO. This bread is full sized and can be used to make standard sandwiches instead of small sandwiches. It does contain natural preservatives. ",
item3url = url + "/" + encodeURIComponent('#')+"image3",
href3="https://www.amazon.com/New-Grains-Gluten-Free-Sandwich/dp/B006QG556Q/ref=as_li_ss_il?ie=UTF8&qid=1487875360&sr=8-1&keywords=New+Grains+Gluten+Free+White+Sandwich+Bread,+32+oz+Loaf&linkCode=li3&tag=bestvoted-20&linkId=745ff63a3dd833033151569ca9030510",
pixel3="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B006QG556Q",
item3ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B006QG556Q&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item3ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B006QG556Q&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item3ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B006QG556Q&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item3WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Bread can be eaten and enjoyed straight from the package due to the flavor and texture`,
    },
    {
        "id": "2",
        "positiveFeature": `When fresh, this gluten free bread maintains it structure and does not crumble or fall apart easily`,
    },
    {
        "id": "3",
        "positiveFeature": `After freezing, with a light toasting this bread is very enjoyable`,
    },
    {
        "id": "4",
        "positiveFeature": `Crust's color resembles non-gluten free bread more closely than some other gluten free bread`,
    },
],

item4Title = "Udi's Gluten Free Wholegrain Bread 12 Oz",
item4Description = "This Udi's gluten free wholegrain bread comes in a pack of 4 and boasts that it has bold and wholesome flavor. Unlike most other gluten free breads, this one can be eaten straight from the bag and does not require toasting in order to enjoy it's flavor or texture. ",
item4url = url + "/" + encodeURIComponent('#')+"image4",
href4="https://www.amazon.com/Udis-Gluten-Free-Wholegrain-Bread/dp/B007XZTDYY/ref=as_li_ss_il?ie=UTF8&qid=1487875810&sr=8-1&keywords=udi's+gluten+free+whole+grain+bread+12+oz+(pack+of+4)&linkCode=li3&tag=bestvoted-20&linkId=64c49ca344e778d9660ef099dcfcfdf8",
pixel4="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B007XZTDYY",
item4ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007XZTDYY&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item4ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007XZTDYY&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item4ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007XZTDYY&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `The taste and texture of this gluten free bread is very similar to standard wheat breads`,
    },
    {
        "id": "2",
        "positiveFeature": `Does not need to be toasted in order to taste good`,
    },
    {
        "id": "3",
        "positiveFeature": `Store well in a bread box but should be frozen for best results`,
    },
],

item5Title = "Udi's Gluten Free White Sandwich Bread",
item5Description = "This bread ships in a case (contains 8 loaves which should be frozen) and is a customer favorite due to it's light and fluffy texture and signature taste. It is made from all natural ingredients and does not contain any additional fillers. For those who have been unable to eat white bread due to digestive issues, this bread could allow you to enjoy the sandwich life once again.",
item5url = url + "/" + encodeURIComponent('#')+"image5",
href5="https://www.amazon.com/Udis-Gluten-White-Sandwich-Bread/dp/B0049OQJXA/ref=as_li_ss_il?ie=UTF8&qid=1487875959&sr=8-1&keywords=Udi's+Gluten+Free+White+Sandwich+Bread+(1+Case)&linkCode=li3&tag=bestvoted-20&linkId=3d5ec19d75574ef2dedb66b11f04e9e9",
pixel5="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B0049OQJXA",
item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0049OQJXA&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item5ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0049OQJXA&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0049OQJXA&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Known for being a great tasting gluten free bread`,
    },
    {
        "id": "2",
        "positiveFeature": `Has a longer shelf life than most other gluten free breads`,
    },
    {
        "id": "3",
        "positiveFeature": `Very similar to standard white bread in terms of flavor`,
    },
    {
        "id": "4",
        "positiveFeature": `Does not easily fall apart or crumble compared to other gluten free breads`,
    },
    {
        "id": "5",
        "positiveFeature": `No weird aftertaste left in mouth`,
    },
],

subject = "Gluten Free Breads",
moreInfo = [
    <div>
        <a name="store"></a>
        <h3>How To Store Your Gluten Free Bread</h3>
        <p>
            Gluten free breads are known to dry out pretty quickly in comparison to normal bread. If this is your first time purchasing gluten free bread, you will want to take additional precautions to make sure you will be able to enjoy it a few days later. After reading our review on gluten free bread and then purchasing the one you felt was the best, if you improperly store it you will never know how good it truly was and will think we steered you wrong. Or you may just think something is wrong with us for making the recommendation. To avoid that, make sure you follow these tips to improve the shelf life of your gluten free bread.
        </p>
        <ol>
            <li><strong>Use A Bread Box:&nbsp;</strong></li>
            <a href="http://amzn.to/2mbADvG">Bread boxes</a> are great for standard bread, but even better for gluten free bread. Remember how we said that gluten free bread lacks a protein that helps to bind the bread? Well that protein is also responsible for helping to maintain it's moisture. This tends to make the bread dry out rapidly. Bread boxes are great because they have a cool and dark interior that does not promote the growth of bacteria as rapidly as storing the bread in a sealed, transparent bag at room temperature does. Plus they look nice. Ever heard the saying "Out of sight, out of mind"? Well the opposite holds true here. An attractive bread box in plain sight can cause you to eat it quicker. The faster you eat it, the less you will have to worry about storing it for longer periods of time. Use a bread box if you have fresh bread and will eat it all within 3 days or less.
            <li><strong>Freeze It:&nbsp;</strong></li>
            If you don't plan to eat it within 3 days, you should freeze it. Freezing it will keep it much longer than letting it sit out. After freezing it you can very quickly microwave a slice or lightly toast it for best results. When freezing it, you can put paper towels/napkins between each slice to make it easier to remove and separate. While this may sound like a hassle it only takes a few minutes and does help to keep the gluten free bread more managable when you are ready to eat it.
            <li><strong>Keep The Bread Out Of The Refrigerator:&nbsp;</strong></li>
            Most newer refrigerators are frost-free. This means they remove the moisture from within the refrigerator. Bread that is stored out of a sealed container will dry very quickly because of this. Bread stored in a sealed bag will still suffer from getting cool and go stale quickly. It is a no win situation for refrigeration and bread. The reason gluten free bread (or any bread) goes stale quickly when cooled has to do with the starch it contains. The starch restructures itself in a cooler temperature which hardens it. However, freezing the bread will dramatically slow this process down.
            <li><strong>Do Not Store The Gluten Free Bread In A Plastic Bag At Room Temperature:&nbsp;</strong></li>
            Storing normal bread and gluten free bread are different beasts. If you store your gluten free bread in a plastic bag at room temperature be prepared for it to go bad quickly as it will get moldy.
        </ol>
        <a name="eat"></a>
        <h3>How To Make Your Gluten Free Bread More Enjoyable</h3>
        <p>
            Some companies have done a fantastic job of making gluten free bread that tastes very similar or outright superior (to some people) than standard bread. When you first receive your bread, be sure to at least taste it while fresh straight out of the package. You may find that when fresh, it tastes incredibly good. If you try this though, and determine you and this particular brand of gluten free bread don't mesh well fresh together then the alternative is to lightly toast it. For bread that maintains it's structure you can toast it in a standard toaster. For the gluten free bread that falls apart in the toaster, instead place it on an oven rack. Gluten free bread when lightly toasted often makes the bread more enjoyable.
        </p>
    </div>
],

links = [
    {
        "id" : 3,
        "title" : "Our Top Picks",
        "tag" : "#picks",
    },
    {
        "id" : 1,
        "title" : "The Types Of Gluten Free Bread We Compared",
        "tag" : "#types",
    },
    {
        "id" : 2,
        "title" : "What Do We Feel Makes The Best Gluten Free Bread",
        "tag" : "#features",
    },
    {
        "id" : 4,
        "title" : "How To Store Your Gluten Free Bread",
        "tag" :  "#store",
        "extra" : "Additional Information",
    },
    {
        "id" : 5,
        "title" : "How To Make Your Gluten Free Bread More Enjoyable",
        "tag" :  "#eat",
    },
    {
        "id": 100,
        "title": "What Do You Think Makes The Best " + subject + "?",
        "tag": "#discussion",
        "extra": subject + " Discussion",
    },
];


class PageDisplayed extends React.Component{
    constructor(props) {
        super(props);
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links}/>

                <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>

                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike}/>


                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}

module.exports = PageDisplayed
