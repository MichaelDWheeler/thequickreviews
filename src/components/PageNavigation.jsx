import React from "react";
import { connect } from "react-redux";
import { ReactBootstrap, Col, Row } from "react-bootstrap";
import { hideAwayFromView, resetInitialScroll, linkClick} from "../actions/FixedHeaderAction.jsx";
import JumpTo from "./JumpTo.jsx";


@connect((store) => {
    return {
        showFixedHeader: store.fixedHeader.showFixedHeader,
        initialScroll: store.fixedHeader.initialScroll,
        linkClick: store.fixedHeader.linkClick,
    }
})

class PageNavigation extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let links = this.props.links;

        const linkTitle = links.map(title => {
            if(title.extra){
                return(
                    <div key={title.id}>
                        <div className="links-extra">
                            <strong>{title.extra}</strong>
                        </div>
                        <li><JumpTo classes="own-line" href={title.tag} title={title.title}/></li>
                    </div>
                )
            }else{
                return(
                    <li key={title.id}><JumpTo classes="own-line" href={title.tag} title={title.title}/></li>
                )
            }

        });

        return(
            <div className="container">
                <Row>
                    <Col xs={12}>
                        <nav className="quick-links">
                            <a name="quick-links"></a>
                            <h3>Quick Links</h3>
                            <ol>
                                {linkTitle}
                            </ol>
                        </nav>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = PageNavigation;
