import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";


class Disqus extends React.Component {

    componentDidMount() {
        var disqus_config = function () {
            this.page.url = this.props.url;
            this.page.identifier = this.props.id;
            console.log(this.page.url);
        };

        (function () { 
            var d = document, s = d.createElement('script');
            s.src = 'https://thequickreviews.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    }

    render() {

        return (
            <div className="container pt-55">
                <Row>
                    <Col xs={12}>
                        <h3>Lets Talk About The Best {this.props.subject}</h3>
                        <a name="discussion"></a>
                        <div id="disqus_thread">Test</div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = Disqus;