import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import JumpTo from "../../components/JumpTo.jsx";
import Video from "../../components/Video.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

let url = "best-cat-toys",
    title = "Best Cat Toys",
    image = "../../js/img/cats.jpg",
    mobileImage = "../js/img/cats.jpg",
    parallaxAlt = "Cat laying on brick wall outside with grass in background",
    subject = "Cat Toys",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>The Types Of {subject} We Compared</h3>
            <p>Cats can be funny when it comes to what they consider toys. They see the world and it's objects differently than we do, and they process that information differently. It is not uncommon that a cat's owner (or is the correct way to phrase that as the person who is owned by the cat???) will go out and purchase what they consider to be a grand toy. The toy of toys for cats. It can be the most extravagant, most expensive cat toy/scratcher/post that can be found. After much work getting the box inside of the house, and after taking the time to set it up the cat has little interest in the new toy of toys and instead is happily playing with the box it came with. With this in mind, we felt we needed to do more than just find what toys exist for cats. We wanted to find out why cats play, and what drives them to do what they do. We wanted to know the science behind the play and then we would be able to find appropriate cat toys that we could list as the best cat toys for our top picks. As a result, we selected toys that many might not consider toys at all. In our defense, our selections did exactly what many consider a standard toy to do. That is it kept the cats engaged and allowed them to play. The types of cat toys that we ended up considering were cat toys that stimulated a cat's natural drives and instincts. Some of the toys are toys that have movement, some of them provide cover. A few allow the cats to relieve the need to scratch and others have herbs that naturally attract the cat to them. A few of the toys allow you to be included in the play. In all, we compared a variety of toys and each one stimulated the cat's natural desires to attract the cat and to engage with the toys in a healthy way. </p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What Do We Feel Makes The Best {subject}?</h3>
            <p>After much research, we feel the best cat toys are the ones that do one or more of four things. They either use movement, provide cover, provide an outlet to scratch, or is an appropriate toy where catnip can be used to improve the play. We'll explain each of those in detail and why we feel this way.</p>
            <ul>
                <li><strong>Movement toys: </strong>Cat toys that utilize movement to engage the cat does two main things. It stimulate the cat's prey drive as well as allows the cat to practice their hunting skills. A cat naturally has an instinct to chase things that move. This is the cat's prey drive. A kitten that is separated and lived apart from other cats it's entire life will automatically be attracted to movement and chase down things that catch it's attention and move away from it. Prey drive does not have to be taught and it does not improve with practice. All cats exhibit different levels of prey drive. Some cats will naturally have a very high prey drive and can't seem to help themselves from chasing down an object that is skittered across the floor. Other cats may have a very low prey drive and may not get out of their comfortable bed when an object is jingled a few inches from their face. Hunting skills are different. Those are learned. Unless a cat practices, it will not know how to successfully hunt even if it has a high prey drive. In the wild, cats are usually taught how to hunt by their mothers. In the beginning when the kittens are nursing the mother cat will bring her own food home and eat it in front of the kittens. As the kittens grow, the mother will begin to bring the dead prey home for the kittens to eat on thier own. Then, she will bring injured prey home that she kills in front of the kittens. After that, she may begin bringing home injured prey that the kittens need to kill in order to eat. The injured prey is an excellent way for the kittens to practice on a slow moving target for the inexperienced hunter. As time goes on and the kittens grow and learn, they begin to join the mother cat in the hunt. Kittens learn by witnessing and then doing in the wild.<sup><JumpTo href="#ref-1" title="[1]" /></sup> Domesticated cats are different. Even if they are weaned by their mothers, the mothers do not bring home dead or injured prey. The cats do not hunt together. Cat toys that have movement allow them to develop the hunting skills that they would use on prey in the wild. By practicing, the cat is given the opportunity to adjust their timing, speed and methods. The movement will grab the cat's attention and cause it to investigate but the final "kill" will be achieved from practicing different ways of hunting the prey. This can be a full out sprint and attack, to a sneaky, low belly crawl and then pounce, to rushing in and swatting then biting. Some cat toys with movement also have sound. These toys are great for initially getting the cat's attention, and then giving the cat the chance to hunt it. There are different ways a cat hunts and toys that give them the opportunity to practice different methods are well suited for cats. Our top picks for the best cat toys would be incomplete if we did not include movement toys.</li>
                <li><strong>Toys that provide cover: </strong>Cats love to hide, and they hide for a variety of reasons. Cats that are not feeling well, are in pain, discomfort, or that may have a medical issue tend to hide and remain withdrawn. If your cat is hiding most of the time, you should consider a trip to the veterinarian before purchasing a toy that provides cover. If you cat is just relaxing or playfully hiding, and is not withdrawn then cat toys that provide cover can be the perfect gift for your cat. One of the reasons cats like to hide is that they are able to have place to relax. Small spaces let the cats conserve their body's heat as well as offer them a place out of view from potential predators and other threats.<sup><JumpTo href="#ref-2" title="[2]" /></sup> On their own, cats will seek out places to hide. They can hide in all types of spaces that are not only hard to find, but they may be dangerous. They can crawl into areas and get stuck or areas that present another danger that the cat and the owner may be unaware of. Cat toys that provide a place to hide or lots of cover are made for this purpose. Cat's often jump in and out of cover when playing and toys that allow this to be easily accomplished get used often. These types of cat toys are safe and the pet owner determines the placement of the items. </li>
                <li><p><strong>Scratch toys: </strong>Cats are famous (or infamous) for scratching furniture. They do not scratch because they are being malicious or because they are upset at you. They scratch because this behavior is natural and instinctive. Your cat may be domesticated but that doesn't change it's biology. It's claws will still grow and they can scratch to help remove the outer layer of their claws. They can scratch to visually claim their territory as well.  A cat's paws will produce a scent through the scent glands located in them. Scratching helps to transfer that scent when marking territory. Like most other mammals, cats will need to stretch and flex. By using thier nails to latch onto something they can aid themselves in stretching. When scratching and digging in to something it helps to stretch their paws and the muscles responsible for contracting their paws. When the cats nails grow, if you do not provide an outlet for your cat to deal with these issues, it can pass these issues onto you and your furniture. Cat toys that take into account the cat's biology and natural instinct can give you peace of mind and save your furniture while keeping your cat occupied.</p></li>
                <li><strong>Toys that use catnip: </strong>There is no denying that cats go crazy for catnip although you may not know why. Catnip is an aromatic plant from the mint family. It is known by other names such as catswort and catmint. It's scientific name is <em>Nepeta cataria</em> and it is a species of the genus Nepeta in the family Lamiaceae.<sup><JumpTo href="#ref-3" title="[3]" /></sup> The reason cats go crazy for catnipis because of an oil the plant produces known as "nepetalactone". The scent of this oil stimulates the same receptors that are responsible for detecting pheromones. When stimulated, this gives the cats an overwhelming sense of happiness. Toys that contain catnip can keep a cat engaged for quite some time. Before spending excessive amounts of money on toys that contain catnip, be sure your cat responds to catnip. Not all cats do. If your cat does, then experiment on the types of cat toys that seem to keep your cat the most engaged. See the video below to see the reaction some cats have with catnip.<a name="video"></a><Video source="https://www.youtube.com/embed/J5Xrcp6k8VE" altText="Effects of catnip on cats" liEmbed /></li>
            </ul>
        </div>
    ],

    item1Title = "PetFusion Ultimate Cat Scratcher Lounge",
    item1Description = "If a cat plays with it, in it, or on it, we consider it a toy because the outcome is the same as any other toy's purpose which is keeping the cat safely engaged. The PetFusion Ultimate Cat Scratcher Lounge is our top pick for the best cat toy for great reasons. It is one of our selections that many may not consider a toy, but we feel it belongs in this category. The first reason is that it provides three of our four criteria when we determined what the best cat toys need to accomplish. It provides cover for the cats. They can hide inside of it, or even rest on top of it. This item is light enough that it can be hung on the walls as shelves are. Cats like to rest in high places and will be happy to rest on them out of the way if you do. It is made out of a durable material that is made for scratching. It also includes premium USA organically grown catnip leaves that can be sprinkled on to attract cats. The thoughtful design is curved for your cats comfort as well as being attractive in it's surroundings. It is a neutral color so as not to clash with other colors in the room. It is made out of recycled cardboard and is designed so that each side is identical to the other. This means you get two times the use because this toy is reversible. The large surface of this scratcher is large enough to allow multiple cats to rest and play comfortably. As cats prefer the feel of cardboard to furniture, having one of these in your home can make the difference between your furniture being damaged and unsightly versus nice and proper looking.",
    href1 = "http://amzn.to/2ph7PVV",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X6UEH6&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X6UEH6&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X6UEH6&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B004X6UEH6",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Fulfills 3 out of our 4 criteria in determining the best cat toys`,
        },
        {
            "id": "2",
            "positiveFeature": `Can be used by multiple cats at once`,
        },
        {
            "id": "3",
            "positiveFeature": `Light enough to be hung out of the way`,
        },
        {
            "id": "4",
            "positiveFeature": `Design and neutral color won't clash with most rooms`,
        },
        {
            "id": "5",
            "positiveFeature": `Provides a safe place for cats seeking cover`,
        },
        {
            "id": "6",
            "positiveFeature": `Made for scratching`,
        },
        {
            "id": "7",
            "positiveFeature": `Includes organically grown catnip to attract cats`,
        },

    ],

    item2Title = "Ethical Products Spot Colorful Springs Wide 10pk",
    item2Description = `The Ethical Products Spot Colorful Springs are simple toys that can be used solo by the cats or enjoyed by the both of you. When given to cats for solo play they will make up their own games and play styles as they attempt to find new ways to "hunt" these springs. These springs can keep your cat entertained for hours. For interactive play you can toss them across the room or insert them onto your fingers, compress them, and let them shoot off. Cats are known to carry them around in their mouth, drop them, swat them, pounce them, lose them, and chase them. These make great toys for kittens and cats alike.`,
    href2 = "http://amzn.to/2phwFFk",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B016L9R2Z4&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B016L9R2Z4&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B016L9R2Z4&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B016L9R2Z4",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Great for solo play or interactive play`,
        },
        {
            "id": "2",
            "positiveFeature": `Can provide hours of enjoyment for your cat`,
        },
        {
            "id": "3",
            "positiveFeature": `Spring design allows cats to hone their hunting skills and methods which stimulates natural desire`,
        },
    ],

    item3Title = "Friends Forever Cat Toys Variety Pack, 20 pieces",
    item3Description = "The Friends Forever brand is the number one brand at both Petco and Petsmart. This variety pack contains 20 pieces of assorted toys that can provide your cat with hours of fun. The variety keeps your cat from becoming easily bored. It contains toys that allow your cat to enjoy supervised solo play, as well as interactive toys that can be enjoyed by both you and your cat. For cats that respond to catnip, this variety pack includes it. These toys were designed with cat play in mind and are a great way for your cat to release pent up energy to stay active and healthy.",
    href3 = "http://amzn.to/2qjj2Ut",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01LXE4RGR&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01LXE4RGR&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01LXE4RGR&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B01LXE4RGR",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Assorted pack has a variety of toys that can keep your cat entertained for hours`,
        },
        {
            "id": "2",
            "positiveFeature": `Cat toy can be used solo by cats or interactive with you`,
        },
        {
            "id": "3",
            "positiveFeature": `Includes toys with different features - Some jingle - some crinkle - some have catnip - some have feathers`,
        },

    ],

    item4Title = "Cat-A-Lack 6-Piece Foam Balls",
    item4Description = "The Cat-A-Lack 6 pc foam ball set features 6 lightly textured balls that are soft, spongy and perfect for batting around. The balls are colorful and will spring back into shape even after crushing or squashing them. They have such little weight that it is easy for your cat to launch them into the air when swatting, swiping, batting, and kicking them. They are made with a material that will even bounce on most carpets. Cats love to play solo with these, but you can also toss them around for interactive play time.",
    href4 = "http://amzn.to/2pM4Q9B",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00CI7YA54&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00CI7YA54&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00CI7YA54&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B00CI7YA54",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Lightweight balls won't cause damage if tossed or swatted around`,
        },
        {
            "id": "2",
            "positiveFeature": `Foam balls return to shape after being stepped on, sat on, crushed, etc...`,
        },
        {
            "id": "4",
            "positiveFeature": `So light that they can easily be launched into air and then bounce from cat`,
        },
        {
            "id": "5",
            "positiveFeature": `Bright colors make balls easy to find when not hidden`,
        },
        {
            "id": "6",
            "positiveFeature": `Soft enough that cats can grip them in teeth and carry around`,
        },

    ],

    item5Title = "Da Bird Single 3 Foot Pole Cat Toy with 2 Extra Guinea Feather Refills",
    item5Description = "Da Bird Single 3 Foot Pole Cat Toy is a toy that most all cats love. This toy was designed with your cats natural prey drive in mind. Using this pole will mimic the sounds and look of a real bird which helps to attract your cat. The pole allows you to manipulate the feather's position. You can put it slightly above you cat and then much higher up causing your cat to leap in the air after it. You can make it scurry across the floor and watch your cat try to pounce on it, swat it, and chase it. It is a great way to exercise and tire out your cat. This kit includes two additional Guinea feather refill attachment. If you enjoy playing with your cat and not simply watching your cat play alone, this wand can provide you with hours upon hours of fun.",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    href5 = "http://amzn.to/2pvMNmS",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000FWAP8A&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000FWAP8A&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000FWAP8A&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B000FWAP8A",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Fun interactive play for both you and your cat`,
        },
        {
            "id": "2",
            "positiveFeature": `Wand allows you to control where the games take place`,
        },
        {
            "id": "3",
            "positiveFeature": `Great way to exercise cat`,
        },
        {
            "id": "4",
            "positiveFeature": `Rotating feature helps to recreate the flapping sounds of wings`,
        },

    ],

    item6Title = "Yeowww! Catnip Toy, Yellow Banana",
    item6Description = "If your cat likes catnip, it will love the Yeowww!! Yellow Banana Catnip Toy. It measures 6.3 inches by 5.8 inches, by 1.5 inches and is filled with 100% organic catnip. Cats that like catnip are known to carry these around, bite them, kick them, cuddle with them, roll over them, and try to steal them from one another.",
    href6 = "http://amzn.to/2pgXDNl",
    item6url = url + "/" + encodeURIComponent('#') + "image6",
    item6ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000RX7OKO&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item6ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000RX7OKO&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item6ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000RX7OKO&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel6 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B000RX7OKO",
    item6WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Filled with 100% organic catnip`,
        },
        {
            "id": "2",
            "positiveFeature": `Cats can carry them around and cuddle with them`,
        },
        {
            "id": "3",
            "positiveFeature": `Heavy duty cotton is soft enough for cats to bite while offering stronger construction than other toys made with lighter materials`,
        },

    ],

    item7Title = "Prosper Pet Cat Tunnel - Crackle Play Toy - Collapsible Chute",
    item7Description = "The Prosper Pet Cat Tunnel has multiple ways to allow your cat to play for hours on end. The tunnel itself gives your cat the cover it enjoys while playing and resting. It has built in crinkle paper and a hanging soft ball toy that you cat can swat. A hole at the center allows your cat to poke it's head out before sinking back in to be sneaky. It is made from an ultra strong polyester material that is tear resistant to protect against cat's that love to scratch. It is easily transportable and stored as it can be collapsed in seconds. It is large enough that multiple cats can play together at once and resistant enough that it can be shared with other small animals such as dogs or rabbits.",
    href7 = "http://amzn.to/2pJlE0Z",
    item7url = url + "/" + encodeURIComponent('#') + "image7",
    item7ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019GNDKN0&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item7ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019GNDKN0&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item7ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019GNDKN0&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel7 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B019GNDKN0",
    item7WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Gives cats cover to play and to rest`,
        },
        {
            "id": "2",
            "positiveFeature": `Built in features such as crinkle paper and ball`,
        },
        {
            "id": "3",
            "positiveFeature": `Built in features such as crinkle paper and ball`,
        },
        {
            "id": "4",
            "positiveFeature": `Can be used by more than just cats`,
        },
        {
            "id": "5",
            "positiveFeature": `Scratch resistant material`,
        },
        {
            "id": "6",
            "positiveFeature": `Collapsible tunnel is easy transported and stored`,
        },

    ],

    item8Title = "Bergan Turbo Scratcher Cat Toy",
    item8Description = [<div>The Bergan Turbo Scratcher Cat Toy is a simple toy that can give your cat hours of fun. It is designed so that a ball set in a channel can be swatted with a cat's paw or even pushed with it's nose which will cause it to spin around. The interior of this cat toy includes a scratch pad that will help your cat satisfy it's scratching desires while possibly helping to save your furniture. The textured scratch pad is not only durable, it can also be <a href="http://amzn.to/2oYINID">replaced</a> so you never have to worry about too much wear.</div>],
    href8 = "http://amzn.to/2qgjx4A",
    item8url = url + "/" + encodeURIComponent('#') + "image8",
    item8ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000IYSAIW&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item8ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000IYSAIW&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item8ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000IYSAIW&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel8 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B000IYSAIW",
    item8WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Both a toy that allows cat to chase and scratch`,
        },
        {
            "id": "2",
            "positiveFeature": `Center scratch pad can be replaced as it wears out`,
        },
        {
            "id": "3",
            "positiveFeature": `10 inch scratch pad provides ample room for claws`,
        },
        {
            "id": "4",
            "positiveFeature": `Ball is easily pushed by kittens and older cats`,
        },
    ],

    item9Title = "Petstages Tower of Tracks Cat Toy",
    item9Description = "The Petstages Tower of Tracks is similar to the cat toy we have listed as our number 8 pick for best cat toys but the three levels create a different experience for the cats that play with it. Because it has multiple levels, the cats can stay in the same place and wait for the balls to come around. The different levels make it more difficult for the cat to have a bird's eye view which means it will be eagerly awaiting for the ball to appear from the other side. Cats have been known to play with this for hours, and though they can remain in the same place they often have a great time circling around it and throwing their arms in it to hit the balls as they come around the side. The three levels take up less space than 3 separate toys would require but offer plenty of stimulation due to more than one item being in motion. This toy works well for older and younger cats as the balls move very freely and do not require much effort to manipulate.",
    href9 = "http://amzn.to/2pJoVxe",
    item9url = url + "/" + encodeURIComponent('#') + "image9",
    item9ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DT2WL26&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item9ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DT2WL26&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item9ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DT2WL26&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel9 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B00DT2WL26",
    item9WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Small footprint toy can provide hours of enjoyment while keeping home free from scattered small objects`,
        },
        {
            "id": "2",
            "positiveFeature": `Center safety bar helps to prevent cats' heads from becoming stuck`,
        },
        {
            "id": "3",
            "positiveFeature": `Multiple objects in motion provide mental stimulation and excitement for cats`,
        },
        {
            "id": "4",
            "positiveFeature": `Non-skid pad helps to keep toy from being pushed around non-carpeted flooring`,
        },

    ],

    item10Title = "The Ripple Rug - Cat Activity Play Mat - Fun Interactive Play - Training - Scratching - Multi Use Habitat Bed Mat",
    item10Description = "The Ripple Rug Play Mat is a simple concept that is surprisingly effective at getting cats to play and hide. It uses special non-fray ribbed needle-punch carpet made from recycled plastic bottles with a thermally insulated non-slip backing as the base. The top carpet has holes that allow your cat the see through them. Because the top is a material that is easily shaped you can create numerous layouts and anchor them down with the Velcro attachments. Safety slits in the holes allow larger cats to use them, and the slits can be extended with scissors. The materials of this toy are non toxic and are resistant to stain, mold, and mildew. No VOC's are used in this toy. One of the reasons this seemingly non-cat toy is so effective is because it can be shaped into new and exciting spaces that will allow your cat to hide, retain body heat as well as provide a means for them to see their surroundings. This cat toys is incredibly easy to transport and to store",
    href10 = "http://amzn.to/2phPhoA",
    item10url = url + "/" + encodeURIComponent('#') + "image10",
    item10ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B016H3T8EC&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item10ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B016H3T8EC&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item10ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B016H3T8EC&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel10 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B016H3T8EC",
    item10WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Simple play mat toy can give cats new and exciting places to hide`,
        },
        {
            "id": "2",
            "positiveFeature": `Made from non-toxic, mold, mildew, and stain resistant material`,
        },
        {
            "id": "3",
            "positiveFeature": `Can be used by multiple cats at the same time`,
        },
        {
            "id": "4",
            "positiveFeature": `Non-slip backing and Velcro attachments help to keep play mat in position and not in separate pieces in your home`,
        },
        {
            "id": "5",
            "positiveFeature": `Very easy to transport and store`,
        },

    ],
    moreInfo = [
        <div>
            <a name="play-with-your-cat"></a>
            <h3>Play With Your Cat</h3>
            <p>Cats are often called solitary creatures which leads many people to believe that they do not require human interaction. This is not true. At least for the cats that live with people. Believe it or not, cats are social creatures. If you already have a cat as the member of the family you may be well aware of this. Many cats will allow you to pet them and sit on your lap for hours on end. They will sleep at the foot or head of the bed with you. They can tell when you are upset. Some will try to comfort you in those times. All of these actions are social actions. So when you hear cats are solitary creatures know that this is a myth. It is true that cats are often independent, but they enjoy social interaction and you can bond with your cat. One of the ways that you can bond with your cat that is enjoyable for both of you is by playing with it. As with everything, there are usually tools that make the task at hand easier, and in this instance these tools would be called toys.</p>
            <a name="cat-toys-are-tools-of-the-trade"></a>
            <h3>Cat Toys Are Tools Of The Trade</h3>
            <p>Cat toys allow you to take part in your cat's fun and well being by offering what your cat desires. Cats play for a number of reasons, and many of them are due to it's own natural instincts and drive. The best cat toys will tap into those natural drives because "playing" then becomes instinctual. In the wild, cats often play to help develop a skill. When they playfully pounce on one another or stalk one another it is because they are practicing stalking and hunting prey.</p>
            <p>Cat toys such as <a href="#image5">this 3 foot cat pole</a> help to extend your reach and can give your cat the exercise it needs in order to stay healthy. You can exercise your cat while sitting on the couch or in a chair if you desire. No matter how you decide to use it, ultimately it is you that determines where and when the cat plays with it. You want your cat to associate this pleasent experience with you by keeping it fun.</p>
            <a name="benefits-of-playing-with-cat"></a>
            <h3>What Are The Benefits Of Playing With Your Cat?</h3>
            <p>Playing with your cat offers benefits to both you and your cat. Among those are:</p>
            <ul>
                <li>Exercises for your cat: This can help to maintain a healthier weight and longer life for your cat.</li>
                <li>Bonding: Playing with your cat can strengthen the bond between your cat and you.</li>
                <li>Self confidence for your cat: Playing with a cat that has low self confidence or may be shy can help to improve it's confidence levels, especially towards you.</li>
                <li>Good for you: Animal companions and playing with them can improve our health as well. There is a growing evidence that pets offer pet therapy and can reduce blood pressure<sup><JumpTo title="[4]" href="#ref-4" /></sup> which can possibly extend our own lives.</li>
                <li>Help reduce anxiety when transitioning to a new environment: As many of us know, moving into a new home can be tramatic for both us and our pets. Cats have been known to hide for days on end until they feel secure enough or hungry enough to explore. Playing with your cat and exercising it can really go a long way in helping them overcome anxiety. Using toys you can introduce them room to room and play with them in each room at their own pace. Get them focused on the game and less on the environment and help them ease into their new surroundings.</li>
            </ul>
            <a name="references"></a>
            <h3>References</h3>
            <ol>
                <li><a name="ref-1"></a>Dr. Kim Smyth. <a href="https://www.gopetplan.com/blogpost/on-the-prowl-petplan-pet-insurance-discusses-prey-drive-and-hunting-behaviors-in-cats">Prey Drive and Hunting Behaviors in Cats </a>June 10 2013</li>
                <li><a name="ref-2"></a>Purina. <a href="http://www.purina.com.au/cats/behaviour/hiding">Why do cats hide? </a>January 10 2016</li>
                <li><a name="ref-3"></a>Wikipedia. <a href="https://en.wikipedia.org/wiki/Catnip">Catnip </a>April 29 2017</li>
                <li><a name="ref-4"></a>Baun MM, Bergstrom N, Langston NF, Thoma L. <a href="https://www.ncbi.nlm.nih.gov/pubmed/6563527">Physiological effects of human/companion animal bonding. </a>1984 May-Jun;33(3):126-9.</li>
            </ol>
        </div>
    ],
    links = [
        {
            "id": 1,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 2,
            "title": `The Types Of ${subject} We Compared`,
            "tag": "#types",
        },
        {
            "id": 3,
            "title": `What Do We Feel Makes The Best ${subject}`,
            "tag": "#features",
        },
        {
            "id": 4,
            "title": "Video: Learn Why Cat Toys With Catnip Work",
            "tag": "#video",
            "extra": "Additional Information",
        },
        {
            "id": 5,
            "title": "Play With Your Cat",
            "tag": "#play-with-your-cat",
        },
        {
            "id": 7,
            "title": "Cat Toys Are Tools Of The Trade",
            "tag": "#cat-toys-are-tools-of-the-trade",
        },
        {
            "id": 8,
            "title": "What Are The Benefits Of Playing With Your Cat?",
            "tag": "#benefits-of-playing-with-cat",
        },
        {
            "id": 6,
            "title": "References",
            "tag": "#references",
        },
        {
            "id": 100,
            "title": `What Do You Think Makes The Best ${subject}?`,
            "tag": "#discussion",
            "extra": `${subject} Discussion`,
        },
    ];

class PageDisplayed extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        ga('create', 'UA-92142805-1', 'auto');
        ga('send', 'pageview');
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }

    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav extended={true} state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} item6Image={item6ImageAComp} item7Image={item7ImageAComp} item8Image={item8ImageAComp} item9Image={item9ImageAComp} item10Image={item10ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} alt6tag={item6Title} alt7tag={item7Title} alt8tag={item8Title} alt9tag={item9Title} alt10tag={item10Title} />

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links} />

                <PageDescription date={this.date()} pageDescription={mainDescription[0]} />

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

                <Description itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />


                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                <Description itemTitle={item6Title} description={item6Description} href={href6} pixel={pixel6} source={item6ImageA} alttag={item6Title} url={item6url} place="6" sup="th" name="image6" />
                <WhatPeopleLike whatPeopleLike={item6WhatPeopleLike} />

                <Description itemTitle={item7Title} description={item7Description} href={href7} pixel={pixel7} source={item7ImageA} alttag={item7Title} url={item7url} place="7" sup="th" name="image7" />
                <WhatPeopleLike whatPeopleLike={item7WhatPeopleLike} />

                <Description itemTitle={item8Title} description={item8Description[0]} href={href8} pixel={pixel8} source={item8ImageA} alttag={item8Title} url={item8url} place="8" sup="th" name="image8" />
                <WhatPeopleLike whatPeopleLike={item8WhatPeopleLike} />

                <Description itemTitle={item9Title} description={item9Description} href={href9} pixel={pixel9} source={item9ImageA} alttag={item9Title} url={item9url} place="9" sup="th" name="image9" />
                <WhatPeopleLike whatPeopleLike={item9WhatPeopleLike} />

                <Description itemTitle={item10Title} description={item10Description} href={href10} pixel={pixel10} source={item10ImageA} alttag={item10Title} url={item10url} place="10" sup="th" name="image10" />
                <WhatPeopleLike whatPeopleLike={item10WhatPeopleLike} />

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />
            </div>
        )
    }
}

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-92142805-1', 'auto');
ga('send', 'pageview');

module.exports = PageDisplayed
