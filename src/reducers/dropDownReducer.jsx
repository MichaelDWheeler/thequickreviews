export default function reducer(state={
    showDropDownNavigation: false,
}, action) {

    switch (action.type){
        case "TOGGLE_DROPDOWN": {
            return {...state, showDropDownNavigation: !state.showDropDownNavigation}
        }
    }
    return state;
}
