export default function reducer(state={
    showFixedHeader: false,
    initialScroll: false,
    linkClick: false,
}, action){
    switch(action.type){
        case "SHOW_FIXED_HEADER":{
            return {...state, showFixedHeader: true, initialScroll: true}
            break;
        }
        case "HIDE_FIXED_HEADER":{
            return {...state, showFixedHeader: false}
            break;
        }
        case "RESET_INITIAL_SCROLL":{
            return {...state, initialScroll: false}
            break;
        }
        case "LINK_CLICK": {
            return {...state, linkClick: true }
            break;
        }
        case "NOT_LINK_CLICK" :{
            return {...state, linkClick: false}
            break;
        }
    }
    return state;
}
