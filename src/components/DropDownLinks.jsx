import React from "react";
import { backgroundAction } from "../actions/BackgroundAction.jsx";
import { toggleNavigation } from "../actions/DropDownActions.jsx";
import { ReactBootstrap, Col, Row } from "react-bootstrap";
import { connect } from "react-redux";
import NavigationLinks from "./NavigationLinks.jsx";

@connect((store)=> {
    return {
        showDropDownNavigation: store.dropDown.showDropDownNavigation,
    }
})

class DropDownLinks extends React.Component{
    constructor(props){
        super(props)
    }
    componentWillMount(){
        document.addEventListener('click', this.checkClick.bind(this));
    }

    checkClick(e){
        const navigationBox = this.refs.pageNavigation;
        if(navigationBox != undefined && e.target !== navigationBox && e.target.className != "fa fa-caret-down fa-2x" && e.target.className !="brand"){
            this.props.dispatch(toggleNavigation());
            this.props.dispatch(backgroundAction());
        }
    }
    render(){
        let dropDownLinks;
        if(this.props.showDropDownNavigation){
            dropDownLinks =
            <div className="drop-down-navigation" ref="pageNavigation">
                <h3 className="drop-down-title"><a href="/">All&nbsp;Reviews</a></h3>
                <nav>
                    <NavigationLinks />
                </nav>
            </div>
        }else{
            dropDownLinks = <div></div>
        }
        return(
            <div>
                {dropDownLinks}
            </div>

        )

    }

}

module.exports = DropDownLinks;
