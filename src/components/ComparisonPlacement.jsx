import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class ComparisonPlacement extends React.Component{
    render(){
        return(
            <div>
                <div className="winners">
                    <div className="placement">
                        <div className="inner-comparison">
                            <div className="place-number text-center">{this.props.place}<sup>{this.props.sup}</sup></div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
module.exports = ComparisonPlacement;
