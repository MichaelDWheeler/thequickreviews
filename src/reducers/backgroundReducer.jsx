export default function reducer(state={
    toggleBackgroundFilter: false,
}, action){
    switch (action.type){
        case "TOGGLE_BACKGROUND": {
            return{...state, toggleBackgroundFilter: !state.toggleBackgroundFilter }
        }
    }
    return state;
}
