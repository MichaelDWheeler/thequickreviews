import React from "react";
import { ReactBootstrap, Col, Row, Image } from "react-bootstrap";

class AdrienneWheeler extends React.Component {
    render() {
        let about;
        let url = document.location.pathname;
        if (url === "/") {
            about = null;
        } else {
            about =
                <div className="container pt-55">
                    <Row>
                        <Col xs={12}>
                            <div className="mobile-center">
                                <div className="adrienne-wheeler flow-left">
                                    <Image className="inline-block" src="../js/img/adriennewheeler.jpg" style={{ maxWidth: '200px' }} circle alt="Adrienne Wheeler main contributor and founder of thequickreviews.com" />
                                </div>
                            </div>
                            <h3 className="inline-block">About the Author</h3>
                            <div className="contact-me">
                                <p>Adrienne Wheeler is the founder and a main contributor of thequickreviews.com. Please feel free to leave her any  comments or suggestions through <a href="mailto:wheelermadrienne@gmail.com">her email </a> or through her  <a href="//plus.google.com/u/0/113448703106430073520?prsrc=3"
                                    rel="publisher" target="_top" style={{ textDecoration: 'none' }}>
                                    <img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style={{ border: '0', width: '32px', height: '32px' }} />
                                </a> account. Thanks for reading!</p>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <div className="horizontal-separator"></div>
                        </Col>
                    </Row>
                </div>
        }
        return (
            <div>
                <aside>
                    {about}
                </aside>
            </div>
        )
    }
}

module.exports = AdrienneWheeler;