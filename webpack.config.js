var debug = process.env.NODE_ENV != "production";
console.log(process.env.NODE_ENV);


var webpack = require('webpack');
var path = require('path');

module.exports = {

    context: path.join(__dirname, "src"),
    devtool: debug ? "inline-sourcemap" : false,
    entry: "./js/client.js",
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: ['react-html-attrs', 'transform-decorators-legacy', 'transform-class-properties'],
                }
            },
            {
                test: /\.(gif|jpe|jpg|png|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /.*\.(gif|png|jpe?g|svg|mov|JPG)$/i,
                loaders: [
                    'file-loader?name=/img/[name].[ext]',
                    'image-webpack-loader?{pngquant:{quality: "65-90", speed: 4}, mozjpeg: {quality: 65}}'
                ]
            }
        ]
    },
    output: {
        path: __dirname + "/src/js/",
        filename: "client.min.js"
    },
    plugins: debug? []: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    ],
};
