import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";


let url = "glutenFreeBreadMix",
title ="Best Gluten Free Bread Mix",
image= "../../js/img/glutenFreeBackground.jpg",
mobileImage="../js/img/glutenFreeBackground.jpg",
parallaxAlt="Grains of various colors and types in small piles",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of Bread Mixes We Compared</h3>
        <p>
            To put it simply, we compared bread mixes. We didn't leave it to a specific type of bread such as sandwich bread, or biscuits. We obviously excluded any type of mix that couldn't be called a bread mix such as pancake mix or cake mix but other than that we kept it simple. If the mix is to make bread, it was fair game. The reason being is because bread mix can also be used for other purposes. There are many recipes that can be made from it that are far and away from gluten free sandwich bread only. We wanted to find what we considered the best gluten free bread mix so that we could present our findings to you.
        </p>
    </div>
],
mainDescription=[
    <div>
        <a name="why"></a>
        <h3>Why Buy Gluten Free Bread Mix Or Gluten Free Anything?</h3>
        <p>
            For those with a gluten sensitivity, gluten free products are a great way to enjoy the foods that they may have become accustomed to without the negative side effects that products that contain gluten give them.  It can be harder to find in stores as supplies are often limited but fortunately many different gluten free brands are readily available online. The main reason gluten free products have gained in popularity is because many people cannot properly digest products that contain gluten. Some believe that gluten sensitivity is on the rise due to various reasons such as GMO wheat, damaged gut flora as a result of high antibiotic usage, and that more wheat products exists now than in previous generations.
        </p>
        <a name="features"></a>
        <h3>Why Purchase Gluten Free Bread Mix Over Gluten Free Loaves Of Bread?</h3>
        <p>
            What is gluten and why would you want to purchase gluten free bread mix? To answer the "why", you first need to know the "what". Gluten is a mixture of two different proteins. The reason that many bread products contain gluten is that gluten is responsible for allowing the dough to have it's elasticity by letting it bind to itself. This allows the bread to have it's shape and stay together. Gluten is extremely popular and effective at what it is used for. Unfortunately for people who are sensitive to gluten, they either have to abstain from eating products that contain gluten altogether or they have to find an alternative. Alternatives such as gluten free bread often cost more to purchase since it requires more ingredients to make.  Most gluten free breads must either be frozen when stored or they go bad quickly. Once the gluten free bread has been baked, the time on it's shelf life starts. When you order bread online, you have to take the transit time into account. Gluten free bread also tends to crumble or fall apart easier since gluten is absent from it, especially as it dries. Gluten free bread has come a long way over the decades and those who have been unable to enjoy a sandwich or toast can now do so but they still must bear the additional costs and care of the bread. When you make your own gluten free bread, the shelf life of the bread is not wasted on the time it takes to be delivered to you. You don't have to worry about it going bad by being in the wrong environment such as being in a hot box on a summer day on your porch. It can also allow you to only make enough so that you can eat it while still fresh. One of the best benefits however, is you can create custom creations and make it in a way that you know your family is sure to love. Plus, gluten free bread mix can often be used for <a href="#more">more than just bread</a>. It gives you versatility in your creations.
        </p>
        <p>
            We have listed what we consider to be the top 5 brands of gluten free bread mix based on reviews so that you can see what people like the best.
        </p>
    </div>],
    subject = "Gluten Free Bread Mixes",
    moreInfo = [
        <div>
            <a name="store"></a>
            <h3>How To Store Your Gluten Free Bread Mix</h3>
            <p>
                Once you receive your gluten free bread mix you should store it in an airtight container. Make sure to keep it away from moisture as well as heat. This goes for both opened and unopened bread mix.  Once you have made sure it is sealed you can store it in a cool place, the refrigerator, or the freezer for best results. Bread mix can be stored in the refrigerator for a time, unlike <a href="/glutenFreeBread#store">storing gluten free bread</a> because the starch has not been destructured yet. Properly storing your bread mix will help to preserve it so that it will last longer. The type of packaging used for the bread mix will play a role in how long it can be stored. Products such as <a href="http://amzn.to/2muxc5M">Pamela's 25 Pound Bag Of Gluten Free Bread Mix</a> can last up to a year when frozen due to the packaging they use. However different brands may have different specifications and you should always check on the packaging or contact the manufacturer if this information is not present. Never assume that your gluten free bread mix will last any length of time other than what the manufacturer confirms that it can otherwise it may be bad when you go to use it.
            </p>
            <aside>*Please note: This is only for the bread mix. Once you bake the actual loaf, you should not store the loaf in the refrigerator as it will dry out and become stale.</aside>
            <a name="more"></a>
            <h3>How To Make More Than Bread With Gluten Free Bread Mix</h3>
            <p>
                Although it is called bread mix, you may be surprised to discover that you can make so much more than just bread! This is another huge benefit over purchasing loaves of bread. You can make dishes such as cinnamon rolls, caramel apple monkey bread, apple pie crust, crumb cake, cookies, crab cakes, pot pies, italian meatballs, potato latkes, apple galettes, pizza bagels, hot dog buns, hamburger buns, italian meatloaf, cheese puff pastry crackers, and so much more! This is because many of the bread mixes can also be used as an all purpose general flour that is gluten free. Some of the manufacturers of the gluten free bread mix have a recipes page on their website that you can check prior to purchasing the mix. Doing so can open up a whole new world for gluten sensitive individuals.
            </p>

        </div>
    ],

    links = [
        {
            "id" : 3,
            "title" : "Our Top Picks",
            "tag" : "#picks",
        },
        {
            "id" : 1,
            "title" : "The Type Of Bread Mixes We Compared",
            "tag" : "#types",
        },
        {
            "id" : 6,
            "title" : "Why Buy Gluten Free Bread Mix Or Gluten Free Anything?",
            "tag" : "#why"
        },
        {
            "id" : 2,
            "title" : "Why Purchase Gluten Free Bread Mix Over Gluten Free Loaves?",
            "tag" : "#features",
        },
        {
            "id" : 4,
            "title" : "How To Store Your Gluten Free Bread Mix",
            "tag" :  "#store",
            "extra" : "Additional Information",
        },
        {
            "id" : 5,
            "title" : "How To Make More Than Bread With Gluten Free Bread Mix",
            "tag" :  "#more",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ],

    item1Title = "Pamela's Products Gluten Free Bread Mix in a 25-Pound Bag",
    item1Description = "This is a 25 pound bag of gluten free bread mix that can be used for more than making just simple bread loaves. It can be used to make mouth watering dinner rolls, savory pie crusts, fragrant cinnamon rolls, delicious pizza bread, scrumptious bagels, and can be used for any of your other breading needs such as gravy or even stuffing. For those with sensitivity, this bread mix is not only gluten free, but dairy free and egg free as well.  It contains no artificial additives or high fructose corn syrup. The ingredients in this 25 pound bag or gluten free bread mix are high quality and the company itself is a member of The NON-GMO project which promotes keeping products free from genetically modified organisms.",
    item1url = url + "/" + encodeURIComponent('#')+"image1",
    href1="https://www.amazon.com/Pamelas-Products-Gluten-Bread-25-Pound/dp/B0028VAS8S/ref=as_li_ss_il?ie=UTF8&qid=1487876373&sr=8-1&keywords=Pamela's+Products+Gluten+Free+Bread+Mix+in+a+25-Pound+Bag&linkCode=li3&tag=bestvoted-20&linkId=374333a6b7dd435b508ac5eadc86485e",
    pixel1="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B0028VAS8S" ,
    item1ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0028VAS8S&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item1ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0028VAS8S&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20" ,
    item1ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0028VAS8S&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Bulk purchase`,
        },
        {
            "id": "2",
            "positiveFeature": `Makes approximately 20 loaves of gluten free bread when using 1.3 pounds of mix`,
        },
        {
            "id": "4",
            "positiveFeature": `Can use yeast but does not require it and can be used to make yeast and gluten free bread`,
        },
        {
            "id": "5",
            "positiveFeature": `Similar taste to real bread`,
        },
        {
            "id": "6",
            "positiveFeature": `Can be frozen up to a year without effecting taste or quality`,
        },
        {
            "id": "7",
            "positiveFeature": `Can be mixed by hand and baked in a standard oven`,
        },
        {
            "id": "8",
            "positiveFeature": `Adding ingredients only takes a few minutes`,
        },
        {
            "id": "9",
            "positiveFeature": `Company website has recipes for using this gluten free bread mix`,
        },
        {
            "id": "10",
            "positiveFeature": `Can be baked in as little as 5 minutes using a bread machine`,
        },
        {
            "id": "11",
            "positiveFeature": `Can be used as all purpose flour`,
        },
        {
            "id": "12",
            "positiveFeature": `Bread that is made has good texture and less crumble than many other brands`,
        },
    ],

    item2Title = "Chebe Gluten Free Bread Original Cheese Bread Mix",
    item2Description = "This gluten free bread mix produces a Brazilian-style, great tasting pao de queijo (cheese bread) that is not only gluten free, but yeast free as well. Some of the ingredients that are included in this gluten free bread mix come from the tropical manioc plant which is also known as the yucca plant and cassava plant. The bread is quick and easy to prepare and can be used for a variety of different foods as it is very versatile and need not be only used for making cheese bread. Other foods that can be made are sandwich buns, rolls, flatbread, bread-sticks, pretzels, hot dog rolls, pie crust, dumpling, tortillas, crackers and so much more. It is certified GFCO. It comes in a pack of 8 which gives a total of 60 ounces. Recipes for this gluten free bread mix can be found on the company's website. This bread mix is gluten free, soy free*, corn free*, rice free*, potato free*, yeast free, peanut free, tree nut free*, iodine free, and sugar free. While it does not contain eggs, you can add egg or egg substitute. Note: This product is made on shared equipment that does make other products that do contain the above marked(*) ingredients but it is under a strict cleaning and allergen control program.",
    item2url = url + "/" + encodeURIComponent('#')+"image2",
    href2="https://www.amazon.com/Chebe-Bread-Original-Cheese-7-5-Ounce/dp/B001ACMCNU/ref=as_li_ss_il?ie=UTF8&qid=1487876750&sr=8-2&keywords=Chebe+Gluten+Free+Bread+Original+Cheese+Bread+Mix&th=1&linkCode=li3&tag=bestvoted-20&linkId=e70adbe874e80705ae45b554eb1b45a6",
    pixel2="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B001ACMCNU",
    item2ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001ACMCNU&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001ACMCNU&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001ACMCNU&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Easy to make gluten free bread`,
        },
        {
            "id": "2",
            "positiveFeature": `Consistenly good flavor`,
        },
        {
            "id": "3",
            "positiveFeature": `Prep time is approximately 10 minutes and bake time is approximately 25 minutes`,
        },
        {
            "id": "4",
            "positiveFeature": `Unique chewy texture`,
        },
    ],

    item3Title = "Namaste Foods Gluten Free Bread & Roll Mix",
    item3Description = "Namaste Gluten Free Bread & Roll Mix is allergen free bread that is described as being in the tradition of bread that your grandma used to make. It does not contain gluten, wheat, egg, dairy, peanut, tree nuts, soy, mustard, sulfites, sesame, caseine, fish, crustaceans, shellfish, potato, corn, high fructose corn syrup or artificial colors, flavors, & preservatives. To make the bread from the bread mix you need to add eggs or egg substitute, oil, and water. It is proudly made in the USA and ships in certified frustration free packaging. ",
    item3url = url + "/" + encodeURIComponent('#')+"image3",
    href3="https://www.amazon.com/Namaste-Foods-Gluten-Bread-16-Ounce/dp/B000LKVHOW/ref=as_li_ss_il?ie=UTF8&qid=1487877099&sr=8-1&keywords=Namaste+Foods+Gluten+Free+Bread+&+Roll+Mix&linkCode=li3&tag=bestvoted-20&linkId=a2b3019415fcdf3c70c7a02a1567e805",
    pixel3="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B000LKVHOW",
    item3ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000LKVHOW&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000LKVHOW&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000LKVHOW&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Makes tasty and moist bread that is similar to non-gluten free bread`,
        },
        {
            "id": "2",
            "positiveFeature": `Can be baked in the oven if bread machine is not available`,
        },
        {
            "id": "3",
            "positiveFeature": `Makes a dense bread`,
        },
        {
            "id": "4",
            "positiveFeature": `Great bread for those with alleries as it is wheat free, gluten free, dairy free, corn free, soy free, potato free, casein free, and nut free`,
        },
    ],

    item4Title = "Glutino Gluten Free Pantry Favorite Sandwich Bread Mix",
    item4Description = "This gluten free bread mix is also wheat free, contains no cholesterol, no trans-fat, and is Kosher. It is a great bread mix for bread machines, electric mixers, or by hand and it produces a nice moist and dense sandwich bread while filling your house with an aroma that will have your neighbors knocking at your door. It is a new formula and size and the company states that customers tell them that they will never go back to eating gluten. ",
    item4url = url + "/" + encodeURIComponent('#')+"image4",
    href4="https://www.amazon.com/Glutino-Gluten-Free-Pantry-Favorite/dp/B00J8BI9FK/ref=as_li_ss_il?ie=UTF8&qid=1487877325&sr=8-1&keywords=Glutino+Gluten+Free+Pantry+Favorite+Sandwich+Bread+Mix&th=1&linkCode=li3&tag=bestvoted-20&linkId=f1e81bb0e7cba9ead370e36782f07c82",
    pixel4="https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00J8BI9FK",
    item4ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00J8BI9FK&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00J8BI9FK&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00J8BI9FK&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Makes a moist bread that does not crumble easily when fresh`,
        },
        {
            "id": "2",
            "positiveFeature": `Very similar to real non-gluten free bread`,
        },
    ],

    item5Title = "King Arthur Flour Bread Mix, Gluten Free, 18.25 Ounce Packages",
    item5Description = 'This gluten free bread mix is great for those with allergies as it is wheat free, soy free, nut free and is packaged in a dedicated gluten free facility. It is certified gluten free by the GFCO. Each box makes a standard 8.5" x 4.5" bread loaf. This bread mix can also be used to make dairy free versions of bread as well as pizza and contains the instructions on the back of the packaging.',
    item5url = url + "/" + encodeURIComponent('#')+"image5",
    href5="https://www.amazon.com/King-Arthur-Flour-Gluten-Packages/dp/B00473NTAO/ref=as_li_ss_il?ie=UTF8&qid=1488221472&sr=8-6&keywords=gluten+free+bread+mix&linkCode=li3&tag=thequickreviews-20&linkId=261fde203b50184fdf1b49f20c9a9586",
    pixel5="https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00473NTAO",
    item5ImageA="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00473NTAO&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageAComp="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00473NTAO&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00473NTAO&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Can be used in bread machine or baked in oven`,
        },
        {
            "id": "2",
            "positiveFeature": `Great tasting gluten free bread`,
        },
        {
            "id": "3",
            "positiveFeature": `Easy to make`,
        },
        {
            "id": "4",
            "positiveFeature": `Directions for making dairy free bread are included on packaging`,
        },
        {
            "id": "5",
            "positiveFeature": `Company claims that it stays fresher longer than bread made from other gluten free bread mixes`,
        },
    ];

    class PageDisplayed extends React.Component{
        constructor(props) {
            super(props);
        }

        date(){
            let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate =`${month} ${day}, ${year}`;
            return newDate;
        }

        year(){
            let d = new Date,
            thisYear = d.getFullYear();
            return thisYear;
        };

        quantity() {
            if (typeof item10Title !== 'undefined') {
                return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
            } else {
                return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
            }
        };

        render() {
            return (
                <div>
                    <MetaTags>
                        <title>{title}</title>
                        <meta id="meta-description" name="description" content={this.quantity()} />
                    </MetaTags>
                    <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                    <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage}/>
                    <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp}  alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title}/>
                    <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                    <PageNavigation links={links}/>
                    <PageDescription date={this.date()} pageDescription={mainDescription[0]}/>

                    <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA}   alttag={item1Title} url={item1url} place="1" sup="st" name="image1"/>
                    <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike}/>

                    <Description itemTitle={item2Title} description={item2Description}  href={href2} pixel={pixel2} source={item2ImageA}  alttag={item2Title} url={item2url}  place="2" sup="nd" name="image2"/>
                    <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike}/>

                    <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3}   source={item3ImageA}  alttag={item3Title} url={item3url} place="3" sup="rd" name="image3"/>
                    <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike}/>

                    <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA}  alttag={item4Title} url={item4url} place="4" sup="th" name="image4"/>
                    <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike}/>

                    <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA}  alttag={item5Title} url={item5url}  place="5" sup="th" name="image5"/>
                    <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                    <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                    <AdrienneWheeler />
                    <Disqus url={url} id={url + "1"} subject={subject} />

                </div>
            )
        }
    }


    module.exports = PageDisplayed
