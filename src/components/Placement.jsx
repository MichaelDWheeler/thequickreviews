import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class Placement extends React.Component{
    render() {
        let word;
        if (this.props.extraTitle) {
            word = "For";
        } else {
            word = "Place";
        }
        return(
            <div>
                <div className="winners">
                    <div className="placement">
                        <div className="inner-container">
                            <div className="place-number text-center">{this.props.place}<sup>{this.props.sup}</sup> {word} {this.props.extraTitle}</div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
module.exports = Placement;
