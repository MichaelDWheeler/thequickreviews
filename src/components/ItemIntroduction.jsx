import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class ItemIntroduction extends React.Component{

    render(){
        return(
            <div className="container">
                <Row>
                    <Col xs={12}>
                        <div>
                            {this.props.catagoryDescription}
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = ItemIntroduction;
