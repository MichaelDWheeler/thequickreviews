import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

import JumpTo from "../../components/JumpTo.jsx";

let url = "naturalDeodorant",
    title = "Best Natural Deodorant",
    image = "../../js/img/bathroom.jpg",
    mobileImage = "../js/img/bathroom.jpg",
    parallaxAlt = "Bathroom where natural deodorant is used",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>What We Compared To Find The Best Natural Deodorant</h3>
            <p>
                When looking for the best natural deodorant we looked for deodorants that used natural ingredients to combat odor and contained as few unnatural ingredients as possible. This means it did not contain aluminum first and foremost, and ideally it did not contain parabens, propylene glycol, triclosan, or synthetic fragrances. The type of application was not important to us at all. Whether it was a stick application, roll on, paste, or spray, it was fair game. Many of the deodorants which are labeled as natural deodorants from the manufacturer still do contain some of the chemicals we listed above while others do not. The main criteria for us was that it was effective and contained as few unnatural ingredients as possible. The natural deodorants that did not contain any of those substances or fewer of those substances scored higher with us. The most important ingredient that we do not want to see present in the deodorant is aluminum. While technically aluminum is not present in a pure single functioning deodorant (aluminum prevents sweating and by adding it to a deodorant it is also becomes a dual functioning deodorant + antiperspirant) we still looked for the best natural deodorant that did not contain it. Our top picks of the ten natural deodorants are what we consider to be the best natural deodorants that are also aluminum free. 
            </p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What Do We Feel Makes The Best Natural Deodorant</h3>
            <p>
                We believe that the best natural deodorants are deodorants that are highly effective at keeping away unpleasent body odors with natural ingredients. They should be easy to apply, non-staining, and long lasting. As the armpits are generally more sensitive than other areas of the skin, they should be suitable for as many people as possible. While no deodorant will be 100% free from irritation from everyone, if it was created to ease sensitivity we scored it higher.  When it comes to fragrances, we feel that a lightly scented or unscented deodorant is the best for three main reasons. The first is that scent often assumes a gender. By assuming a gender it excludes approximately half of the population from using it. Deodorants that could be used comfortably by both men and women, or even the entire family, we found more appealing. Secondly, some people are very sensitive to fragrances or scents and we felt that excluding the stronger smelling deodorants altogether would alleviate this concern. Lastly, certain scents can clash with your cologne or perfume. What is also important to us is that the deodorant is effective. If it contained none of the substances we were concerned with but did not keep the odor away, it didn't make our top picks. The deodorant had to contain as few of the substances we were concerned with as possible and also be highly effective.
            </p>
        </div>
    ],

    item1Title = "Deodorant Stones of America: Thai Crystal Deodorant For Men And Women, 100% Natural, Aluminum Free - 2 Pack",
    item1Description = "This 100% natural deodorant is our top pick for the best natural deodorant and for good reason! Thai crystal deodorant is made from natural minerals salts that have been crystallized. These crystallized mineral salts kill bacteria on your body that cause the body odor. Thai Crystal Deodorant is only a deodorant, it is not an antiperspirant. This means it will allow your body to naturally cool itself through perspiration, and because it is not blocking that action it is not clogging your pores. What is so great about this deodorant, and the reason we chose it as the best natural deodorant is because it contains no harmful chemicals. Instead of trying to mask or cover up unwanted scents, it takes it a step further and stops the odor at the source by eliminating the bacteria which produces it. It can be used with success at the armpits and on the feet to stop both from producing unwanted odors. Using it is incredibly simple. You would wet the crystallized stone and then apply it generously to the areas where you want to stop the bacteria from reproducing. The area will be dried almost immediately and it won't ruin or damage the clothing you are wearing. On top of all the reasons we just gave, what we really like about this natural deodorant is that it can last much longer than your typical deodorant stick.",
    href1 = "https://www.amazon.com/Deodorant-Stones-America-Thai-Crystal/dp/B001E0YN1W/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-48&keywords=natural+deodorant&refinements=p_72:1248873011&th=1&linkCode=li3&tag=thequickreviews-20&linkId=76b74a8e6e26963be9054dae5d8819be",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E0YN1W&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B001E0YN1W",
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E0YN1W&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E0YN1W&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1WhatPeopleLike = [
        {
            "id": "8",
            "positiveFeature": `Natural deodorant does not contain aluminum`,
        },
        {
            "id": "1",
            "positiveFeature": `Made from crystallized mineral salt`,
        },
        {
            "id": "2",
            "positiveFeature": `Prevents odors from occurring by killing the bacteria responsible for them`,
        },
        {
            "id": "3",
            "positiveFeature": `Allows your body to naturally cool by perspiring without the odors`,
        },
        {
            "id": "4",
            "positiveFeature": `Created without any harmful chemicals`,
        },
        {
            "id": "5",
            "positiveFeature": `Does not ruin clothing`,
        },
        {
            "id": "6",
            "positiveFeature": `Dries almost immediately`,
        },
        {
            "id": "7",
            "positiveFeature": `Lasts considerably longer than typical antiperspirant stick`,
        },

    ],

    item2Title = "Thai Deodorant Stone Crystal Mist Natural Deodorant Spray For Men And Women, Aluminum Free - 2 Pack",
    item2Description = "This spray is for both men and women and works by killing the bacteria responsible for odors before they can create the odors. It does this naturally without using any aluminum or other dangerous chemicals by using 100% mineral salt and purified water. Spray on formula allows easy application underarms, or on feet to combat body odor that occur in those areas. This is a great alternative to the stick form as it can be lightly misted wherever you have a concern, and unlike the Thai Crystal Stone you do not have to wet it first. The mineral salt does not stain and will not ruin your clothing or shoes. This works in the same way that the Thai Crystal Stone works. The bacteria that is responsible for causing the offending odors are eliminated before they get the chance to. This stops the problem at the source. It does not allow the problem to exist only to mask it. Another benefit is that this mist will keep the odors at bay while allowing you to cool yourself naturally through perspiration. Your body cooling itself is a natural function, and when you use an antiperspirant you are stopping that process from occurring as nature intended. You also run the risk of clogging your pores. This spray on formula is great for those who have excess folds or other concerns that would prevent manual application of a deodorant.",
    href2 = "https://www.amazon.com/Thai-Deodorant-Stone-Crystal-Natural/dp/B00Y31W2KI/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-26&keywords=natural+deodorant&refinements=p_72:1248873011&th=1&linkCode=li3&tag=thequickreviews-20&linkId=11b00c975db84c0f5230b95df6246a31",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00Y31W2KI&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00Y31W2KI",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00Y31W2KI&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00Y31W2KI&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2WhatPeopleLike = [
        {
            "id": "6",
            "positiveFeature": `Natural deodorant that does not contain aluminum`,
        },
        {
            "id": "1",
            "positiveFeature": `Made with mineral salt and purified water`,
        },
        {
            "id": "2",
            "positiveFeature": `Spray on formula will not harm clothing or shoes`,
        },
        {
            "id": "3",
            "positiveFeature": `Spray on formula allows easy application to feet or underarms`,
        },
        {
            "id": "4",
            "positiveFeature": `Will not block your body's natural way of cooling itself through perspiration`,
        },
        {
            "id": "5",
            "positiveFeature": `Prevents odors from removing what causes them`,
        },

    ],

    item3Title = "PiperWai Natural Deodorant with Application Spoon Aluminum Free",
    item3Description = "PiperWai natural deodorant is an effective and powerful deodorant. It stops the odor causing bacteria at their source and also uses the power of activated charcoal to absorb moisture and combat stinky body odors. This deodorant is more than a deodorant. It is also a skin conditioner. It contains organic coconut oil, shea butter, and vitamin E. Those with sensitive skin should give it a try as the ingredients are suitable for most skin types. This deodorant will not stain the skin or clothing and it becomes clear when rubbed in. Also included is an application spoon to help reduce the amount of germs that are transferred between the arm pits and deodorant. By using different hands for each application (this is the natural method of application as you use opposite hands to apply under the armpit) it can help keep the bacteria transfer to a minimum. This deodorant works well keeping the moisture away because activated charcoal can absorb up to 1000x it's own weight in moisture.",
    href3 = "https://www.amazon.com/PiperWai-Natural-Deodorant-Application-Spoon/dp/B01KPD3KOE/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490730714&sr=1-2&keywords=piper+wai&linkCode=li3&tag=thequickreviews-20&linkId=bf5f65b168154108eb90da5fa3ff98d9",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KPD3KOE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01KPD3KOE",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KPD3KOE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KPD3KOE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3WhatPeopleLike = [
        {
            "id": "6",
            "positiveFeature": `Aluminum free natural deodorant`,
        },
        {
            "id": "5",
            "positiveFeature": `Contains natural ingredients that help fight body odor and absorb odors as well`,
        },
        {
            "id": "1",
            "positiveFeature": `Natural and effective deodorant that conditions as well as stops odors`,
        },
        {
            "id": "2",
            "positiveFeature": `Active charcoal ingredient helps to reduce moisture`,
        },
        {
            "id": "3",
            "positiveFeature": `Application spoon included can help reduce bacteria from deodorant container`,
        },
        {
            "id": "4",
            "positiveFeature": `Skin conditioning ingredients`,
        },
    ],

    item4Title = "NANINATURALS Unscented Natural Organic Deodorant For Men And Women, Aluminum Free",
    item4Description = "NANINATURALS natural deodorant is for both men and women. This unisex formula is unscented, aluminum free, paraben free, non-GMO, vegan, and gluten free.  It provides all day protection even through vigorous exercise. As many people are concerned with aluminum being present in their products, this natural deodorant does not contain it. Instead, it uses natural products that are safe for the skin. Ingredients such as organic ozonated coconut oil which helps to stop the odor at it's source by killing the bacteria. It also contains other ingredients which condition the skin naturally, lock in moisture, while still allowing it to perspire to cool the body down and remove toxins. Unlike many traditional antiperspirants or deodorants, this one goes on clear and won't cause your clothing to stain. This is a great alternative to chemical deodorants and for those individuals who may have sensitivity to certain allergens.",
    href4 = "https://www.amazon.com/All-Natural-Aluminum-Free-Long-Lasting-Gluten-Free-Underarms/dp/B01NAW18K2/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-25&keywords=natural+deodorant&refinements=p_72:1248873011&linkCode=li3&tag=thequickreviews-20&linkId=2315c9744ca2b2c2d0df41a31f6a55c8",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01NAW18K2&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01NAW18K2",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01NAW18K2&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01NAW18K2&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4WhatPeopleLike = [
        {
            "id": "6",
            "positiveFeature": `Natural deodorant free from aluminum`,
        },
        {
            "id": "1",
            "positiveFeature": `Unisex formula`,
        },
        {
            "id": "2",
            "positiveFeature": `Non-scented formula does not compete with colognes or perfumes`,
        },
        {
            "id": "3",
            "positiveFeature": `Bacteria killing formula stops odors at their source and does not simply mask them`,
        },
        {
            "id": "4",
            "positiveFeature": `Applies clear and will not stain clothing`,
        },
        {
            "id": "5",
            "positiveFeature": `Great for those with sensitive skin or certain allergies`,
        },

    ],

    item5Title = "Essential Eden Unscented Pit Stank Paste Natural Deodorant For Entire Family, Aluminum Free - Net Weight 4 oz ",
    item5Description = "Pit Stank Paste by Essential Eden comes in a glass jar and is safe enough to use for the entire family. Not that we would recommend doing this, but the manufacturer states that it is safe enough to eat! This natural deodorant is made with high quality organic ingredients. It is 100% free from aluminum, parabens, petroleum, propylene glycol, triclosan, artificial colors, or talc. Each jar is double the size so that you get twice the amount of deodorant without having to pay 2x the cost. The jars that the Pit Stank Paste comes in are reusable glass jars and the main ingredient is organic coconut oil which is known for it's natural antibacterial properties. Body odor is created from bacteria, and by removing the bacteria you remove the source of the odor.",
    href5 = "https://www.amazon.com/Essential-Eden-Deodorant-Unscented-Aluminum/dp/B01F4P0YM4/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-27&keywords=natural+deodorant&refinements=p_72:1248873011&linkCode=li3&tag=thequickreviews-20&linkId=064028aec7b9f2bf75d3b75b553a260b",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01F4P0YM4&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01F4P0YM4",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01F4P0YM4&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01F4P0YM4&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "4",
            "positiveFeature": `Natural deodorant formulated without aluminum`,
        },
        {
            "id": "1",
            "positiveFeature": `Safe enough to use for the entire family`,
        },
        {
            "id": "2",
            "positiveFeature": `Unisex unscented deodorant can be shared between family members`,
        },
        {
            "id": "3",
            "positiveFeature": `Easy to apply with finger tips even with tight top on`,
        },

        {
            "id": "5",
            "positiveFeature": `Antibacterial coconut oils helps to prevent the body odor from occurring in the first place by eliminating bacteria that causes it`,
        },
    ],
    item6Title = "nasanta Magnesium Natural Deodorant For Men, Aluminum Free",
    item6Description = "Magnesium Deodorant for men is a 100% aluminum free deodorant that is formulated with magnesium. Magnesium is needed by the body for various bodily functions which include the production of energy, synthesizing protein, muscle relaxation, and more. This deodorant is able to control body odor from the magnesium while allowing the body to cool itself naturally through perspiration. The deodorant is a roll on application and is non staining to clothing. It is free from ethyl alcohol, aluminum, baking soda, parabens, propylene glycol, talc, essential oils, triclosan, and animal testing. This is a deodorant and not an antiperspirant. It allows the body to naturally cool itself through sweat while controlling the offending odors.",
    href6 = "https://www.amazon.com/nasanta-Magnesium-Deodorant-Men-Australian/dp/B014J2G8U0/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-19&keywords=natural+deodorant&refinements=p_72:1248873011&linkCode=li3&tag=thequickreviews-20&linkId=59d608d739b5e80ddac8d80c220b673e",
    item6url = url + "/" + encodeURIComponent('#') + "image6",
    item6ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B014J2G8U0&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel6 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B014J2G8U0",
    item6ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B014J2G8U0&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item6ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B014J2G8U0&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item6WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `100% aluminum free natural deodorant`,
        },
        {
            "id": "2",
            "positiveFeature": `Uses magnesium, a nutrient the body needs to stay healthy, to control odor`,
        },
        {
            "id": "3",
            "positiveFeature": `Non antiperspirant formula allows body to cool naturally as nature intended`,
        },
        {
            "id": "4",
            "positiveFeature": `May last all day with normal activity`,
        },
        {
            "id": "5",
            "positiveFeature": `Non-irritating formula is suitable for all skin types`,
        },

    ],
    item7Title = "Arm & Hammer Essentials Deodorant Aluminum Free, Fresh Scent - 6 Pack",
    item7Description = "Arm & Hammer Essentials fresh scent natural deodorant is a clear gel in a stick application. It uses Arm & Hammer baking soda and natural deodorizers such as rosemary oil and lavandin oil to combat body odor. It is an effective deodorant that does not contain any aluminum or parabens. The fresh scent is very light and this particular deodorant is suitable for both men and women. It does not contain an antiperspirant so it allows your body to naturally cool through perspiration while combating offensive body odors.",
    href7 = "https://www.amazon.com/Arm-Hammer-Essentials-Deodorant-Fresh/dp/B00I07Y9IE/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-34&keywords=natural+deodorant&refinements=p_72:1248873011&linkCode=li3&tag=thequickreviews-20&linkId=55a6745a1001cfa3a7a3306bc2351fa5",
    item7url = url + "/" + encodeURIComponent('#') + "image7",
    item7ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00I07Y9IE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel7 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00I07Y9IE",
    item7ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00I07Y9IE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item7ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00I07Y9IE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item7WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Does not contain any aluminum in this natural deodorant`,
        },
        {
            "id": "2",
            "positiveFeature": `Nice light and fresh scent through the use of rosemary, lavandin, and fruit oils`,
        },
        {
            "id": "3",
            "positiveFeature": `Baking soda is gentle to most skin types and is a natural pH balancer`,
        },
    ],
    item8Title = "Greener Path All Natural Deodorant For Men And Women Aluminum Free Organic For Men And Women, Lavender Scent",
    item8Description = "This organics and all natural deodorant uses superior ingredients so that you can go about your day feeling fresh and clean the entire day. This hypoallergenic formula is completely free from aluminum, parabens, gluten, soy, wheat, dairy, animal cruelty, and is non-GMO. It is a great deodorant to try for those with sensitivities or allergies. It is a non staining formula so it won't discolor or stain your clothing. The main ingredient in this deodorant is coconut oil which is known to be antibacterial. Bacteria are responsible for producing the body odors, and by killing the odor at the source you stop the odor. This all natural deodorant gets it's lavender scent from lavender oil. ",
    href8 = "https://www.amazon.com/Deodorant-Greener-Path-Lavender-Aluminum/dp/B01KWQ1CXK/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-39&keywords=natural+deodorant&refinements=p_72:1248873011&linkCode=li3&tag=thequickreviews-20&linkId=c77aa8cce2b23e96a6867e7c693c9692",
    item8url = url + "/" + encodeURIComponent('#') + "image8",
    item8ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KWQ1CXK&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel8 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01KWQ1CXK",
    item8ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KWQ1CXK&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item8ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KWQ1CXK&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item8WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `All natural, organic deodorant is hypoallergenic and aluminum free`,
        },
        {
            "id": "3",
            "positiveFeature": `Will not stain or discolor clothing`,
        },
        {
            "id": "4",
            "positiveFeature": `Main ingredient is coconut oil which is a known antimicrobial that can stop odors at their source`,
        },

    ],
    item9Title = "Real Purity Roll-On Natural Deodorant Aluminum Free For Men And Women",
    item9Description = "Real Purity Natural Deodorant is a quick drying roll on that is formulated for even those with the most sensitive of skin. All of the ingredients used in this roll on natural deodorant are organic and it make use of many different essential oils. Essential oils include grapefruit, sandalwood, lavender, rosemary, geranium, sage and rosewood. Each essential oil has different properties which helps to combat odors and neutralize them at the source. This roll on natural deodorant provides 24 hour protection and was made with health in mind. It is free from aluminum, parabens, propylene glycol, phthaltes, and triclosan.",
    href9 = "https://www.amazon.com/Real-Purity-Roll-On-Natural-Deodorant/dp/B005FN0QGU/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490633068&sr=1-31&keywords=natural+deodorant&refinements=p_72:1248873011&linkCode=li3&tag=thequickreviews-20&linkId=654f44e4c5f9facf6c3d1d63f0805779",
    item9url = url + "/" + encodeURIComponent('#') + "image9",
    item9ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B005FN0QGU&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel9 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B005FN0QGU",
    item9ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B005FN0QGU&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item9ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B005FN0QGU&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item9WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `No aluminum in this natural deodorant`,
        },
        {
            "id": "2",
            "positiveFeature": `All ingredients are organic`,
        },
        {
            "id": "3",
            "positiveFeature": `Variety of essential oils are used in this formula`,
        },
        {
            "id": "4",
            "positiveFeature": `Provides up to 24 hours of protection`,
        },
    ],
    item10Title = "AC NATURE All Natural Deodorant For Entire Family Aluminum-Free",
    item10Description = "AC NATURE Natural Deodorant is formulated with the finest organic and natural ingredients. This natural deodorant can be shared amongst the entire family and will give up to 24 hours of protection. Like many of the other of our top picks, this one uses organic coconut oil which is safe, and a known antimicrobial. Coconut oil can do more than simply neutralize odors, it stops them from occurring by removing the source of those odors which is bacteria. It also has many other properties which are beneficial to the skin, so it conditions while it combats body odor production. This deodorant will not cause staining on clothing or under the arms. It also contains organic arrowroot powder, organic raw shea butter, and organic tea tree oil which is a well known antibacterial, antifungal, and antiseptic.",
    href10 = "https://www.amazon.com/AC-NATURE-Aluminum-Free-Ingredients-Protection/dp/B01N4QN9EY/ref=as_li_ss_il?s=beauty&ie=UTF8&qid=1490722805&sr=1-13&keywords=natural+deodorant&refinements=p_36:1253951011&th=1&linkCode=li3&tag=thequickreviews-20&linkId=27b8c8cf4547647fad77101618f3e954",
    item10url = url + "/" + encodeURIComponent('#') + "image10",
    item10ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01N4QN9EY&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel10 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01N4QN9EY",
    item10ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01N4QN9EY&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item10ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01N4QN9EY&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item10WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `100% Aluminum free deodorant with natural ingredients`,
        },
        {
            "id": "2",
            "positiveFeature": `Ingredients focus on stopping odors by removing the source and not masking the odors`,
        },
        {
            "id": "3",
            "positiveFeature": `Superior and organic ingredients condition and smooth skin`,
        },
        {
            "id": "4",
            "positiveFeature": `Can provide up to 24 hours of odor protection`,
        },

    ],
    subject = "Natural Deodorants",
    moreInfo = [
        <div>
            <a name="aluminum-in-deodorant"></a>
            <h3>Aluminum In Deodorant</h3>
            <p>Aluminum in a deodorant is somewhat of a misnomer, because the addition of aluminum creates an antiperspirant. The product is still a deodorant because the active ingredients that are responsible for combating the odor is the same, but it is also an antiperspirant due to the addition of aluminum and it's ability to stop perspiration. When most people are searching for a deodorant without aluminum, in reality they are searching for an antiperspirant without aluminum. Deodorants on their own are meant to combat body odors and not combat sweat. Antiperspirants on their own are meant to combat sweat, but not combat body odor. Antiperspirants typically are paired with deodorants while deodorants are not always paired with antiperspirants.</p>
            <p>There are people who are genuinely searching for a deodorant without aluminum. To those that are, this is just a specific way to search for a product that will not impede the body's natural ability to sweat. As aluminum is the main ingredient used for stopping the perspiration, a search for a deodorant without aluminum is a good way to find a pure deodorant that will allow the body to function as intended.</p>
            <p>Deodorant without aluminum is searched for by millions of people each year. The reason it has become such a popular topic is because it is believed by many that it could be a harmful and toxic substance. Those who believe it is often believe that it is harmful to the brain. They believe this because aluminum levels in elderly people's brain have been found to contain as much as 20x the levels of middle aged people<sup><JumpTo classes="inline-block" href="#ref-1" title="[1]" /><JumpTo classes="inline-block" href="#ref-2" title="[2]" /></sup> Furthermore, the Agency for Toxic Substances and Disease Registry which is a part of the United States Department of Health and Human Services, acknowledges that aluminum is a metal that can affect the neurolgical system<sup><JumpTo classes="inline-block" href="#ref-3" title="[3]" /></sup> When looking at the dangers aluminum potentially presents to the brain, the biggest concern for most is it's relationship with Alzheimer's disease. Numerous Universities believe that aluminum is associated with Alzheimer's <sup><JumpTo classes="inline-block" href="#ref-4" title="[4]" /><JumpTo classes="inline-block" href="#ref-5" title="[5]" /><JumpTo classes="inline-block" href="#ref-6" title="[6]" /><JumpTo classes="inline-block" href="#ref-7" title="[7]" /><JumpTo classes="inline-block" href="#ref-8" title="[8]" /><JumpTo classes="inline-block" href="#ref-9" title="[9]" /></sup> and that evidence has caused a big concern among the public.</p>
            <p>There is also a belief by many people that aluminum in deodorants can cause breast cancer. A 2003 study reported that women who shaved and used antiperspirants/deodorants were younger in their breast cancer diagnosis<sup><JumpTo classes="inline-block" href="#ref-10" title="[10]" /></sup>.Although there are no studies that conclusively state this to be the case, and conflicting studies do exist, it is a concern that has spread and become a fear among those who believe it to be true.</p>
            <a name="references"></a>
            <h3>References</h3>
            <ol>
                <li><a name="ref-1"></a>Fernandez-Davila ML, Razo-Estrada AC, Garcia-Medina S, Gomez-Olivan LM, Pinon-Lopez MJ, Ibarra RG, Galar-Martinez M. <a href="https://www.ncbi.nlm.nih.gov/pubmed/21993346" target="_blank">Aluminum-induced oxidative stress and neurotoxicity in grass carp (Cyprinidae-Ctenopharingodon idella)</a>. Ecotoxicol Environ Saf. 2012 Feb;76(2):87-92. doi: 10.1016/j.ecoenv.2011.09.012. Epub 2011 Oct 10.</li>
                <li><a name="ref-2"></a>Jansson ET. <a href="https://www.ncbi.nlm.nih.gov/pubmed/12214020" target="_blank">Aluminum exposure and Alzheimer's disease</a>. J Alzheimers Dis. 2001 Dec;3(6):541-549.</li>
                <li><a name="ref-3"></a>Pohl HR, Roney N, Abadin HG. <a href="https://www.ncbi.nlm.nih.gov/pubmed/21473383" target="_blank">Metal ions affecting the neurological system.</a> Met Ions Life Sci. 2011;8:247-62.</li>
                <li><a name="ref-4"></a>Belojevic G, Jakovljevic B. <a href="https://www.ncbi.nlm.nih.gov/pubmed/?term=Srp+Arh+Celok+Lek.+1998+(7-8)%3A283-9.+Review.+Serbian" target="_blank">[Aluminum and Alzheimer's disease]</a>. Srp Arh Celok Lek. 1998 Jul-Aug;126(7-8):283-9. Review. Serbian.</li>
                <li><a name="ref-5"></a>Shrivastava S. <a href="https://www.ncbi.nlm.nih.gov/pubmed/22575537" target="_blank">Combined effect of HEDTA and selenium against aluminum induced oxidative stress in rat brain.</a> J Trace Elem Med Biol. 2012 Jun;26(2-3):210-4. doi: 10.1016/j.jtemb.2012.04.014. Epub 2012 May 8.</li>
                <li><a name="ref-6"></a>Mandour RA, Azab YA. <a href="https://www.ncbi.nlm.nih.gov/pubmed/23022843" target="_blank">The prospective toxic effects of some heavy metals overload in surface drinking water of Dakahlia Governorate, Egypt.</a> Int J Occup Environ Med. 2011 Oct;2(4):245-53.</li>
                <li><a name="ref-7"></a>Bondy SC. <a href="https://www.ncbi.nlm.nih.gov/pubmed/20553758" target="_blank">The neurotoxicity of environmental aluminum is still an issue.</a> Neurotoxicology. 2010 Sep;31(5):575-81. doi: 10.1016/j.neuro.2010.05.009. Epub 2010 May 27. Review.</li>
                <li><a name="ref-8"></a>Nishida Y. <a href="https://www.ncbi.nlm.nih.gov/pubmed/14577644" target="_blank">Elucidation of endemic neurodegenerative diseases-a commentary.</a> Z Naturforsch C. 2003 Sep-Oct;58(9-10):752-8. Review.</li>
                <li><a name="ref-9"></a>Brenner S. <a href="https://www.ncbi.nlm.nih.gov/pubmed/23261179" target="_blank">Aluminum may mediate Alzheimer's disease through liver toxicity</a>, with aberrant hepatic synthesis of ceruloplasmin and ATPase7B, the resultant excess free copper causing brain oxidation, beta-amyloid aggregation and Alzheimer disease. Med Hypotheses. 2013 Mar;80(3):326-7. doi: 10.1016/j.mehy.2012.11.036. Epub 2012 Dec 20.</li>
                <li><a name="ref-10"></a>McGrath KG. <a href="https://www.ncbi.nlm.nih.gov/pubmed/14639125" target="_blank">An earlier age of breast cancer diagnosis related to more frequent use of antiperspirants/deodorants and underarm shaving.</a> European Journal of Cancer 2003; 12(6):479-485.</li>
            </ol>
        </div >
    ],
    links = [
        {
            "id": 1,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 2,
            "title": "What We Compared To Find The Best Natural Deodorant",
            "tag": "#types",
        },
        {
            "id": 3,
            "title": "What We Feel Makes The Best Natural Deodorant",
            "tag": "#features",
        },
        {
            "id": 4,
            "title": "Aluminum In Deodorants",
            "tag": "#aluminum-in-deodorant",
            "extra": "Additional Information",
        },
        {
            "id": 5,
            "title": "References",
            "tag": "#references",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ];

class PageDisplayed extends React.Component {
    constructor(props) {
        super(props);
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }

    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav extended={true} state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} item6Image={item6ImageAComp} item7Image={item7ImageAComp} item8Image={item8ImageAComp} item9Image={item9ImageAComp} item10Image={item10ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} alt6tag={item6Title} alt7tag={item7Title} alt8tag={item8Title} alt9tag={item9Title} alt10tag={item10Title} />

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links} />

                <PageDescription date={this.date()} pageDescription={mainDescription[0]} />

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

                <Description itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />


                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                <Description itemTitle={item6Title} description={item6Description} href={href6} pixel={pixel6} source={item6ImageA} alttag={item6Title} url={item6url} place="6" sup="th" name="image6" />
                <WhatPeopleLike whatPeopleLike={item6WhatPeopleLike} />

                <Description itemTitle={item7Title} description={item7Description} href={href7} pixel={pixel7} source={item7ImageA} alttag={item7Title} url={item7url} place="7" sup="th" name="image7" />
                <WhatPeopleLike whatPeopleLike={item7WhatPeopleLike} />

                <Description itemTitle={item8Title} description={item8Description} href={href8} pixel={pixel8} source={item8ImageA} alttag={item8Title} url={item8url} place="8" sup="th" name="image8" />
                <WhatPeopleLike whatPeopleLike={item8WhatPeopleLike} />

                <Description itemTitle={item9Title} description={item9Description} href={href9} pixel={pixel9} source={item9ImageA} alttag={item9Title} url={item9url} place="9" sup="th" name="image9" />
                <WhatPeopleLike whatPeopleLike={item9WhatPeopleLike} />

                <Description itemTitle={item10Title} description={item10Description} href={href10} pixel={pixel10} source={item10ImageA} alttag={item10Title} url={item10url} place="10" sup="th" name="image10" />
                <WhatPeopleLike whatPeopleLike={item10WhatPeopleLike} />

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}


module.exports = PageDisplayed
