import React from "react";
import MetaTags from 'react-meta-tags';
import { Row, Col, Image } from 'react-bootstrap';
import { Link } from 'react-router';

let categoryArray = [
    {
        "categoryName": "Electric Pressure Cookers",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00FLYWNYQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "electricPressureCookers",
        "altTag": "The best electric pressure cooker",
        "id": 1
    },
    {
        "categoryName": "Coffee Makers",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0055P70MQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "coffeeMakers",
        "altTag": "The best coffee maker",
        "id": 2
    },
    {
        "categoryName": "Dog Crates",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000Y905XE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "best-dog-crate",
        "altTag": "The best dog crates",
        "id": 3
    },
    {
        "categoryName": "Gluten Free Breads",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003UMRNQE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "glutenFreeBread",
        "altTag": "The best gluten free bread",
        "id": 4
    },
    {
        "categoryName": "Gluten Free Bread Mixes",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001ACMCNU&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "glutenFreeBreadMix",
        "altTag": "The best gluten free bread mix",
        "id": 5
    },
    {
        "categoryName": "Gluten Free Snacks",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0018AXEWC&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "glutenFreeSnacks",
        "altTag": "The best gluten free snacks",
        "id": 6
    },
    {
        "categoryName": "Gluten Free Protein Bars",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001H0G0BK&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "glutenFreeProteinBars",
        "altTag": "The best gluten free protein bars",
        "id": 7
    },
    {
        "categoryName": "Gluten Free Pastas",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EBE831Y&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "glutenFreePasta",
        "altTag": "The best gluten free pastas",
        "id": 8
    },
    {
        "categoryName": "Evening Dresses",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01IEC7800&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "eveningDresses",
        "altTag": "The best evening dresses",
        "id": 0
    },
    {
        "categoryName": "Flea Treatments For Dogs",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004QRHRIQ&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "best-flea-treatment-for-dogs",
        "altTag": "The best flea treatment for dogs",
        "id": 10
    },
    {
        "categoryName": "Prom Dresses",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01KNIBE5I&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "promDresses",
        "altTag": "The best prom dresses",
        "id": 11
    },
    {
        "categoryName": "Natural Deodorants",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E0YN1W&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "naturalDeodorant",
        "altTag": "The best natural deodorant",
        "id": 12
    },
    {
        "categoryName": "Safety Razors",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B019H58OEW&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "best-safety-razor",
        "altTag": "The best safety razor",
        "id": 13
    },
    {
        "categoryName": "Cat Toys",
        "image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004X6UEH6&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
        "link": "best-cat-toys",
        "altTag": "The best cat toys",
        "id": 14
	},
	{
		"categoryName": "Cat Harnesses",
		"image": "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00UU1EXB6&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
		"link": "best-cat-harness",
		"altTag": "The best cat harness",
		"id": 15
	},

];
class ReviewedItems extends React.Component {
    constructor(props) {
        super(props);
    }

    goToTop() {
        document.body.scrollTop = 0;
    }

    render() {
        const sortedArray = categoryArray.sort(function (a, b) {
            var textA = a.categoryName.toUpperCase();
            var textB = b.categoryName.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        const productsListed = sortedArray.map(category => {
            return (
                    <Col xs={12} sm={4} md={4} lg={3} key={category.id}>
                        <Link onClick={this.goToTop.bind(this)} to={category.link}>
                            <div class="image-container">
                                <div className="full-width">
                                    <Image className="images" responsive src={category.image} alt={category.altTag} />
                                </div>
                                <div className="winners">
                                    <div className="placement">
                                        <div className="inner-container">
                                            <div className="category-text">{category.categoryName}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    </Col>
            )
        });
        return (
            <div>
                <MetaTags>
                    <title>The Quick Reviews</title>
                    <meta id="meta-description" name="description" content="We make your buying decisions quick and easy by displaying and rating popular products in each category" />
                </MetaTags>
                {productsListed}
            </div>
        )
    }
}


module.exports = ReviewedItems