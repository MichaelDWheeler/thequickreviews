import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";
import {IndexLink, Link} from "react-router";

class Footer extends React.Component{
    render(){
        return(
            <div>
                <div className="container ptb40">
                    <Row>
                        <Col xs={12}>
                            <Link to="attribution">Attribution</Link>
                        </Col>
                    </Row>
                </div>
                <div className="container-fluid">
                    <Row>
                        <Col xs={12} className="indigo-background footer">
                            <div className="text-center p-tb-40">We are a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for us to earn fees by linking to Amazon.com and affiliated sites</div>
                        </Col>
                    </Row>
                </div>
            </div>

        )
    }
}

module.exports = Footer;
