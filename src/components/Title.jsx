import React from "react";
import { connect } from "react-redux";
import ReactDOM from "react-dom";
import { ReactBootstrap, Col, Row, Image } from "react-bootstrap";
import { showTitle, hideTitle } from "../actions/titleActions.jsx";

@connect((store) => {
    return {
        showTitleInPicture: store.showTitleInPicture.showTitle,
    }
})

class Title extends React.Component {
    constructor(props) {
        super(props);
        this.setTitleInImage = this.setTitleInImage.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(hideTitle());
    }

    componentDidMount() {
        addEventListener('resize', () => {
            this.setTitleInImage();
        });
    }

    setTitleInImage() {
        this.props.dispatch(showTitle());
    }

    render() {
        let title, year, text, url;
        // removes flashing of text before image load on mobile//
        url = 'url(' + this.props.image + ')';
        if (this.props.showTitleInPicture) {
            title = this.props.title;
            year = this.props.year;
            text = `for`;
        } else {
            title = null;
            year = null;
            text = null;
        }

        return (
            <div className="container-fluid">
                <Row>
                    <div className="parallax" style={{ backgroundImage: url }} alt={this.props.alt}>
                        <Image className="no-display" src={this.props.image} onLoad={this.setTitleInImage} />
                        <Col xs={12}>
                            <h1 className="text-center page-title">{title} {text} {year}</h1>
                        </Col>
                    </div>
                </Row>
            </div>
        )
    }
}

module.exports = Title;
