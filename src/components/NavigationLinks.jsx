import React from 'react';

import { IndexLink, Link } from "react-router";
import { backgroundAction } from "../actions/BackgroundAction.jsx";
import { toggleNavigation } from "../actions/DropDownActions.jsx";
import { resetInitialScroll } from "../actions/FixedHeaderAction.jsx";
import { getLocation } from "../actions/titleActions.jsx";
import { hideTitle } from "../actions/titleActions.jsx";
import { ReactBootstrap, Col, Row } from "react-bootstrap";
import { connect } from "react-redux";


import NavigationLinks from "./NavigationLinks.jsx";

@connect((store) => {
    return {
        showDropDownNavigation: store.dropDown.showDropDownNavigation,
        toggleBackgroundFilter: store.backgroundAction.toggleBackgroundFilter,
        showTitleInPicture: store.showTitleInPicture.showTitle,
        currentLocation: store.showTitleInPicture.currentLocation,
    }
})

class NavigationLink extends React.Component {
    constructor(props) {
        super(props);
        this.hideTitle = this.hideTitle.bind(this);
        this.toggleDropDown = this.toggleDropDown.bind(this);
    }

    componentDidMount() {
        let location = document.location.href;
        this.props.dispatch(getLocation(location));
    }

    toggleDropDown(e) {
        if (this.props.currentLocation != e.target.href) {
            this.props.dispatch(hideTitle());
        }
        this.props.dispatch(toggleNavigation());
        this.props.dispatch(resetInitialScroll());
        this.props.dispatch(backgroundAction());
    }

    hideTitle(e) {
        let url = document.location.pathname;
        let urlPath = url.split('/');
        let target = e.target.href;
        let targetPath = target.split('/');
        if (urlPath[1] != targetPath[3]) {
            this.props.dispatch(hideTitle());
        }

    }
    render() {
        const fashionItems = [
            {
                "link": "eveningDresses",
                "name": "Evening Dresses",
                "id": "3",
            },
            {
                "link": "promDresses",
                "name": "Prom Dresses",
                "id": "4",
            },
        ];
        const glutenFree = [
            {
                "link": "glutenFreeBread",
                "name": "Gluten Free Breads",
                "id": "5",
            },
            {
                "link": "glutenFreeBreadMix",
                "name": "Gluten Free Bread Mixes",
                "id": "6",
            },
            {
                "link": "glutenFreeSnacks",
                "name": "Gluten Free Snacks",
                "id": "7",
            },
            {
                "link": "glutenFreeProteinBars",
                "name": "Gluten Free Protein Bars",
                "id": "8",
            },
            {
                "link": "glutenFreePasta",
                "name": "Gluten Free Pastas",
                "id": "9",
            }
        ];
        const kitchenItems = [
            {
                "link": "electricPressureCookers",
                "name": `Electric Pressure Cookers`,
                "id": "1",
            },
            {
                "link": "coffeeMakers",
                "name": "Coffee Makers",
                "id": "2",
            },
        ];

        const personalItems = [
            {
                "link": "naturalDeodorant",
                "name": `Natural Deodorants`,
                "id": "1",
            },
            {
                "link": "best-safety-razor",
                "name": `Safety Razors`,
                "id": "3",
            },
        ];

        const dogItems = [
            {
                "link": "best-flea-treatment-for-dogs",
                "name": `Flea Treatment For Dogs`,
                "id": "1",
            },
            {
                "link": "best-dog-crate",
                "name": `Dog Crates`,
                "id": "2",
            },
        ];

        const catItems = [
            {
                "link": "best-cat-toys",
                "name": `Cat Toys`,
                "id": "1",
			},
			{
				"link": "best-cat-harness",
				"name": `Cat Harnesses`,
				"id": "2",
			},
        ];

        const sortAlphabetically = function (a, b) {
            var textA = a.link.toUpperCase();
            var textB = b.link.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        };

        const fashionList = fashionItems.sort(sortAlphabetically).map(category => {
            return (
                <Link key={category.id} onClick={this.hideTitle} className="nav-links" to={category.link}>{category.name}</Link>
            )
        });

        const glutenFreeFood = glutenFree.sort(sortAlphabetically).map(category => {
            return (
                <Link key={category.id} onClick={this.hideTitle} className="nav-links" to={category.link}>{category.name}</Link>
            )
        });

        const personalList = personalItems.sort(sortAlphabetically).map(i => {
            return (
                <Link key={i.id} onClick={this.hideTitle} className="nav-links" to={i.link}>{i.name}</Link>
            )
        });

        const kitchenList = kitchenItems.sort(sortAlphabetically).map(category => {
            return (
                <Link key={category.id} onClick={this.hideTitle} className="nav-links" to={category.link}>{category.name}</Link>
            )
        });

        const dogList = dogItems.sort(sortAlphabetically).map(category => {
            return (
                <Link key={category.id} onClick={this.hideTitle} className="nav-links" to={category.link}>{category.name}</Link>
            )
        });

        const catList = catItems.sort(sortAlphabetically).map(category => {
            return (
                <Link key={category.id} onClick={this.hideTitle} className="nav-links" to={category.link}>{category.name}</Link>
            )
        });

        return (
            <div>
                Cats({catList.length}):
                <div onClick={this.toggleDropDown} className="page-navigation">
                    {catList}
                </div>

                Dogs({dogList.length}):
                <div onClick={this.toggleDropDown} className="page-navigation">
                    {dogList}
                </div>

                Fashion({fashionList.length}):
                <div onClick={this.toggleDropDown} className="page-navigation">
                    {fashionList}
                </div>
                Gluten Free Foods({glutenFreeFood.length}):
                <div onClick={this.toggleDropDown} className="page-navigation">
                    {glutenFreeFood}
                </div>
                Kitchen({kitchenList.length}):
                <div onClick={this.toggleDropDown} className="page-navigation">
                    {kitchenList}
                </div>
                Personal({personalList.length})
                 <div onClick={this.toggleDropDown} className="page-navigation">
                    {personalList}
                </div>
            </div>

        )
    }
}

module.exports = NavigationLink;
