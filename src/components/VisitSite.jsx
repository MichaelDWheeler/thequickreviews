import React from 'react';
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class VisitSite extends React.Component{
    render(){
        return(
            <div className="visit-site-btn site-shutter-out-horizontal">
                <a className="white-link" href={this.props.href} target="_blank">VISIT SITE</a>
            </div>
        )
    }
}

module.exports = VisitSite;
