import React from "react";
import MetaTags from 'react-meta-tags';
import { ReactBootstrap, ResponsiveEmbed } from "react-bootstrap";


import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import Description from "../../components/Description.jsx";
import Disqus from "../../components/Disqus.jsx";
import ExtraContent from "../../components/ExtraContent.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import JumpTo from "../../components/JumpTo.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import Placement from "../../components/Placement.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import Video from "../../components/Video.jsx";

let url = "best-dog-crates",
    buttonText = "See Additional Sizes",
    title = "Best Dog Crate",
    image = "../../js/img/dogs.jpg",
    mobileImage = "../js/img/dogs.jpg",
    parallaxAlt = "Crate trained dogs running outside in the grass playing together",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>The Types Of Dog Crates We Compared</h3>
            <p>When searching for the best dog crate, we compared different types of dog crates and then selected the one we felt was the best dog crate for that particular type. The basic types of dog crates that we considered are the heavy duty dog crates, wire dog crates, plastic dog crates, fashionable dog crates, and soft sided dog crates. That means the dog crates ranged from extremely heavy duty and not as travel friendly as others, to dog crates that were extremely light weight and portable. While each crate we chose as our top pick represents what we consider to be the best dog crate based on the type of crate, we list them in order of what we also consider to be the most durable. For instance, the heavy duty crate could contain the most powerful and aggressive dogs, the wire crates could contain all but the most powerful, the plastic crate could contain the majority of all dogs, the fashionable crate can contain dogs that are more docile or did not have the strength to escape, and the soft sided crate was sufficient for dogs that were content being in the crate. </p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What Do We Feel Makes The Best Dog Crates?</h3>
            <p>When it comes to dog crates, we feel the best dog crates are the ones that offer the most adequate form of containment and protection for the type of dog that will be using it. This is the reason we separated our top picks into "Our Picks" for each type of dog crate (heavy duty, wire, plastic, fashion, and soft sided) instead of 1st through 5th place. We did not feel that one type of dog crate was the best universally because dogs have such different characteristics. The best heavy duty crate would be complete overkill for a small and docile dog, while the opposite would be true for a soft sided crate containing a large, powerful dog intent on escaping. Depending on the type of dog, a crate that allows easy travel might be considered better than one that cannot be taken on trips. First and foremost, the crate has to be sufficient at keeping the dog contained. The crate may allow easy transport but if the dog can easily push open the zipper it becomes practically worthless as it is not able to effectively do what it is created for.</p>
        </div>
    ],
    item1Title = "ProSelect Empire Dog Cage",
    item1Description = `To us, and quite possibly to many of you, this is the crate of crates. The makers of the ProSelect Empire Dog Cage made a crate that is able to withstand all the aggression and all the power of the largest dog that you can fit in it. This is perhaps one of the strongest and toughest built dog crates on the market. It is without a doubt the strongest dog crate we have considered for our top picks. This is a dog crate that can't be chewed through, rammed through, scratched through, kicked through, or dug out of. The only way a dog is getting out of here once it is closed shut is if someone lets it out. This crate may be complete overkill for your dog, but think of how cool it would look having this virtually indestructible heavy duty dog crate in your house. We are not dogs and we are salivating looking at it, wanting it for all the wrong reasons. Is it too large for any of our dogs? Yes. Yes it is. Is it more crate than necessary for our docile girls? Absolutely. Do we still want it? Of course we do. How will we justify it? We might buy a bigger dog... In all seriousness, if you have a large, powerfully aggressive dog that can escape other dog crates this may be the one you are looking for. These commercial grade crates feature 20-gauge steel reinforced with 0.5" steel tubing, dual stout locking door latches, and heavy duty welding at stress points for unmatched durability.  The hammertone finish is stylish and rust resistant. This crate comes with a steel grate, steel sliding tray, and four locking caster wheels for mobility that can also be removed if needed. The ProSelect Empire Dog Cage is for dogs that have destroyed lesser crates, for dogs that you worry will get out and harm themselves or damage property in your absence, or for dog owners who just want a really cool looking and functioning crate.`,
    href1 = "http://amzn.to/2nLVFSs",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000Y905XE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B000Y905XE",
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000Y905XE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000Y905XE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `One of the strongest and most durable dog crates on the market`,
        },
        {
            "id": "2",
            "positiveFeature": `A true heavy duty crate able to contain the most powerful and aggressive dogs intent on becoming escape artists`,
        },
        {
            "id": "3",
            "positiveFeature": `Includes removable locking caster wheels to aid in mobility`,
        },
        {
            "id": "4",
            "positiveFeature": `Built for powerful dogs that may be able to bite, claw, kick, chew, dig and ram their way out of other crates`,
        },
        {
            "id": "5",
            "positiveFeature": `Stylish hammertone finish is rust resistant`,
        },
        {
            "id": "6",
            "positiveFeature": `Commercial grade`,
        }

    ],

    item2Title = "MidWest iCrate Folding Metal Dog Crate",
    item2Description = "The Midwest iCrate is an extremely popular dog crate that is both strong and sturdy, yet portable due to the crate being able to fold into a much slimmer object that becomes much more transportable. This crate was designed to create a safe and comfortable space for your dog when you are away from it. It is a metal dog crate with a durable satin black Electro-Coat finish that has two entry ways which provides better options for placement. Setting up the iCrate does not require any tools at all, and can be set up in seconds. The makers of the iCrate knew that many people would not want to have to purchase multiple crates as their puppy grows into adulthood, and have included a divider that allows you to resize the interior of the crate as needed. For added safety, the corners of the iCrate are rounded.",
    href2 = "http://amzn.to/2nuaAPx",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000QFWCLY&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B000QFWCLY",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000QFWCLY&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000QFWCLY&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Crate is suitable for all but the strongest and most aggressive escape artists`,
        },
        {
            "id": "2",
            "positiveFeature": `Durable construction`,
        },
        {
            "id": "3",
            "positiveFeature": `Slide out tray can be easily cleaned`,
        },
        {
            "id": "4",
            "positiveFeature": `Open design is easy to hose down in case of accidents`,
        },
        {
            "id": "5",
            "positiveFeature": `Fold up in seconds without any tools and can be stored in fairly thin places such as under a bed`,
        },
        {
            "id": "6",
            "positiveFeature": `Sets up in seconds without any tools`,
        },
        {
            "id": "7",
            "positiveFeature": `Dual entry allows more options for placement`,
        },
    ],

    item3Title = "Plastic Kennels Airline Approved Wire Door Travel Dog Crate",
    item3Description = "As the title states, this dog crate is airline approved (for certain airlines) and was made with air travel in mind. The manufacturer does recommend contacting the airline of your choice first however to determine if this crate satisfies all their requirements as each airline is different and the rules and regulations are bound to change. This crate is constructed of sturdy plastic and metal wire doors and sides. Dogs are known for chewing through plastic doors or side air vents out of boredom or to escape and the metal bars prevent them from doing this with this dog crate. The dog crate has tie down holes so that you can use bungee cords that may be required by the airlines.  Crates larger than medium also include wheels for added mobility. ",
    href3 = "http://amzn.to/2nuitEs",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01C7JGIYI&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B01C7JGIYI",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01C7JGIYI&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01C7JGIYI&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Made with air travel in mind`,
        },
        {
            "id": "2",
            "positiveFeature": `Sturdy and durable plastic construction`,
        },
        {
            "id": "3",
            "positiveFeature": `Door and side vents are made with metal wire for added strength`,
        },
        {
            "id": "4",
            "positiveFeature": `Sizes larger than medium also include roller wheels for added mobility`,
        },
        {
            "id": "5",
            "positiveFeature": `Small holes can be used for bungee cord tie down to keep crate in place on airline or in truck`,
        },
        {
            "id": "6",
            "positiveFeature": `Water dish and live animal stickers included`,
        },
        {
            "id": "7",
            "positiveFeature": `Easy to assemble and disassemble`,
        },

    ],
    item4Title = "Petmate Two Door Top Load Kennel",
    item4Description = `The Petmate Two Door Top Load Kennel is a fashionable pet carrier that has the ability to load pets from either the top or through the front door. It is made of steel and plastic construction and is highly ventilated for the safety and visibility of your pets. It features a handle which is ergonomically designed so you can easily carry your pet and it makes traveling with your pet very convenient. This is a smaller carrier and is suitable for pets that are smaller than 7" tall and 15" long. If your dog is larger than that, you would want to go with a different size of carrier. It is easy to assemble and disassemble.`,
    href4 = "http://amzn.to/2n42mSI",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B0062JFGFC",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0062JFGM0&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0062JFGM0&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0062JFGM0&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Ability to load pet from the top of the carrier as well as the front door`,
        },
        {
            "id": "2",
            "positiveFeature": `Easy to transport small dogs in it`,
        },
        {
            "id": "3",
            "positiveFeature": `Color coded carrier corresponds to the different sizes that are available`,
        },

    ],

    item5Title = "EliteField 3-Door Folding Soft Dog Crate, Indoor & Outdoor Pet Home",
    item5Description = "This is a soft sided carrier that can be set up in seconds without any required tools. When it is compacted, it resembles an over-sized briefcase which can be held by the strap or worn over the shoulder for ease of transport. While the sides of this dog crate is a material, the frame of it is strong steel tubing. This dog crate has 3 mesh entry points which can all be rolled up for an open space. For added convenience, this dog crate has pockets that can be used to carry additional accessories or items for your dog. As an added bonus, this dog crate comes with a free carrier bag as well and a fleece bed for your dog to use while using the crate.",
    href5 = "http://amzn.to/2nxwSAL",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B018OUW18Q",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B018OUW18Q&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B018OUW18Q&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B018OUW18Q&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Frame is strong steel`,
        },
        {
            "id": "2",
            "positiveFeature": `Dog crate comes with a 2 year warranty and 100% money back guarantee from the manufacturer`,
        },
        {
            "id": "3",
            "positiveFeature": `Can be set up or taken down in seconds with no tools required`,
        },
        {
            "id": "4",
            "positiveFeature": `Can be carried over the should or by strap for easy transport`,
        },
        {
            "id": "5",
            "positiveFeature": `When folded down it is small enough to keep in a closet or under the bed`,
        },

    ],
    extraContentTitle = "What Is The Best Type Of Dog Crate For Your Dog?",
    extraContentSummary = "You undoubtedly have noticed that many different styles of dog crates exist, but you may not have been aware that each type of dog crate serves a secondary purpose. The first purpose of any dog crate is that it is able to provide a safe place for your dog that will keep them confined to that area. Without supervision, many puppies and dogs can get into areas that you do not want them to, they can damage property, harm other animals or people, or even harm themselves. Providing your dog with a crate and confining them to it is a caring and humane way to keep your dog safe.  Beyond keeping your dog safely confined, the different types of crates often provide a secondary purpose. In this section we will describe what each type of dog crate exists so that you can determine what type of dog crate will be the best dog crate for your dog.",
    extraContentInfo = [
        <div>
            <section>
                <a name="heavy-duty-dog-crates"></a>
                <h3>Heavy Duty Dog Crates</h3>
                <p>As the name implies, these dog crates are <a href="http://amzn.to/2oFmYOw" target="_blank">heavy duty dog crates</a>. They are built to last, extremely tough and durable, and can contain even the most powerful and aggressive dogs that are intent on getting out. These dog crates are the best dog crates for dogs that are able to somehow get out of lesser built dog crates. These types of dog crates are usually less portable than the other types of dog crates. They may have roller or caster wheels for added mobility, but they typically do not fold down, or break down and they weigh more due to the steel or strong metal construction. There are exceptions such as this <a href="http://amzn.to/2nGEEZl" target="_blank">folding metal heavy duty crate</a> but that is not the norm. Heavy duty dog crates usually have permanent welds or are fastened using nuts and bolts. These type of crates are commercial grade and are often purchased either because they intend to get a lot of traffic and durability is extremely important, or because the dog it will contain is extremely powerful or adept at escaping other types of crates. After considering dozens of heavy duty crates, we chose the <JumpTo href="#image1" classes="inline-block" title="ProSelect Empire Dog Cage"/> as our top pick for the best heavy duty dog crate.</p>
                <a name="wire-dog-crates"></a>
                <h3>Wire Dog Crates</h3>
                <p>Wire dog crates are very popular for large and small dogs. They provide adequate and sufficient containment for most dogs.  <a href="http://amzn.to/2nUajHD" target="_blank">Metal wire dog crates</a> were designed to be both easily stored and portable. The size of the crate and the strength of the person obviously determines ease of portability, but all things considered they do fold nicely and can be carried from one location to the next. When transporting or storing they can often be folded or unfolded in seconds without the use of tools. These types of crates are constructed with metal wires and the best wire dog crates are coated to help prevent rust. Because the crate is made up of metal wires, it offers great visibility for both you and your dog and it allows the crate to be extremely well ventilated. These types of crates are not guaranteed to be sufficient for very active chewers. Though the metal wires are very difficult to chew through, an active chewer can bend the wires and damage the cage. However, for most dogs the metal wire cages are strong enough and allow your dog to be contained while also easily being aware of it's surroundings. They are extremely easy to clean as they can be hosed down and even spritzed with a bleach and water solution and then allowed to air dry. When considering the different makes and models of wire dog crates, we chose the <JumpTo href="#image2" classes="inline-block" title="MidWest iCrate Folding Metal Dog Crate"/> as our top pick for the best dog crate. These crates do expose the dog to the elements and if kept in direct sunlight in a hot environment they can become very hot as the black absorbs the sunlight and heat. The black trays that the dogs rest on can also become very hot in direct sunlight.</p>
                <a name="plastic-dog-crates"></a>
                <h3>Plastic Dog Crates</h3>
                <p>Plastic dog crates vary in construction. Some are extremely durable while others are somewhat flimsy. One of the things they share in common regardless of durability is that they are composed primarily of plastic as the name indicates. <a href="http://amzn.to/2ozfYpn" target="_blank">Plastic dog crates</a> are popular for transporting dogs while the dog is in the crate. They are used on airlines, vehicles large enough to contain them, and in the open bed of a pick up truck. The individual walls don't shake as easily as wire crates do when in the back of a pickup truck getting blown about by wind. Plastic dog crates also provide more protection from the elements when compared to wire crates. In direct sunlight, the interior of a plastic dog crate can be much cooler than the black wire crates as the color black absorbs light and heat. The lighter colored plastic dog crates will not absorb as much light and heat as a darker colored dog crate. Additionally, most of the plastic dog crates have covered roofs which helps to provide shade. When compared to wire dog crates, the plastic dog crates do a better job of keeping the surrounding areas cleaner. They do this in a couple different ways. First, by trapping the hairs that shed from the dogs. While both types of dog crates will allow hairs to escape, due to the less open design the plastic crates tend to keep more inside the crate. In regards to accidents, the plastic dog crates usually can keep the accidents inside (and all over the dog) while wire dog crates can allow the fecal matter, vomit and urine to be push or ejected through. For heavy slobbers, the plastic crates do a better job of keeping the slobber in the crate and off of your walls. Because of their design, cleaning plastic dog crates can be more difficult than cleaning a wire crate. With wire crates the pan can be removed and the frame can be hosed. With a plastic crate you will need to either separate the two halves, reach inside or crawl inside of it (for larger crates) or turn it on it's side while spraying the inside so the mess can leak through the side windows. Dogs that are chewers have been known to chew unprotected plastic areas such as ventilation areas which damage the crate as well as the external ridges of the crates when out of the crate. Most large plastic dog crates have an entry way the uses two bars that slide into holes at the top and bottom of the crate. Powerful dogs have been known to ram or kick the entry way with such force that it bends the bars at an angle just enough to allow the dog to exit the crate. After considering many different plastic dog crates, we selected <JumpTo href="#imagE3" classes="inline-block" title="Plastic Kennels Airline Approved Wire Door Travel Dog Crate"/> as our top pick for the best dog crate in the plastic dog crate category.</p>
                <a name="fashion-dog-crates"></a>
                <h3>Fashion Dog Crates</h3>
                <p>Fashion dogs crates are dog crates that have a secondary purpose of being stylish. When a dog crate is put in the house, like it or not it becomes part of the furniture. If carried, it can become an accessory if you wanted it to supplement your outfit. <a href="http://amzn.to/2nUoCf0" target="_blank" >Fashion crates</a> have colors, shapes, and even styles to improve what many would consider a boring appearance of the standard dog crate. These types of dog crates can be any material and the durability can wildy vary. When selecting a fashion dog crate it is important to keep in mind the strength, energy level, and drive of your dog as the most attractive crate may or may not be adequate to contain your dog. When we selected the <JumpTo href="#image4" title="Petmate Two Door Top Load Kennel" classes="inline-block"/> as our top pick for the best dog crate in the fashion category, we did so because we liked the form with the function.</p>
                <a name="soft-sided-dog-crates"></a>
                <h3>Soft Sided Dog Crates</h3>
                <p>Soft sided dog crates are great for portability and can be used for docile dogs that are completely content being in a dog crate. They are lightweight, come ready to use or can be folded or unfolded usually in seconds, and are highly transportable and storable. They offer protection from most of the elements and are often well ventilated. Due to the pliability and material they are popular for traveling. These type of dog crates are not suited for aggressive dogs, or dogs that want to get out for any reason. They can be chewed through, they can be damaged by scratching, and the zippers that shut the entry ways can be split from a dog charging or kicking it with sufficient force. These dog crates are not the best choice for dogs that are not crate trained, as they can be more difficult to clean than the others. The material can soak up odors as well as liquids. We selected the <JumpTo href="#image5" title="EliteField 3-Door Folding Soft Dog Crate, Indoor & Outdoor Pet Home" classes="inline-block" /> as the best dog crate in the <a href="http://amzn.to/2ozuM7b" target="_blank">soft-sided crate</a> category.</p>
            </section>
        </div>
    ],
    subject = "Dog Crate",
    moreInfo = [
        <div>
            <section>
                <a name="why-use-a-dog-crate"></a>
                <h3>Why Use A Dog Crate?</h3>
                <p>If you have never used a dog crate before, or don't know the purpose of crate training a dog you may mistakenly believe that it is a cruel practice. It is a common misconception that securing a dog in even the best dog crate is the same as locking a person in jail, but this is far from the truth. Placing a dog in a crate would be far more similar to a person being secured in their home versus a jail.</p>
                <p>Though many of us consider our dogs another member of our family, we have obvious differences. One of those differences is that dogs have natural instincts that control their behaviors. Have you ever seen a dog walk in circles prior to laying down? It is believed that is an instinct<sup><JumpTo href="#ref-1" classes="inline-block" title="[1]" /></sup> in which the dog is trampling down grass or weeds in order to make a comfortable bed and also alerts other dogs that the territory has been taken. Dogs also have an instinct that causes them to be den animals. This means they seek out confined spaces in order to stay protected when sleeping and raising a family. This is where the dog crate comes in. It provides a safe and comfortable den for your dog. Once your dog becomes accustomed to the dog crate, your dog will often seek the dog crate (it's new den) on it's own when it wants to sleep. The best dog crate for your dog is one that allows it to satisfy it's natural instinct while keeping it safe when you are away. When done properly, the dog crate that is best for your dog will give your dog a sense of security and comfort because of it's natural instincts. Because of those natural instincts your dog has, the best crate for your dog will also make house breaking it much easier.</p>
                <div className="horizontal-separator"></div>
            </section>
            <section>
                <a name="dog-crate-sizes"></a>
                <h3>Dog Crate Sizes</h3>
                <p>Dog crates come in a variety of sizes, but it is important that the crate you use is properly sized for your dog otherwise crate training may not be effective at all. If the crate is too large and the dog needs to go potty, it will move to a section further away from where it sleeps and do it's business there. A properly sized crate will encourage the dog to hold it, until you let it out and direct it to the proper place. The best dog crate for your puppy or dog is one that gives it just enough room to stand up and turn around. The space the dog takes up should seem like a space that cannot be further divided into areas. For example, the crate should not be so large where the dog can rest in different corners or sections where it appears the sections are defined. If you are crate training a growing dog or puppy, you do not need to purchase multiple crates in various sizes. Instead you can use objects to block off the unneeded extra space so that it confines your dog into an area that will allow them to turn around and stand up in. The best dog crates for large dogs that are still puppies are <JumpTo href="#image2" classes="inline-block" title="dog crates" /> that include a divider allowing the size of the crate to become adjustable. </p>
                <div className="horizontal-separator"></div>
            </section>
            <section>
                <a name="crate-training-a-puppy"></a>
                <h3>Crate Training A Puppy</h3>
                <p>One of the biggest reasons that people use a dog crate is because it is an effective tool when house breaking a puppy or adult dog. Training a dog to not use the bathroom in the house by use of a dog crate is known as crate training. Crate training is most effective when done as a puppy but can be done at any point in the dog's life. Using a dog crate for housebreaking combines the dog's natural instincts and your leadership to effectively teach the dog the proper areas to use the bathroom. Instinctively, the dog will not want to soil the area they sleep in. By using a dog crate you allow this natural instinct to help you determine when the dog should use the bathroom and over time where to use the bathroom becomes a habit for the dog.</p>
                <p>It is also very important that you are mindful of your dog and keep it on schedule so that it develops a pattern for using the bathroom. Dogs very much like to follow schedules. Even though they do not understand our clocks, they have their own internal and highly accurate clock. Dogs that are fed on schedule can let you know, usually down to the last few minutes, when it is time for another feeding. Dogs that are taken outside to use the bathroom on schedule will often let you know when it is time to go for that walk. As dogs have such an accurate internal clock, keeping a strict schedule for potty breaks can accelerate the time needed to house train your puppy. If you are not mindful and you allow your dog to constantly relieve itself on itself in the dog crate, eventually it will learn to accept it. At that point using a dog crate to house break your dog will become very difficult as it no longer attempts to be use the bathroom away from itself because you have conditioned the dog into believing that behavior is acceptable.</p>
                <p>When crate training a dog, it is important to keep a few things in mind. These are:</p>
                <ul>
                    <li>Do not forget that the crate is for your dog's protection and comfort. Initially you may not believe this because your dog may not seem to like the crate. If you are new to crate training you may feel you are punishing your dog. You are not. You are teaching your dog that the dog crate is it's very own place that will keep it safe and comfortable. A dog that is left to run around while you are gone can bite into electrical cords, jump against windows and break them, jump onto counters, and all other kinds of dangerous actions. A crate can very well save your dog's life, not to mention your furniture and belongings. </li>
                    <li>Do not make the dog afraid of the crate. Initially your dog may not want to go into the dog crate. This is common. Do not force your dog into the dog crate. If you have a small puppy it is easy enough to gently place them in the crate and shut the door, but a larger puppy or dog may need to be coaxed into it. To do this you can reward the dog by giving it treats to retrieve from the crate. Sometimes treats will be enough for dogs that are highly food motivated. Other times it won't be. In those instances, at feeding time you can place the dog's food into the dog crate. Once the dog gets hungry enough, it will go into the crate to eat. After the dog has become accustomed to eating it's food in the crate and will go in there without hesitation to eat, you can stop using the food and begin using treats. While you are making the dog feel comfortable with being in the crate, it is also important to teach them a word to associate with going in the crate. You can say anything as long as you say it consistently and it is a short word such as "crate", "bed", "rest" that you can assign to that object and only that object. Eventually you will be able to say only the word and your dog will run itself inside of the dog crate if you have been patient and rewarded the dog for doing so.</li>
                    <li>It is much better to teach your dog to be motivated by rewards instead of fear. Remember this when training your dog always, but it especially rings true for those who are new to crate training their dog. House breaking and crate training a dog can be frustrating to someone who is not used to it. Often, frustrated people let out their frustrations. This can scare your dog. As someone who is new to training, you might see that scaring the dog works and decide that is the best way to get the dog to respond how you want. However, realize that the dog is not afraid of the consequences of not listening to you. The dog is afraid of you. It has taught itself that running into the crate will keep it safe from you. It has rewarded itself by figuring out how to escape your wrath when it sees you acting a certain way. You don't want this. Instead you want a dog that is eager to please you because it respects you. In the beginning you may have to lay the rewards on thick, but over time a "Good boy" or "Good girl" and the occasional treat will be enough for your dog.</li>
                    <li>The crate is not a place for your dog to live it's life. It is a place to house break your dog, and to keep it safe when you are unable to watch it. If you leave the dog in the dog crate for too long, it will get depressed and anxious. Dogs are social and active creatures. They need interaction and they need exercise. They get neither when confined to a crate for extended periods of time. If you are unable to give your dog the time and interaction it needs and you plan on leaving it in a dog crate please consider finding a better solution. A better solution can be changing your schedule, hiring a dog sitter, or placing the dog in an environment where it can get the stimulation it needs such as a doggy daycare.</li>
                    <li>Just like little children, puppies are unable to show the control over their bladders that adults can. This means that they should be not be kept in a crate for more than 3 to 4 hours at a time. If they are kept in a dog crate for longer periods they will be forced to use the bathroom which will teach them that it is okay to use the bathroom in their immediate surroundings. Adult dogs will have much more control over their bodily functions but may not know they are supposed to. This is especially true for adult dogs that have spent a good portion of their lives in a kennel or crate. Let them out often and direct them to the area that you want to be where they relieve themselves to teach them. Senior dogs will also suffer the same issues that adult humans do in regards to their bladder control. If you allow it to stay in the crate for too long it may leak on itself, eventually getting used to it and conditioning itself that is acceptable.</li>
                    <li>Reward your dog when it goes potty in the area you have directed it to. Giving your dog rewards will go a long ways. Rewarding your dog will work faster than punishing your dog, and your dog will respect you more. Some people do not like feeling that they need to reward their dogs because they consider themselves as the "boss" or "pack leader" and that rewarding the dog is giving in to it. If you are this type of person, I invite you to look at it in another way. You are using your superior intelligence to figure out what the dog responds the quickest to and then using that knowledge to get what you want. A dog looks to it's pack leader for reassurance<sup><JumpTo href="#ref-2" title="[2]" classes="inline-block" /></sup>. You offer the reassurance when you reward the dog. You are not giving in to the dog, you are giving the dog your approval which it is desperate for.</li>
                    <li>When your dog has done something that upsets you, do not use the crate as punishment. This goes back to the first bullet point. Using the dog crate as punishment will cause a negative association with the crate. This will make the dog avoid using it. Ideally you want to be able to let them dog roam freely and use it voluntarily. If you make your dog afraid of the dog crate, or associate negativity with it, this will not happen.</li>
                </ul>
                <Video source="https://www.youtube.com/embed/FG-gyJP8op0" altText="potty and crate training a dog video" tagName="potty-training-&-crate-training-video" videoTitle="Potty And Crate Training A Dog"/>
            </section>
            <section>
                <div className="horizontal-separator"></div>
                <a name="references"></a>
                <h3>References</h3>
                <ol>
                    <li>Remy Melina (March 28, 2011) <a name="ref-1" href="http://www.livescience.com/33160-why-do-dogs-walk-in-circles-before-lying-down.html">Why Do Dogs Walk in Circles Before Lying Down?</a></li>
                    <li>Cesar's Way <a name="ref-2" href="https://www.cesarsway.com/dog-psychology/five-techniques/rules-boundaries-limitations/establish-rules-boundaries-limitations">Pack Leadership Technique 3: Establish rules, boundaries and limitations.</a></li>
                </ol>
            </section>
        </div>
    ],
    links = [
        {
            "id": 1,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 2,
            "title": "The Types Of Dog Crates We Compared",
            "tag": "#types",
        },
        {
            "id": 3,
            "title": "What Do We Feel Makes The Best Dog Crates",
            "tag": "#features",
        },
        {
            "id": 9,
            "title": "Heavy Duty Dog Crates",
            "tag": "#heavy-duty-dog-crates",
            "extra": "What Is The Best Type Of Dog Crate For Your Dog?",
        },
        {
            "id": 10,
            "title": "Wire Dog Crates",
            "tag": "#wire-dog-crates",
        },
        {
            "id": 11,
            "title": "Plastic Dog Crates",
            "tag": "#plastic-dog-crates",
        },
        {
            "id": 12,
            "title": "Fashion Dog Crates",
            "tag": "#fashion-dog-crates",
        },
        {
            "id": 13,
            "title": "Soft-sided Dog Crates",
            "tag": "#soft-sided-dog-crates",
        },
        {
            "id": 4,
            "title": "Why Use A Dog Crate?",
            "tag": "#why-use-a-dog-crate",
            "extra": "Additional Information",
        },
        {
            "id": 5,
            "title": "Dog Crate Sizes",
            "tag": "#dog-crate-sizes",
        },
        {
            "id": 7,
            "title": "Crate Training A Puppy",
            "tag": "#crate-training-a-puppy",
        },
        {
            "id": 8,
            "title": "Potty Training & Crate Training Video",
            "tag": "#potty-training-&-crate-training-video",
        },
        {
            "id": 6,
            "title": "References",
            "tag": "#references",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ];

class PageDisplayed extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        ga('create', 'UA-92142805-1', 'auto');
        ga('send', 'pageview');
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }

    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links} />

                <PageDescription date={this.date()} pageDescription={mainDescription[0]} />

                <Description buttonText={buttonText} itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="Our Pick" extraTitle="Heavy Duty Crates" name="image1" />
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="Our Pick" extraTitle="Wire Crates" name="image2" />
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="Our Pick" extraTitle="Plastic Crates" name="image3" />
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="Our Pick" extraTitle="Fashion Crates" name="image4" />
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />

                <Description buttonText={buttonText} itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="Our Pick" extraTitle="Soft Sided Crates" name="image5" />
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />
                <ExtraContent extraContentTitle={extraContentTitle} extraContentSummary={extraContentSummary} extraContentInfo={extraContentInfo[0]} />
                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />
            </div>
        )
    }
}

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-92142805-1', 'auto');
ga('send', 'pageview');

module.exports = PageDisplayed
