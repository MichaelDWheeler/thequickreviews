import React from "react";
import { ReactBootstrap, ResponsiveEmbed } from "react-bootstrap";

class Video extends React.Component {
    render() {
        let horizontalSeparator;
        if (this.props.liEmbed) {
            horizontalSeparator = null;
        } else {
            horizontalSeparator =
                <div>
                    <div className="horizontal-separator"></div>
                    <div className="pt-55"></div>
                </div>
        }
        return (
            <div>
                {horizontalSeparator}
                <a name={this.props.tagName}></a>
                <h3>Video: {this.props.videoTitle}</h3>
                <ResponsiveEmbed a16by9>
                    <iframe width="560" height="315" src={this.props.source} frameBorder="0" allowFullScreen alt={this.props.altText}></iframe>
                </ResponsiveEmbed>
            </div>
        )
    }
}

module.exports = Video