# Bootstrap without jQuery

> A lightweight script (< 4k) to replace both
[Twitter Bootstrap](http://getbootstrap.com/) JS script and its jQuery
dependency.

## Demo

https://bootstrap-without-jquery.herokuapp.com/

## Installation

### NPM

    npm install bootstrap-without-jquery --save

### Bower

    bower install bootstrap-without-jquery-2 --save

## Coverage

- Dropdown menus (including "hamburger" menu icon)
- Dismiss alert messages

## Dependencies

- **None**
