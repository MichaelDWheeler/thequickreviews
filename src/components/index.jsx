var React = require('react');

class Index extends React.Component {
    render() {
        return (
            <html lang="en">

                <head>
                    <meta charset="utf-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="robots" content="index,follow"/>
                    <meta name="author" content="Adrienne Wheeler" />
                    <link href="../stylesheets/styles.css" rel="stylesheet" />
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css" />
                    <link href="../vendors/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
                </head>

                <body>
                    <div id="app"></div>
                    <script src="../js/client.min.js"></script>
                    <script id="dsq-count-scr" src="//thequickreviews.disqus.com/count.js" async></script>
                </body>

            </html>
        )
    }
}

module.exports = Index;
