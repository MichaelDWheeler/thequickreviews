import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class Attribution extends React.Component{
    render(){
        return(
            <div className="container">
                <Row className="p-20">
                    <Col xs={12}>
                        <p>We would like to thank the following people for allowing us to use their icons:</p>
                        <ul className="pt-20">
                            <li>
                                <a href="https://www.iconfinder.com/icons/771362/facebook_logo_media_network_share_social_square_icon#size=24">Creative Commons Social Media & Networks - Color Shapes </a> by <a href="https://www.iconfinder.com/MaxIcon">Martial Red</a> licensed under <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>
                            </li>
                            <li>
                                <a href="https://www.iconfinder.com/icons/682222/google_logo_media_new_plus_social_square_icon#size=24">Creative Commons Social Media & Networks - Color Shapes </a> by <a href="https://www.iconfinder.com/MaxIcon">Martial Red</a> licensed under <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>
                            </li>
                            <li>
                                <a href="https://www.iconfinder.com/icons/410515/social_twitter_icon#size=128">Creative Commons Social Media & Networks - Color Shapes </a> by <a href="https://www.iconfinder.com/leetvin">Litvin</a> licensed under Free for commercial use
                            </li>
                            <li>
                                <a href="https://www.iconfinder.com/icons/410519/linkedin_social_icon#size=128">Creative Commons Social Media & Networks - Color Shapes </a> by <a href="https://www.iconfinder.com/leetvin">Litvin</a> licensed under Free for commercial use
                            </li>
                        </ul>
                    </Col>
                </Row>
                <Row className="p-20">
                    <Col xs={12}>
                        <img className="thank-you-image" responsive src="../js/img/thankyou.jpg" />
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = Attribution;
