import React from "react";
import { ReactBootstrap, Col, Row, Image} from "react-bootstrap";

class ComparisonImages extends React.Component{
	constructor(props) {
		super(props);
	}

	displayError(e) {
		e.target.src = "../js/img/image-not-available.png";
	}

    render(){
        const style = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        };

        let item6, item7, item8, item9, item10;

        if(this.props.item6Image != undefined){
            item6 =
            <Col xs={2} xsOffset={1} style={style} className="images-container">
				<a href="#image6"><Image style={style} className="preview-image center-image" src={this.props.item6Image} onError={this.displayError} alt={this.props.alt6tag}/></a>
            </Col>;
        } else {
            item6 = null;
        }

        if(this.props.item7Image != undefined){
            item7 =
            <Col xs={2} style={style} className="images-container">
				<a href="#image7"><Image style={style} className="preview-image center-image" src={this.props.item7Image} onError={this.displayError} alt={this.props.alt7tag}/></a>
            </Col>;
        } else {
            item7 = null;
        }

        if(this.props.item8Image != undefined){
            item8 =
            <Col xs={2} style={style} className="images-container">
				<a href="#image8"><Image style={style} className="preview-image center-image" src={this.props.item8Image} onError={this.displayError} alt={this.props.alt8tag}/></a>
            </Col>;
        } else {
            item8 = null;
        }

        if(this.props.item9Image != undefined){
            item9 =
            <Col xs={2} style={style} className="images-container">
				<a href="#image9"><Image style={style} className="preview-image center-image" src={this.props.item9Image} onError={this.displayError} alt={this.props.alt9tag}/></a>
            </Col>;
        } else {
            item9 = null;
        }

        if(this.props.item10Image != undefined){
            item10 =
            <Col xs={2} style={style} className="images-container">
				<a href="#image10"><Image style={style} className="preview-image center-image" src={this.props.item10Image} onError={this.displayError} alt={this.props.alt10tag}/></a>
            </Col>;
        } else {
            item10 = null;
        }

        return(
            <div className="container">
                <Row>
                    <div className="main-image-container">
                        <a name="picks"></a>
                        <h3 className="text-center">Our Top Picks</h3>
                        <Col xs={2} xsOffset={1} style={style} className="images-container">
							<a href="#image1"><Image style={style} className="preview-image center-image" src={this.props.item1Image} onError={this.displayError} alt={this.props.alt1tag} title="See our top pick"/></a>
                        </Col>
                        <Col xs={2} style={style} className="images-container">
							<a href="#image2"><Image style={style} className="preview-image center-image" src={this.props.item2Image} onError={this.displayError} alt={this.props.alt2tag} title="See our second pick"/></a>
                        </Col>
                        <Col xs={2} style={style} className="images-container">
							<a href="#image3"><Image style={style} className="preview-image center-image" src={this.props.item3Image} onError={this.displayError} alt={this.props.alt3tag} title="See our third pick"/></a>
                        </Col>
                        <Col xs={2} style={style} className="images-container">
							<a href="#image4"><Image style={style} className="preview-image center-image" src={this.props.item4Image} onError={this.displayError} alt={this.props.alt4tag} title="See our fourth pick"/></a>
                        </Col>
                        <Col xs={2} style={style} className="images-container">
							<a href="#image5"><Image style={style} className="preview-image center-image" src={this.props.item5Image} onError={this.displayError} alt={this.props.alt5tag} title="See our final pick"/></a>
                        </Col>
                        {item6}
                        {item7}
                        {item8}
                        {item9}
                        {item10}
                        <Col xs={12}>
                            <div className="horizontal-separator"></div>
                        </Col>
                    </div>
                </Row>
            </div>
        )
    }
}

module.exports = ComparisonImages;
