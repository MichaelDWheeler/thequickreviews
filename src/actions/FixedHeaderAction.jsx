export function slideIntoView(){
    return{
            type: "SHOW_FIXED_HEADER"
    }
}

export function hideAwayFromView(){
    return{
            type: "HIDE_FIXED_HEADER"
    }

}

export function resetInitialScroll(){
    return {
        type: "RESET_INITIAL_SCROLL"
    }
}

export function linkClick(){
    return {
        type: "LINK_CLICK"
    }
}

export function notLinkClick() {
    return {
        type: "NOT_LINK_CLICK"
    }
}
