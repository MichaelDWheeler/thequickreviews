import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import JumpTo from "../../components/JumpTo.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";

let url = "best-cat-harness",
	title = "Best Cat Harness",
	buttonText = "See Additional Colors and Sizes",
	image = "../../js/img/cats.jpg",
	mobileImage = "../js/img/cats.jpg",
	parallaxAlt = "Cat laying on brick wall outside without cat harness",
    subject = "Cat Harnesses",
catagoryDescription = [
    <div>
        <a name="types"></a>
        <h3>The Types Of {subject} We Compared</h3>
        <p>

        </p>
    </div>
],
mainDescription = [
    <div>
        <a name="features"></a>
        <h3>What Do We Feel Makes The Best {subject}?</h3>
        <p>
        </p>
    </div>
],

item1Title = "Mynwood Cat Jacket/Harness Black Adult Cat",
item1Description = "The Mynwood Cat Jacket is made for cats that are at least 12 months old. Each cat harness is personally handmade by a designer in Yorkshire named Maria, who is also the proud owner of 3 Bengal cats. Maria began designing these cat harnesses because she realized that many of the mass produced cat harnesses on the market were not escape proof. It seemed that many cats could get out of them simply by reversing out of them. Maria set out to make a better harness by doing research which included getting the opinions of other cat owners as well as their feedback. What she created was a cat harness/jacket that was secure, strong, practical, fashionable, comfortable, and very satisfying for the people who purchased them. To that end, each jacket is made to suit the your cat's requirements. Each cat harness is secured around the neck of your feline family member with long strips of velcro. The jackets are also high quality reversible black cotton. They are triple sewn and are extremely durable, with a steel welded super secure D-ring with reinforced stitching for going on cat walks or keeping your cat close by. These jackets are not made to leave cats unattended outside.test",
href1 ="http://amzn.to/2tC2doG",
item1url = url + "/" + encodeURIComponent('#')+"image1",
item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00UU1EXB6&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00UU1EXB6&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00UU1EXB6&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B00UU1EXB6",
item1WhatPeopleLike = [
	{
		"id": "6",
		"positiveFeature": `Made with security in mind to help prevent cat from escaping`,
	},
    {
        "id": "1",
        "positiveFeature": `Handmade with quality materials`,
	},
	{
		"id": "2",
		"positiveFeature": `Triple sewn harnesses for extra durability`,
	},
	{
		"id": "3",
		"positiveFeature": `Easily attaches to cat with long velcro straps`,
	},
	{
		"id": "4",
		"positiveFeature": `Velcro straps allow for easy adjustment`,
	},
	{
		"id": "5",
		"positiveFeature": `Reversible design`,
	},

],

item2Title = "BPS Voyager - All Weather No Pull Step-in Mesh Harness with Padded Vest for Puppy and Cats",
item2Description = "The BPS Voyager is an all weather harness that can be used for both puppies and cats. It is designed with a breathable mesh fabric and is great for allowing your cat to let body heat to escape in the warmer days. It is also made with soft materials for the comfort of your cat. The BPS Voyager is a step in design which means that it is put on by having your cat step or slide in to the front legs of it. Instead of a one size fits all design, this one comes in 5 different sizes to give your cat the proper fit as well as 11 different colors.",
href2 ="http://amzn.to/2rtgYJy",
item2url = url + "/" + encodeURIComponent('#')+"image2",
item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00M0V7UVE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00M0V7UVE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00M0V7UVE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20" ,
pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B00M0V7UVE" ,
item2WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": `Designed with mesh fabric which is breathable and allows body heat to escape more easily`,
	},
	{
		"id": "2",
		"positiveFeature": `Front step in design`,
	},
	{
		"id": "3",
		"positiveFeature": `5 different sizes to ensure maximum comfort and fit`,
	},
	{
		"id": "4",
		"positiveFeature": `11 different colors`,
	},

],

item3Title = "Kitty Holster Cat Harness",
item3Description = "",
href3 ="http://amzn.to/2tgOSCz",
item3url = url + "/" + encodeURIComponent('#')+"image3",
item3ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007KAX8DO&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item3ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007KAX8DO&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item3ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007KAX8DO&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B007KAX8DO" ,
item3WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item4Title = "Adjustable Figure H Cat Harness",
item4Description = "",
href4 ="http://amzn.to/2tgEfjn",
item4url = url + "/" + encodeURIComponent('#')+"image4",
item4ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0002DHWWE&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item4ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0002DHWWE&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item4ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0002DHWWE&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B0002DHWWE" ,
item4WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],

item5Title = "FUNPET Soft Mesh Dog Harness No Pull Comfort Padded Vest for Small Pet Cat and Puppy",
item5Description = "",
item5url = url + "/" + encodeURIComponent('#')+"image5",
href5 ="http://amzn.to/2tgZykM",
item5ImageA ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01HGX96U8&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item5ImageAComp ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01HGX96U8&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
item5ImageANav ="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01HGX96U8&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li2&o=1&a=B01HGX96U8" ,
item5WhatPeopleLike = [
    {
        "id": "1",
        "positiveFeature": ``,
    },

],
moreInfo = [
    <div>
        <a name="store"></a>
        <h3></h3>
        <p>

        </p>
    </div>
],
links = [
    {
        "id": 1,
        "title": "Our Top Picks",
        "tag": "#picks",
    },
    {
        "id": 2,
        "title": `The Types Of ${subject} We Compared`,
        "tag": "#types",
    },
    {
        "id": 3,
        "title": `What Do We Feel Makes The Best ${subject}`,
        "tag": "#features",
    },
    {
        "id": 4,
        "title": "",
        "tag": "#store",
        "extra": "Additional Information",
    },
    {
        "id": 100,
        "title": `What Do You Think Makes The Best ${subject}?`,
        "tag": "#discussion",
        "extra": `${subject} Discussion`,
    },
];

class PageDisplayed extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount() {
        ga('create', 'UA-92142805-1', 'auto');
        ga('send', 'pageview');
    }

    date(){
        let date = new Date(),
        locale = "en-us",
        month = date.toLocaleString(locale, { month: "long" }),
        day = date.getDate(),
        year = date.getFullYear(),
        newDate =`${month} ${day}, ${year}`;
        return newDate;
    }

    year(){
        let d = new Date,
        thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

	render() {
		return (
			<div>
				<MetaTags>
					<title>{title}</title>
					<meta id="meta-description" name="description" content={this.quantity()} />
				</MetaTags>
				<FixedHeaderNav state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
				<Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
				<ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />

				<ItemIntroduction catagoryDescription={catagoryDescription[0]} />
				<PageNavigation links={links} />

				<PageDescription date={this.date()} pageDescription={mainDescription[0]} />

				<Description buttonText={buttonText} itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
				<WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
				<WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
				<WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
				<WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />

				<Description buttonText={buttonText} itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
				<WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />
				<AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
				<AdrienneWheeler />
				<Disqus url={url} id={url + "1"} subject={subject} />
			</div>
		)
	}
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-92142805-1', 'auto');
ga('send', 'pageview');

module.exports = PageDisplayed
