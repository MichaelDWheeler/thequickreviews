import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class PageDescription extends React.Component{

    render(){
        let paragraph2, paragraph3;
        if(this.props.description2){
            paragraph2 = <p className="pt-20">
                             {this.props.description2}
                         </p>
        }else{
            paragraph2 = null;
        }
        if(this.props.description3){
            paragraph3 = <p className="pt-20">
                             {this.props.description3}
                         </p>
        }else{
            paragraph3 = null;
        }
        return(
            <div className="container">
                <Row>
                    <Col xs={12}>
                        <div className={this.props.indexDescription}>
                                {this.props.pageDescription}
                                {paragraph2}
                                {paragraph3}
                        </div>
                    </Col>
                    <Col xs={12}>
                        <div className="horizontal-separator"></div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = PageDescription;
