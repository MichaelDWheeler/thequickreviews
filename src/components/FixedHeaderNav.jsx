import React from "react";
import {slideIntoView, hideAwayFromView, notLinkClick} from "../actions/FixedHeaderAction.jsx";
import {Image} from "react-bootstrap";
import {connect} from "react-redux";

@connect((store)=> {
    return {
        showFixedHeader: store.fixedHeader.showFixedHeader,
        initialScroll: store.fixedHeader.initialScroll,
        linkClick: store.fixedHeader.linkClick,
    }
})

class FixedHeaderNav extends React.Component{
    constructor(props){
        super(props);
        this.hideNavigation = this.hideNavigation.bind(this)
    }

    componentWillMount() {
        document.addEventListener('scroll', this.checkScroll.bind(this));
    }

    checkScroll(event){
        if(!this.props.linkClick){
            let scrollTop = event.target.body.scrollTop;
            let width = document.body.clientWidth;
            if(width > 991){
                if(scrollTop >= 700){
                    this.props.dispatch(slideIntoView());
                }else{
                    this.props.dispatch(hideAwayFromView());
                }
            }else{
                if(scrollTop >= 300){
                    this.props.dispatch(slideIntoView());
                }else{
                    this.props.dispatch(hideAwayFromView());
                }
            }
        }else{
            this.props.dispatch(notLinkClick());
        }

    }

    hideNavigation(){
        this.props.dispatch(hideAwayFromView());
    }

    render(){
        let showNavigation;
        let mobile = document.body.clientWidth;

        if(this.props.showFixedHeader){
            /* If more than 5 items on page exists it uses a different drop down menu for quick linking them */
            if(this.props.extended){
                showNavigation =
                <div className="showOnScroll animate-element slideDown">
                    <div className="nav-image-container">
                        <div className="number-margin">
                            <a onClick={this.hideNavigation} href="#top"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></a>
                            <a href="#image1"><div className="number-links">1</div></a>
                            <a href="#image2"><div className="number-links">2</div></a>
                            <a href="#image3"><div className="number-links">3</div></a>
                            <a href="#image4"><div className="number-links">4</div></a>
                            <a href="#image5"><div className="number-links">5</div></a>
                            <a href="#image6"><div className="number-links">6</div></a>
                            <a href="#image7"><div className="number-links">7</div></a>
                            <a href="#image8"><div className="number-links">8</div></a>
                            <a href="#image9"><div className="number-links">9</div></a>
                            <a href="#image10"><div className="number-links">10</div></a>
                        </div>
                    </div>
                </div>
            }else if(mobile <= 768){
                showNavigation =
                <div className="showOnScroll animate-element slideDown">
                    <div className="nav-image-container">
                        <div className="number-margin">
                            <a onClick={this.hideNavigation} href="#top"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></a>
                            <a href="#image1"><div className="number-links">1</div></a>
                            <a href="#image2"><div className="number-links">2</div></a>
                            <a href="#image3"><div className="number-links">3</div></a>
                            <a href="#image4"><div className="number-links">4</div></a>
                            <a href="#image5"><div className="number-links">5</div></a>
                        </div>
                    </div>
                </div>
            }else{
                showNavigation =
                <div className="showOnScroll animate-element slideDown">
                    <div className="nav-image-container">
                        <div className="center-image">
                            <a onClick={this.hideNavigation} href="#top"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></a>
                            <a href="#image1"><Image src={this.props.item1Image} className="navigation-image" alt={this.props.alt1tag}/></a>
                            <a href="#image2"><Image src={this.props.item2Image} className="navigation-image" alt={this.props.alt2tag}/></a>
                            <a href="#image3"><Image src={this.props.item3Image} className="navigation-image" alt={this.props.alt3tag}/></a>
                            <a href="#image4"><Image src={this.props.item4Image} className="navigation-image" alt={this.props.alt4tag}/></a>
                            <a href="#image5"><Image src={this.props.item5Image} className="navigation-image" alt={this.props.alt5tag}/></a>
                        </div>
                    </div>
                </div>
            }
        }
        if(!this.props.showFixedHeader && this.props.initialScroll){
            /* If more than 5 items on page exists it uses a different drop down menu for quick linking them */
            if(this.props.extended){
                showNavigation =
                <div className="showOnScroll animate-element slideUp">
                    <div className="nav-image-container">
                        <div className="number-margin">
                            <a onClick={this.hideNavigation} href="#top"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></a>
                            <div className="number-links"><a href="#image1">1</a></div>
                            <div className="number-links"><a href="#image2">2</a></div>
                            <div className="number-links"><a href="#image3">3</a></div>
                            <div className="number-links"><a href="#image4">4</a></div>
                            <div className="number-links"><a href="#image5">5</a></div>
                            <div className="number-links"><a href="#image6">6</a></div>
                            <div className="number-links"><a href="#image7">7</a></div>
                            <div className="number-links"><a href="#image8">8</a></div>
                            <div className="number-links"><a href="#image9">9</a></div>
                            <div className="number-links"><a href="#image10">10</a></div>
                        </div>
                    </div>
                </div>
            }else if(mobile <= 768){
                showNavigation =
                <div className="showOnScroll animate-element slideUp">
                    <div className="nav-image-container">
                        <div className="number-margin">
                            <a onClick={this.hideNavigation} href="#top"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></a>
                            <a href="#image1"><div className="number-links">1</div></a>
                            <a href="#image2"><div className="number-links">2</div></a>
                            <a href="#image3"><div className="number-links">3</div></a>
                            <a href="#image4"><div className="number-links">4</div></a>
                            <a href="#image5"><div className="number-links">5</div></a>
                        </div>
                    </div>
                </div>
            }else{
                showNavigation =
                <div className="showOnScroll animate-element slideUp">
                    <div className="nav-image-container">
                        <div className="center-image">
                            <a href="#top"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></a>
                            <a href="#image1"><img src={this.props.item1Image} className="navigation-image" alt={this.props.alt1tag}/></a>
                            <a href="#image2"><img src={this.props.item2Image} className="navigation-image" alt={this.props.alt2tag}/></a>
                            <a href="#image3"><img src={this.props.item3Image} className="navigation-image" alt={this.props.alt3tag}/></a>
                            <a href="#image4"><img src={this.props.item4Image} className="navigation-image" alt={this.props.alt4tag}/></a>
                            <a href="#image5"><img src={this.props.item5Image} className="navigation-image" alt={this.props.alt5tag}/></a>
                        </div>
                    </div>
                </div>
            }
        }
        if(!this.props.showFixedHeader && !this.props.initialScroll){
            showNavigation = null;
        }
        return(
            <div>
                {showNavigation}
            </div>
        )
    }
}

module.exports = FixedHeaderNav;
