import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";



let thisYear = function () {
    let d = new Date();
    let year = d.getFullYear();
    return year;
};

let url = "glutenFreeSnacks",
    title = "Best Gluten Free Snacks",
    image = "../../js/img/glutenFreeBackground.jpg",
    mobileImage = "../js/img/glutenFreeBackground.jpg",
    parallaxAlt = "Ingredients that are often included in gluten free snacks",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>The Types Of Gluten Free Snacks We Compared</h3>
            <p>
                When we decided to compare snacks, we knew we were looking at a wide range of choices. After much thought, we decided we wanted to compare snacks that gave us pleasure just because we are munching on them. These were not necessarily snacks that were supposed to hold us over until the next meal, they did not need to be a meal replacement, they did not need to offer us any specific type of nutrition. Instead, they are just snacks that we like to munch on. Maybe because we are bored. Maybe because we are hungry. Maybe because munching on them will make whatever we are currently doing better. These are just snacks that we chose and the main criteria was that they are gluten free. We might be wrong but we figured that is the type of snacks that most people wanted to look for too. That's not to say that some of our gluten free snacks can't be used in between meals to hold you over, because they can. Some are nutritious, and some can actually be used as a meal replacement. Most of them can if you eat multiple servings of them, but that's beside the point and probably not what people are wanting to do. So there you have it. These are the types of snacks we decided to compare, to present to you what we felt are the best gluten free snacks you can get to satisfy your craving.
        </p>
            <p>
                Part of our reason for picking snack foods is because we know that transitioning to a gluten free diet can be a bit scary. Many people believe they will suddenly have to completely flip their lives upside down, and for some that may be a truth. But what many people don't realize is that gluten free snacks are actually quite plentiful. There are many gluten free snack foods that have always been gluten free and no one ever took notice because it wasn't the marketing term that it now is. It is this reason alone, that we did not <em>only</em> choose snacks that normally contain gluten, that have an alternative. We also selected snacks that never contained gluten in the first place and did not need an alternative.
        </p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="lifestyle"></a>
            <h3>Is It Hard To Live A Gluten Free Life Style?</h3>
            <p>Being on a gluten free diet may seem a bit overwhelming at first. Although many of us take it for granted because we partake in it so often, eating is a huge and obviously important portion of our lives. It is a necessity. For the most part, we have done it multiple times per day since we were born and will continue to do so. Often we associate eating with a pleasurable activity such as going out to a restaurant, or we get pleasure by just enjoying the tastes and savoring the smells. We often eat depending on our moods. The reasons we eat and the ways we eat vary so much and being on a gluten free diet complicates it. Sometimes eating a normal diet can be a bit difficult. While some of us may just grab what looks good on the supermarket shelf, others may count calories and check ingredients. We haven't even discussed when a family member is on a gluten free diet. It is no longer about making yourself happy. You know what you like and dislike once you taste it, but what about a picky teenager or child? That just adds another complication to this already worrisome problem. A simpler life of pleasure prior to being gluten free suddenly seems so far away. Fear not, it doesn't have to be. In this review, we select the top ten gluten free snacks that we chose as out top picks for {thisYear()}.</p>
        </div>],
    subject = "Gluten Free Snacks",
    moreInfo = [
        <div>
            <a name="cost"></a>
            <h3>Are Gluten Free Snacks More Expensive</h3>
            <p>
                Unlike some baked products, gluten free snacks are not necessarily any more expensive for the simple reason that the snack may have never contained gluten to begin with. Gluten is a mixture of proteins that is found in wheat and other grains such as rye and barley. A snack may advertise itself as gluten free, but they may just be doing so to let it be known that those with a gluten sensitivity can eat it. It may not be the result of the company creating a special formulation directed at those people, but instead a marketing campaign to include those people. Many of the snacks you may have grown up with are already gluten free and always have been.
            </p>
            <p>
                On the other side of the coin however are the snacks that are specifically formulated to be gluten free for products that normally contain gluten. This could be gluten free donuts, gluten free bars, gluten free cupcakes, and anything that normally contains gluten. These items may be more expensive, may require special care, and as a result of being without gluten may differ from the item that contains gluten.
            </p>

            <a name="knowledge"></a>
            <h3>How Do I Know If A Snack Is Gluten Free</h3>
            <p>
                Now that we know that many snacks never contained gluten and are advertised as gluten free for a marketing campaign, is it safe to assume only snacks that look baked or breaded contain gluten? Unfortunately no. Not at all. You still need to do your due dilligence to make certain that the snack you are about to eat or give to someone else does not contain gluten. Not only that, but your research needs to be specific to that exact product. For instance, you may look at the <a href="http://amzn.to/2mcxsEj">Hershey's Special Dark Kisses</a>, see that they are advertised as gluten free and logically assume that the Hershey's Special Dark Bars are gluten free as well. If so, you would be wrong. Most people would be. For each and every product you need to look at the label or packaging. The easiest way to tell is if the product has been certified as gluten free and has the logo on the label. If not, you may need to look at all the ingredients and see if it contains anything that has gluten. Not only do you want to make sure it does not contain ingredients that have gluten, you also need to look for warnings such as "May contain traces of wheat" or "Made on shared equipment with wheat ingredients". If that is too confusing and you really want to know, your best bet would be to contact the manufacturer.
            </p>

        </div>
    ],

    links = [
        {
            "id": 3,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 1,
            "title": "The Type Of Gluten Free Snacks We Compared",
            "tag": "#types",
        },
        {
            "id": 2,
            "title": "Is It Hard To Live A Gluten Free Life Style?",
            "tag": "#lifestyle",
        },
        {
            "id": 4,
            "title": "Are Gluten Free Snacks More Expensive",
            "tag": "#cost",
            "extra": "Additional Information",
        },
        {
            "id": 5,
            "title": "How Do I Know If A Snack Is Gluten Free?",
            "tag": "#knowledge",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ],
    item1Title = "Werther's Original Caramel Coffee Hard Candies Gluten Free Snacks, 5.5-Ounce Bags ",
    item1Description = "These hard caramel and coffee flavored hard candies are a gluten free snack that is sure to put a smile on your face. It comes with 12 bags of 5.5 ounces of absolutely mouth watering hard candy. The candy has a creamy smooth caramel flavor with a bold taste of coffee that mixes wonderfully. The reason this candy taste exceptionally good is because it uses real butter and fresh cream in the recipe, which is a family original. These gluten free snacks are great when you are craving food and just need some exciting flavors to hold you over. The outer shell of this candy is semi-hard, and the center is a soft, delicate, and delicious.",
    href1 = "https://www.amazon.com/Werthers-Original-Caramel-Candies-5-5-Ounce/dp/B0018AXEWC/ref=as_li_ss_il?ie=UTF8&qid=1487959956&sr=8-5&ppw=fresh&keywords=gluten+free+snacks&refinements=p_72:2661618011&linkCode=li3&tag=bestvoted-20&linkId=4fc15a8ea7e0a3eea383aa502b7cff5b",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0018AXEWC&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B01IUAE8S6",
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0018AXEWC&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0018AXEWC&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Popular and familiar snack for most people`,
        },
        {
            "id": "2",
            "positiveFeature": `Convenient, individually wrapped candies that can be carried in purse or pocket`,
        },
        {
            "id": "3",
            "positiveFeature": `Caramel and coffee flavor is delicious`,
        },
        {
            "id": "4",
            "positiveFeature": `Great for snacking when looking for gluten free snack with exciting flavor`,
        },
    ],
    item1WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Candy tastes so good that it is hard to stop at one`,
        },
    ],

    item2Title = "Crunchmaster Multi-Grain Crackers, Sea Salt, 4.5 Ounce Gluten Free Snacks",
    item2Description = "The wafer thin multi-grain crackers are a great snack to have between meals or included with your meal. They are a gluten free wheat alternative cracker that has wholesome ingredients. These crackers are baked to a light, crisp, satisfying perfection. Crunchmaster multi-grain crackers are also a great source of fiber as they contain whole grain brown rice, stone ground corn, oat fiber, and 4 seeds blended into the recipe. They can be eaten straight out of the packaging or you can use your favorite spread to add some excitement to your gluten free snacks.",
    href2 = "https://www.amazon.com/Crunchmaster-Multi-Grain-Crackers-Salt-Ounce/dp/B004H1YIBU/ref=as_li_ss_il?ie=UTF8&qid=1487961908&sr=8-43&keywords=gluten+free+snacks&refinements=p_72:2661618011&th=1&linkCode=li3&tag=bestvoted-20&linkId=138402ce5ce9d287476430af8b3479a7",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004H1YIBU&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B004H1YIBU",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004H1YIBU&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004H1YIBU&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item2WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Healthy, gluten free snack`,
        },
        {
            "id": "2",
            "positiveFeature": `Great grain flavor and nice satisfying crunch`,
        },
        {
            "id": "3",
            "positiveFeature": `Guilt free snack is versatile and can be used for adding spreads, condiments, etc...`,
        },
        {
            "id": "4",
            "positiveFeature": `Gluten free, soy free, wheat free`,
        },
    ],
    item2WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `In comparison to non-gluten free crackers, these tend to be more expensive`,
        },
    ],

    item3Title = "Dark Chocolate Nuts & Sea Salt Bars, Gluten Free",
    item3Description = "These gluten free snack bars are incredibly popular and are currently a best seller. They are a healthy alternative to high sugar snacks and also provide 6 grams of protein. They have a sweet and saltly flavor mixed with crunchy almonds, walnuts and peanuts. Then they are drizzled in a bit of chocolate and sea salt for an experience that is certain to make your mouth water. These snack bars are made with all natural and wholesome ingredients with a healthy lifestyle in mind. Who says gluten free snacks have to be boring? Treat yourself to a wonderfully satisfying snack in between meals or after a hard workout. These bars really do curb your appetite.",
    href3 = "https://www.amazon.com/KIND-Chocolate-Gluten-Ounce-Count/dp/B007PE7ANY/ref=as_li_ss_il?ie=UTF8&qid=1487890352&sr=8-20&keywords=gluten+free+snacks&refinements=p_72:2661618011&th=1&linkCode=li3&tag=bestvoted-20&linkId=e8effe852275424ccc8c15d5a4adda7c",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007PE7ANY&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B007PE7ANY",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007PE7ANY&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B007PE7ANY&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item3WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `A single bar can be used as a meal replacement`,
        },
        {
            "id": "2",
            "positiveFeature": `Can be used to calm hunger pains in between meals`,
        },
        {
            "id": "3",
            "positiveFeature": `Great bar for those who follow a vegan lifestyle`,
        },
        {
            "id": "4",
            "positiveFeature": `Contains less sugar than typical candy bars`,
        },
        {
            "id": "5",
            "positiveFeature": `Ingredients include dark chocolate which is an anti-oxidant`,
        },
        {
            "id": "6",
            "positiveFeature": `Each bar provides nutritional benefits as well as fiber`,
        },
        {
            "id": "7",
            "positiveFeature": `Manufacturer is a member of the RSPO and only uses sustainable palm oil`,
        },
        {
            "id": "8",
            "positiveFeature": `Non-GMO`,
        },
        {
            "id": "9",
            "positiveFeature": `Dark chocolate, salty and sweet make a great combination of flavor`,
        },
    ],
    item3WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Bars can melt in hot environment as they contain chocolate`,
        },
    ],

    item4Title = "Pamela's Products Gluten Free Whenever Bars, Oat Chocolate Chip Coconut",
    item4Description = "These oat and chocolate chip coconut bars are a great gluten free snack! Not only are they gluten free, each one contains all natural ingredients, is non dairy, and is wheat free. These bars are made with rich, dark European chocolate and are lightly sweetend with all natural agave and coconut sugar. They contain fiber from nutritious chia seeds and oats. These bars can calm a hungry stomach in between meals for the health conscience person and for those who simply love great flavored snacks.",
    href4 = "https://www.amazon.com/Pamelas-Products-Whenever-Chocolate-7-05-Ounce/dp/B00503DP0O/ref=as_li_ss_il?ie=UTF8&qid=1487890352&sr=8-17&keywords=gluten+free+snacks&refinements=p_72:2661618011&th=1&linkCode=li3&tag=bestvoted-20&linkId=951b3bbd2115658154e8d6ef5d028cbd",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00503DP0O&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00503DP0O",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00503DP0O&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00503DP0O&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item4WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `non-GMO bar`,
        },
        {
            "id": "2",
            "positiveFeature": `Exceptional taste`,
        },
        {
            "id": "3",
            "positiveFeature": `Nutritious snack`,
        },
        {
            "id": "4",
            "positiveFeature": `Lighter on calories due to small size but still filling enough to calm stomach pains`,
        },
    ],
    item4WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `May require multiple bars to fill up between meals due to smaller size`,
        },
    ],

    item5Title = "Amrita Paleo Variety Pack of Energy High Protein Bars - Gluten-Free",
    item5Description = "This variety pack of gluten free energy snack bars is a plant based, high protein, certified non-GMO, superfood. It comes in chocolate maca, apricot strawberry, apple cinnamon, mango coconut, cranberry raisin, and pineapple chia. Each bar comes individually wrapped which makes it incredibly convenient to bring along anywhere that you may get hungry. Because these are energy bars, they can be eaten prior to working out or even used for recovery after working out. These bars are a source of great tasting fuel for athletes. For those with food allergies, this bar is also dairy free and soy free. The bars are naturally sweetened with organic paste made from dates and depending on the flavor they may contain dried fruits. These gluten free snacks are tasty and moist and are easy to digest. They make a great snack for those who are concerned with having a healthy lifestyle.",
    href5 = "https://www.amazon.com/Paleo-Variety-Pack-Energy-Protein/dp/B00MV3IO7U/ref=as_li_ss_il?ie=UTF8&qid=1487890352&sr=8-25&keywords=gluten+free+snacks&refinements=p_72:2661618011&linkCode=li3&tag=bestvoted-20&linkId=3c655929cebe49d19d43d8fc160d02c3",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00MV3IO7U&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00MV3IO7U",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00MV3IO7U&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00MV3IO7U&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Variety pack`,
        },
        {
            "id": "2",
            "positiveFeature": `Used by some athletes for energy and fuel`,
        },
        {
            "id": "3",
            "positiveFeature": `Soft and moist texture`,
        },
        {
            "id": "5",
            "positiveFeature": `No artificial sweeteners, only natural sweeteners`,
        },
        {
            "id": "6",
            "positiveFeature": `Healthy snack in between meals`,
        },
        {
            "id": "7",
            "positiveFeature": `Easily digestible`,
        },
    ],
    item5WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Advertised as free from peanuts on label but processed in facility that processes tree nuts and peanuts`,
        },
    ],

    item6Title = "LAIKI Red Rice Crackers Gluten Free Snack",
    item6Description = "These gluten free snack crackers come in a pack of 12. These ingredient simple crackers are made with rice, oil and salt. That's it! The red rice comes from farms in Thailand. Each bag is a great snack at only 100 calories. The crackers are made from 100% wholegrain red rice. They are a light and delicious snack and can be eaten by themselves or you can use them to scoop up dips, eaten with spread, or even used as a condiment for soups and salads. These crackers are non-GMO, vegan, and delicious. They can be eaten as a snack or included in lunches. Although these crackers are made with palm oil, the palm oil is sources from sustainable plants in Thailand and do not harm orangutans or their natural habitat. Not only are these crackers deliciously satisfying, they are guilt free as well.",
    href6 = "https://www.amazon.com/LAIKI-Crackers-Gluten-Snack-Single/dp/B00R0O948S/ref=as_li_ss_il?ie=UTF8&qid=1487890281&sr=8-2-spons&keywords=gluten+free+snacks&refinements=p_72:2661618011&th=1&linkCode=li3&tag=bestvoted-20&linkId=62f0398d6ac6bfbca47a8d7fa62cf247",
    item6url = url + "/" + encodeURIComponent('#') + "image6",
    item6ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00R0O948S&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel6 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00R0O948S",
    item6ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00R0O948S&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item6ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00R0O948S&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item6WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Healthy alternative to other snack foods`,
        },
        {
            "id": "2",
            "positiveFeature": `Baked, not fried`,
        },
        {
            "id": "3",
            "positiveFeature": `Nice crunchy snack without being brittle or hard`,
        },
        {
            "id": "4",
            "positiveFeature": `Small bags offer easy portion control`,
        },
        {
            "id": "5",
            "positiveFeature": `Can curb hunger in between meals with one bag`,
        },
    ],
    item6WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Only comes in small bags, not available in family size`,
        },
        {
            "id": "2",
            "negativeConcern": `Baked, but still contains oil`,
        },
    ],

    item7Title = "LUNA BAR Gluten Free Snack Bar Chocolate Dipped Coconut",
    item7Description = "These are chocolate dipped coconut gluten free snacks that use up to 70% organic ingredients that can help provide energy while supplying vitamins and minerals to support women's health. Each bar is individually wrapped making it a convenient snack on the go and can be brought whenever you worry you may get hungry. The bars can help to curb an appetite between meals and is a healthy way to do so. Instead of unhealthy ingredients that do little for nutrition, these bars contain protein and fiber which is beneficial to your body. These bars are non-GMO.",
    href7 = "https://www.amazon.com/LUNA-BAR-Gluten-Chocolate-Coconut/dp/B00I669OKW/ref=as_li_ss_il?ie=UTF8&qid=1487890352&sr=8-24&keywords=gluten+free+snacks&refinements=p_72:2661618011&linkCode=li3&tag=bestvoted-20&linkId=45f2403e6093fcb0d8e00a33460a06f0",
    item7url = url + "/" + encodeURIComponent('#') + "image7",
    item7ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00I669OKW&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel7 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B00I669OKW",
    item7ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00I669OKW&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item7ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00I669OKW&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item7WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Healthy snack but can be used as candy bar replacement`,
        },
        {
            "id": "2",
            "positiveFeature": `Nutritionally geared towards women's health`,
        },
        {
            "id": "3",
            "positiveFeature": `Source of protein without odd protein after taste that some protein bars have`,
        },
        {
            "id": "4",
            "positiveFeature": `Can be used as a meal replacement`,
        },
    ],
    item7WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `New recipe contains less sugar but more fat, more calories, and more carbs`,
        },
    ],

    item8Title = "Pirates Booty Aged White Cheddar Puffs, 4 oz",
    item8Description = "These baked rice and corn puffs are blended with real aged white cheddar and are a great tasting gluten free snack that you can enjoy. These puffs are individually packaged and come in a 12 pack. These are a healthier alternative than junk food snacks for kids and adults alike and because they are individually packaged it is easy to eat a healthy amount without going overboard Matey!",
    href8 = "https://www.amazon.com/Pirates-Booty-Cheddar-4-Ounce-Packaging/dp/B000CONMBS/ref=as_li_ss_il?ie=UTF8&qid=1487975148&sr=8-1&keywords=Pirates+Booty+Aged+White+Cheddar+Puffs,+4+oz&th=1&linkCode=li3&tag=bestvoted-20&linkId=9c599311c5d651ccd7bc6858f7c99b4e",
    item8url = url + "/" + encodeURIComponent('#') + "image8",
    item8ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000CONMBS&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel8 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B000CONMBS",
    item8ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000CONMBS&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item8ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B000CONMBS&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item8WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Delicious gluten free snack`,
        },
        {
            "id": "2",
            "positiveFeature": `Easy to maintain portion control due to individual size bags`,
        },
        {
            "id": "3",
            "positiveFeature": `Does not contain MSG`,
        },
    ],
    item8WhatPeopleDislike = [
        {
            "id": "1",
            "negativeConcern": `Open packages tend to go stale quickly once exposed to air`,
        },
        {
            "id": "2",
            "negativeConcern": `Easy to overeat if ordering a large bag opposed to individual size packages`,
        },
    ],

    item9Title = "Annie's Organic Bunny Gluten Free Fruit Snacks, Variety Pack",
    item9Description = "These gluten free snacks come in a variety pack which includes Berry Patch, Summer Strawberry, Sunny Citrus, and Tropical Treat flavors. There are 24 packages in a box. All of these flavors are certified organic, and health conscience people can eat guilt free knowing that these contain no artificial flavors, preservatives, high fructose corn syrup or any synthetic colors. The colors of the fruit snacks are made using plant dyes instead. These snacks are great for kids and adults alike. Kids will love that they are bite sized for them, and adults will love the convenience of eating portion controlled healthy snacks that are tasty. All of these gluten free snacks in the variety pack contains real fruit juice and offers 100% of the daily requirement for vitamin C.",
    href9 = "https://www.amazon.com/Annies-Organic-Snacks-Variety-Pouches/dp/B003V5V5J6/ref=as_li_ss_il?ie=UTF8&qid=1487890281&sr=8-8&keywords=gluten+free+snacks&refinements=p_72:2661618011&th=1&linkCode=li3&tag=bestvoted-20&linkId=c5accdd9ee0a22e6291c606c6f556d43",
    item9url = url + "/" + encodeURIComponent('#') + "image9",
    item9ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003V5V5J6&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel9 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B003V5V5J6",
    item9ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003V5V5J6&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item9ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B003V5V5J6&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item9WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `A healthy snack that children and adults like`,
        },
        {
            "id": "2",
            "positiveFeature": `Certified Organic`,
        },
        {
            "id": "3",
            "positiveFeature": `Convenient individual packages are portable`,
        },
        {
            "id": "4",
            "positiveFeature": `Gelatin free`,
        },
        {
            "id": "5",
            "positiveFeature": `Variety pack`,
        },
    ],
    item9WhatPeopleDislike = [
        {
            "id": "5",
            "negativeConcern": `Contains up to 10 grams of sugar per packet`,
        },
    ],

    item10Title = "Katz Gluten Free Powdered Donuts",
    item10Description = "Do you need a gluten free powdered donut? Where can you find one that is also dairy free, peanut free, tree nut free, soy free, and Kosher? Does gluten free snacks such as this exist? You bet they do. Look no further because Katz Gluten Free Powered Donuts offer all that and more! Like delicious powdered donut taste that is certified gluten free by the GIG and Kosher certified by the OU! Just because you may be on a gluten free diet doesn't mean you have to give up dunking a donut in your morning beverage, and it doesn't mean no more donuts for you. Discover and indulge in these fantastically delectable delights. Oh, the best part is these ones come in a 6 pack. That's up to 6 times the fun.",
    href10 = "https://www.amazon.com/Katz-Gluten-Free-Powdered-Donuts/dp/B01BCPQ1OK/ref=as_li_ss_il?ie=UTF8&qid=1487890281&sr=8-12&keywords=gluten+free+snacks&refinements=p_72:2661618011&th=1&linkCode=li3&tag=bestvoted-20&linkId=8d82615c5e60ac82f5345b5697c777ea",
    item10url = url + "/" + encodeURIComponent('#') + "image10",
    item10ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01BCPQ1OK&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    pixel10 = "https://ir-na.amazon-adsystem.com/e/ir?t=bestvoted-20&l=li3&o=1&a=B01BCPQ1OK",
    item10ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01BCPQ1OK&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item10ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B01BCPQ1OK&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=bestvoted-20",
    item10WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `A gluten free powdered donut`,
        },
        {
            "id": "2",
            "positiveFeature": `Freezes well for storing and maintains freshness well`,
        },
    ],
    item10WhatPeopleDislike = [
        {
            "id": "5",
            "negativeConcern": `If not stored properly it can become dry easily`,
        },
    ];

class PageDisplayed extends React.Component {
    constructor(props) {
        super(props);
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }

    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav extended={true} state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} item6Image={item6ImageAComp} item7Image={item7ImageAComp} item8Image={item8ImageAComp} item9Image={item9ImageAComp} item10Image={item10ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} alt6tag={item6Title} alt7tag={item7Title} alt8tag={item8Title} alt9tag={item9Title} alt10tag={item10Title} />
                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links} />
                <PageDescription date={this.date()} pageDescription={mainDescription[0]} />

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

                <Description itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />

                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                <Description itemTitle={item6Title} description={item6Description} href={href6} pixel={pixel6} source={item6ImageA} alttag={item6Title} url={item6url} place="6" sup="th" name="image6" />
                <WhatPeopleLike whatPeopleLike={item6WhatPeopleLike} />

                <Description itemTitle={item7Title} description={item7Description} href={href7} pixel={pixel7} source={item7ImageA} alttag={item7Title} url={item7url} place="7" sup="th" name="image7" />
                <WhatPeopleLike whatPeopleLike={item7WhatPeopleLike} />

                <Description itemTitle={item8Title} description={item8Description} href={href8} pixel={pixel8} source={item8ImageA} alttag={item8Title} url={item8url} place="8" sup="th" name="image8" />
                <WhatPeopleLike whatPeopleLike={item8WhatPeopleLike} />

                <Description itemTitle={item9Title} description={item9Description} href={href9} pixel={pixel9} source={item9ImageA} alttag={item9Title} url={item9url} place="9" sup="th" name="image9" />
                <WhatPeopleLike whatPeopleLike={item9WhatPeopleLike} />

                <Description itemTitle={item10Title} description={item10Description} href={href10} pixel={pixel10} source={item10ImageA} alttag={item10Title} url={item10url} place="10" sup="th" name="image10" />
                <WhatPeopleLike whatPeopleLike={item10WhatPeopleLike} />

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}


module.exports = PageDisplayed
