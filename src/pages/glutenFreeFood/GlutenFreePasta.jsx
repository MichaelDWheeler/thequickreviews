import React from "react";
import MetaTags from 'react-meta-tags';
import Title from "../../components/Title.jsx";
import PageDescription from "../../components/PageDescription.jsx";
import Description from "../../components/Description.jsx";
import Placement from "../../components/Placement.jsx";
import AmazonButton from "../../components/AmazonButton.jsx";
import ProductImages from "../../components/ProductImages.jsx";
import WhatPeopleLike from "../../components/WhatPeopleLike.jsx";
import ComparisonImages from "../../components/ComparisonImages.jsx";
import FixedHeaderNav from "../../components/FixedHeaderNav.jsx";
import ShareIcons from "../../components/ShareIcons.jsx";
import AdditionalInformation from "../../components/AdditionalInformation.jsx";
import PageNavigation from "../../components/PageNavigation.jsx";
import ItemIntroduction from "../../components/ItemIntroduction.jsx";
import JumpTo from "../../components/JumpTo.jsx";
import AdrienneWheeler from "../../components/AdrienneWheeler.jsx";
import Disqus from "../../components/Disqus.jsx";


let url = "glutenFreePasta",
    title = "Best Gluten Free Pasta",
    image =  "../../js/img/glutenFreeBackground.jpg",
    mobileImage = "../js/img/glutenFreeBackground.jpg",
    parallaxAlt = "Various gluten free nuts and grains grouped together",
    catagoryDescription = [
        <div>
            <a name="types"></a>
            <h3>The Types Of Gluten Free Pastas We Compared</h3>
            <p>When we decided to look for the best gluten free pasta, we had an issue that we had to tackle first. That issue was, what type of gluten free pasta do we compare? Do we only look for the 10 most popular pastas and then give our top picks for each type in a gluten free option? Or do we just find the ten gluten free pastas we felt were the best regardless of type and then list those with the reason we liked it? In the end, we decided to compare all of the gluten free pastas and then list the ten we liked the best regardless of the type of pasta it is. The reason we went with this choice is because there are so many different pastas that if we were only to select the top ten, we would be excluding so many more. By comparing the overall top ten gluten free pasta that we selected, it leveled the playing field and gave each type a fair chance to be ranked high even if the type of pasta was not as popular. For many of our comparisons we only list the top five products, but when a category contains a large number of variations we often list the top ten so that you can have a larger selection to choose from.</p>
        </div>
    ],
    mainDescription = [
        <div>
            <a name="features"></a>
            <h3>What Do We Feel Makes The Best Gluten Free Pasta?</h3>
            <p>In our opinion, the best gluten free pasta should be on par with the best pasta period. It should be difficult, if not impossible to tell that the dish is a gluten free dish and hard to believe even when told. We wanted a gluten free pasta that had a very similar taste, very similar texture, and similar structure as the non-gluten free pasta. This meant that we did not want a sticky, gummy, or gritty pasta. Many gluten free pastas on the market tend to fall apart because they lack the proteins that helps to give the pasta shape. Because of this, gluten free pasta makers must find an alternative to give the pasta it's form. The ingredients they use determine how the pasta maintains it's structure after being put into boiling water. Some gluten free pasta makers have found a better way to accomplish this than others, and those that have scored higher with us.</p>
        </div>
    ],
    item1Title = "Le Veneziane Gluten Free Italian Fettuce - 4 Pack",
    item1Description = "This gluten free non-GMO italian fettuccine pasta is an authentic Italian pasta made from producer Molina di Ferro. It contains 100% Italian corn from Veneto which is the corn capital of Italy. It contains no dyes and replaces the wheat with corn for those who have gluten sensitivity, is easily digestible for most people, and has a low fat content. The color of the pasta is the natural color of the B-carotene which is present in the raw materials used to make this pasta. It is Italy's #1 brand of gluten free pasta and it is our top recommendation as well.",
    href1 = "https://www.amazon.com/Venezian-Italian-Fettucee-Gluten-Free/dp/B00EBE831Y/ref=as_li_ss_il?ie=UTF8&linkCode=li3&tag=thequickreviews-20&linkId=abafd2fb324644b05516e92044e0019a",
    item1url = url + "/" + encodeURIComponent('#') + "image1",
    item1ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EBE831Y&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel1 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00EBE831Y" ,
    item1ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EBE831Y&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00EBE831Y&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item1WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Authentic Italian made gluten free pasta`,
        },
        {
            "id": "2",
            "positiveFeature": `Does not fall apart`,
        },
        {
            "id": "3",
            "positiveFeature": `Does not get sticky or gummy`,
        },
        {
            "id": "4",
            "positiveFeature": `Texture is very similar to regular pasta`,
        },
        {
            "id": "5",
            "positiveFeature": `High quality gluten free pasta`,
        },
        {
            "id": "6",
            "positiveFeature": `Low in fat, high in complex carbs`,
        },

    ],
    item2Title = "Le Veneziane Gluten Free Italian Spaghetti - 4 Pack",
    item2Description = "It should come as no surprise that our second top pick in gluten free pasta goes to the maker of our first top pick. Le Veneziane knows how to make a great gluten free pasta that is hard to distinguish from the real deal. Made from Molina di Ferro, this gluten free pasta is made with 100% Italian corn grown in Veneto which is the corn capital of Italy. As with other gluten free pastas from this maker, it contains no dyes and uses corn as a wheat alternative. The color of the pasta is a natural color derived from the B-carotene that is present in the raw ingredients. This brand is Italy's #1 brand of gluten free pasta. This pasta is highly digestible and has a low fat content for those who are sensitive to gluten and are watching their fat intake.",
    href2 = "https://www.amazon.com/Veneziane-Italian-Spaghetti-Gluten-Free-Pkgs/dp/B00DUFHNOS/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368573&sr=1-15&keywords=gluten+free+pasta&refinements=p_36:698504011,p_72:1248897011&linkCode=li3&tag=thequickreviews-20&linkId=cb49c65c616f1cc0886fc320af20a4c7",
    item2url = url + "/" + encodeURIComponent('#') + "image2",
    item2ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DUFHNOS&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel2 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B00DUFHNOS",
    item2ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DUFHNOS&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00DUFHNOS&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item2WhatPeopleLike = [
        {
            "id": "6",
            "positiveFeature": `Authentic Italian made gluten free pasta with Italian grown corn`,
        },
        {
            "id": "1",
            "positiveFeature": `Difficult to tell that it is gluten free pasta`,
        },
        {
            "id": "2",
            "positiveFeature": `Great texture is similar to regular pasta`,
        },
        {
            "id": "3",
            "positiveFeature": `High quality wheat replacement`,
        },
        {
            "id": "4",
            "positiveFeature": `Does not get sticky or gummy`,
        },
        {
            "id": "5",
            "positiveFeature": `0.5 grams of fat and 45 grams of complex carbohydrates per serving`,
        },

    ],
    item3Title = "Jovial Organic Gluten Free Brown Rice Fusilli - 6 Pack",
    item3Description = "Jovial brown rice pasta is organically grown and gluten free. This pasta is a product of Italy and created using traditional methods. Each variation of rice has been carefully selected to be as close to standard pasta as gluten free pasta can be. This gluten free pasta cooks firm and works well with all types of sauces. It is one of the superior gluten free pastas on the market that is pressed and slow dried. The rice used is Italian grown and 100% organic brown rice. The facility in which this pasta is created is dedicated gluten free, dairy free, egg free, tree nut free, and peanut free. This pasta is certified to be gluten free at less than 10 PPM and certified Kosher.",
    href3 = "https://www.amazon.com/Jovial-Organic-Fusilli-12-Ounce-Packages/dp/B0041QJVC2/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-8&keywords=gluten+free+pasta&refinements=p_36:698505011&linkCode=li3&tag=thequickreviews-20&linkId=e016ef76f8d472921f0c01f1a4affd0f",
    item3url = url + "/" + encodeURIComponent('#') + "image3",
    item3ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QJVC2&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel3 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B0041QJVC2",
    item3ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QJVC2&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QJVC2&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item3WhatPeopleLike = [
        {
            "id": "3",
            "positiveFeature": `Authentic Italian gluten free pasta`,
        },
        {
            "id": "1",
            "positiveFeature": `Simple, wholesome ingredients consist of organic brown rice flour and water`,
        },
        {
            "id": "2",
            "positiveFeature": `Certified gluten free`,
        },
        {
            "id": "4",
            "positiveFeature": `Great for individuals with multiple food allergies`,
        },
        {
            "id": "5",
            "positiveFeature": `Manufacturer focused on taste and texture to deliver the most similar gluten free pasta to traditional pasta`,
        },

    ],
    item4Title = "Bionaturae Organic Gluten Free Linguine Pasta - 4 Pack",
    item4Description = `The name Bionaturae closely means "organic nature" but for the founders of the company it has a deeper meaning. For them, the name is a celebration of authentic Italian food, family, and time honored Old World tradition. This is a superior quality pasta made in a small scale production in Italy. Gluten free pasta doesn't mean that you need to sacrifice all the things you love about pasta. Bionaturae pasta still delivers the as close to traditional pasta as you have been looking for and will work fantastic with all of your favorite sauces. This pasta is certified organic, kosher and gluten free! It is made in a dedicated facility that is egg free, wheat free, milk free, peanut free, tree nut free, fish free and shellfish free.`,
    href4 = "https://www.amazon.com/Bionaturae-Linguine-Gluten-Pasta-12-Ounce/dp/B004MDWES2/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-10&keywords=gluten+free+pasta&refinements=p_36:698505011&linkCode=li3&tag=thequickreviews-20&linkId=23054cfd932db2f3712b4e9293968ff3",
    item4url = url + "/" + encodeURIComponent('#') + "image4",
    item4ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004MDWES2&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel4 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B004MDWES2",
    item4ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004MDWES2&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004MDWES2&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item4WhatPeopleLike = [
        {
            "id": "4",
            "positiveFeature": `Authentic Italian gluten free pasta`,
        },
        {
            "id": "1",
            "positiveFeature": `Great linguine pasta that is hard to tell that it is gluten free pasta after putting sauce on it`,
        },
        {
            "id": "2",
            "positiveFeature": `Has the look and very similar feel of wheat pasta.`,
        }

    ],
    item5Title = "Bionaturae Organic Gluten Free Penne Rigate Pasta - 6 Pack",
    item5Description = "Just because a pasta shares the name of another pasta does not mean they were created the same. This especially holds true for gluten free pasta. When you are searching for the best gluten free pasta you want one that most closely resembles the traditional pastas that you love. Bionaturae pastas are made in Italy with organic ingredients and time honored traditional techniques of artisans from long ago. Instead of using teflon dies as many other pasta makers do, they use bronze dies and slowly allow the pasta to dry at a low temperature. While this method of creating the pasta takes longer and is more expensive, it creates a superior quality pasta.",
    href5 = "https://www.amazon.com/bionaturae-Organic-Rigate-Gluten-12-Ounce/dp/B001E5E0YW/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-4&keywords=gluten+free+pasta&refinements=p_36:698505011&th=1&linkCode=li3&tag=thequickreviews-20&linkId=a879f4fd3e14220f9807c67b3d8cf18e",
    item5url = url + "/" + encodeURIComponent('#') + "image5",
    item5ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E5E0YW&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel5 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B001E5E0YW",
    item5ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E5E0YW&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001E5E0YW&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item5WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Authentic Italian gluten free pasta`,
        },
        {
            "id": "2",
            "positiveFeature": `The way this pasta is created allows it to closely resemble traditional wheat pasta`,
        },
        {
            "id": "3",
            "positiveFeature": `Hard to distinguish between traditional pasta when sauce has been added`,
        },
        {
            "id": "4",
            "positiveFeature": `Certified organic`,
        },

    ],
    item6Title = "Sam Mills Pasta D'Oro Gluten Free Lasagne Corte - 6 Pack",
    item6Description = "Sam Mills gluten free lasagne corte is made with 100% non-GMO corn and uses 100% natural ingredients. Not only is it gluten free, but it is also cholesterol free, wheat free, dairy free, egg free, soy free, Kosher, and low in fat. This pasta cooks in half the time which is a benefit with gluten free pastas as it helps to maintain the texture and form better. This pasta is produced in a gluten free facility and has the cleanest label in the industry measuring under 5 PPM for gluten. It is important to note that these are smaller lasagne noodles that are only a few inches long. They are not the full length lasagne slabs you may be accustomed to.",
    href6 = "https://www.amazon.com/Sam-Mills-Gluten-Lasagne-1-Pound/dp/B004TPWRPI/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368573&sr=1-9&keywords=gluten+free+pasta&refinements=p_36:698504011,p_72:1248897011&linkCode=li3&tag=thequickreviews-20&linkId=95d4c9140cff487002a8b2e31a2556e0",
    item6url = url + "/" + encodeURIComponent('#') + "image6",
    item6ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004TPWRPI&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel6 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B004TPWRPI",
    item6ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004TPWRPI&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item6ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004TPWRPI&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item6WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `100% natural ingredients that are non-GMO`,
        },
        {
            "id": "2",
            "positiveFeature": `Suitable for those sensitive to other allergens`,
        },
        {
            "id": "3",
            "positiveFeature": `Makes a great skillet lasagne as a lasagne alterative`,
        },
        {
            "id": "4",
            "positiveFeature": `Made with corn and holds shape and texture better than some other ingredients in gluten free pastas`,
        },

    ],
    item7Title = "Bionaturae Organic Gluten Free Spaghetti Pasta - 6 Pack",
    item7Description = "Bionaturae makes an authentic Italian spaghetti from a tradition and time honor method that is not only gluten free, but certified organic and Kosher. This superior gluten free spaghetti pasta is imported from Italy and made with rice, potatoes, and non-GMO soy. It is produced in a dedicated gluten free facility. It is dairy free, tree nut free, peanut free, fish free, shellfish free and egg free. As it is a dedicated facility, there is no risk of cross contamination of any of those products. The maker of this gluten free spaghetti pasta is known for producing great tasting and great textured pastas.",
    href7 = "https://www.amazon.com/bionaturae-Organic-Spaghetti-Gluten-12-Ounce/dp/B001EQ4P00/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-12&keywords=gluten+free+pasta&refinements=p_36:698505011&th=1&linkCode=li3&tag=thequickreviews-20&linkId=93736c4ae43d50561d8cfc3ce4c1bb12",
    item7url = url + "/" + encodeURIComponent('#') + "image7",
    item7ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001EQ4P00&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel7 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B001EQ4P00",
    item7ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001EQ4P00&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item7ImageANavsrc = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B001EQ4P00&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item7WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Authentic Italian gluten free spaghetti`,
        },
        {
            "id": "2",
            "positiveFeature": `Very similar taste and texture to traditional spaghetti`,
        },
        {
            "id": "4",
            "positiveFeature": `Organic and non-GMO`,
        },

    ],
    item8Title = "Jovial Organic Gluten Free Brown Rice Penne Rigate - 6 Pack",
    item8Description = "This gluten free pasta is produced in Italy using time honored traditions that allow it to be free from gluten, while also being delicious, healthy, and satisfying to the pallet. Due to the organic brown rice ingredient and the method this pasta is produced, it allows it to remain firm after cooking and have a great flavor that works well with any sauce. Instead of using silicone dies as many do, this company uses bronze dies and allows the penne rigate to slow dry. Although this process is more time consuming and expensive, they believe it to be well worth it for the superior flavor and texture. This gluten free penne rigate pasta is made with 100% Italian grown organic brown rice in a facility that is dedicated free from gluten, dairy, eggs, tree nuts as well as peanuts. This product is certified to be gluten free at under 10 parts per million.",
    href8 = "https://www.amazon.com/Jovial-Organic-Rigate-12-Ounce-Packages/dp/B0041QIPKG/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-14&keywords=gluten+free+pasta&refinements=p_36:698505011&linkCode=li3&tag=thequickreviews-20&linkId=fb68ed3907f7ef7d2c2122947aae44bd",
    item8url = url + "/" + encodeURIComponent('#') + "image8",
    item8ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QIPKG&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel8 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B0041QIPKG",
    item8ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QIPKG&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item8ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QIPKG&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item8WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Authentic Italian gluten free penne rigate`,
        },
        {
            "id": "2",
            "positiveFeature": `Makers place special emphasis on taste and texture`,
        },
        {
            "id": "3",
            "positiveFeature": `When cooked properly it does not get gummy`,
        },

    ],
    item9Title = "Tinkyada Gluten Free Vegetable Spirals - 6 Pack",
    item9Description = "These gluten free vegetable spirals are stone ground and made with brown rice, carrot powder, tomato powder, spinach powder and water. They are a great alternative to products that contain allergens and can be enjoyed by those who cannot eat wheat products, corn, gluten, casein, meat, soy, tree nut, eggs, dairy products, or peanuts. The facility this gluten free vegetable spiral is produced in is dedicated to the production of rice pasta and there is no risk of cross contamination of other grains or cereals. This product, as well as all other Tinkyada products are certified Kosher. This is a healthy pasta and the manufacturer is dedicated to creating the best quality pastas in the world that will be enjoyed by all.",
    href9 = "https://www.amazon.com/Tinkyada-Vegetable-Spirals-Gluten-12-Ounce/dp/B004727NK2/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-22&keywords=gluten+free+pasta&refinements=p_36:698505011&linkCode=li3&tag=thequickreviews-20&linkId=7324b60d1ec8e21f4e0b8618491c05ef",
    item9url = url + "/" + encodeURIComponent('#') + "image9",
    item9ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004727NK2&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel9 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B004727NK2",
    item9ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004727NK2&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item9ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B004727NK2&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item9WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Pasta contains actual vegetable ingredients and are not just colored to look like they do`,
        },
        {
            "id": "2",
            "positiveFeature": `Does not fall apart while cooking`,
        },
        {
            "id": "3",
            "positiveFeature": `Energy efficient instructions on front of package`,
        },

    ],
    item10Title = "Jovial Organic Brown Rice Caserecce - 6 Pack",
    item10Description = "This organic and gluten free brown rice caserecce is made using traditional and time honored methods. The ingredients the manufactures uses in this pasta allows it to be as close to traditional pasta while still remaning gluten free.",
    href10 = "https://www.amazon.com/Jovial-Organic-Caserecce-12-Ounce-Packages/dp/B0041QCXMC/ref=as_li_ss_il?s=grocery&ie=UTF8&qid=1490368710&sr=1-18&keywords=gluten+free+pasta&refinements=p_36:698505011&linkCode=li3&tag=thequickreviews-20&linkId=6b918f0c8af5f525a4d83a17621a3219",
    item10url = url + "/" + encodeURIComponent('#') + "image10",
    item10ImageA = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QCXMC&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    pixel10 = "https://ir-na.amazon-adsystem.com/e/ir?t=thequickreviews-20&l=li3&o=1&a=B0041QCXMC",
    item10ImageAComp = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QCXMC&Format=_SL160_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item10ImageANav = "//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B0041QCXMC&Format=_SL110_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=thequickreviews-20",
    item10WhatPeopleLike = [
        {
            "id": "1",
            "positiveFeature": `Authentic Italian gluten free caserecce`,
        },
        {
            "id": "2",
            "positiveFeature": `Organic`,
        },
        {
            "id": "3",
            "positiveFeature": `Holds texture better than many other gluten free pastas`,
        },
        {
            "id": "4",
            "positiveFeature": `Manufacturers main focus is texture and taste`,
        },
    ],
    subject = "Gluten Free Pastas",
    moreInfo = [
        <div>
            <a name="store"></a>
            <h3>How Is Gluten Free Pasta Different From Normal Pasta?</h3>
            <p>Like other gluten free products, pasta made without grains such as wheat lack the protein called gluten which allows the product to bind to itself. This means the makers of the pasta need to find alternative ingredients that will allow the pasta to have elasticity which will give it shape. Different ingredients have different results and different products need to find what works best with that particular product. In other words, what makes a great gluten free bread won't necessarily make the best gluten free pasta. The bread needs to be baked, while the pasta needs to be boiled. The different cooking methods for the two gluten free products will give very different results. When boiled, many gluten free pastas become sticky or gummy and can lump together. Since the pasta lacks gluten, it can fall apart easily as well. With traditional spaghetti, it is easy to twirl it around your fork. Even with the best gluten free pastas, this is difficult to accomplish because it will break since it does not have the same elasticity. Gluten free pastas often taste different than their counterpart. This is not to say that they all taste different, some have done very well to get a very similiar if not better taste to many people. There is no denying however that different ingredient produce different results and this can often be distinguished in the taste. Gluten free pastas also react differently than traditional pastas when reheating and may taste completely different when cold. The ability to successfully reheat the product and enjoy it depends on the ingredients that were used to replace the gluten, and your personal preference.</p>
            <a name="tips"></a>
            <h3>How To Cook Gluten Free Pasta</h3>
            <p>You can have the best gluten free pasta, but if you cook it wrong you would never know it. Below are some tips and tricks on how to cook the best gluten free pasta so the people who are eating it savor every last bite!</p>
            <ol>
                <li>Make sure to use a large pot. You are going to want to use a larger pot than is required for traditional pasta. The reason is because gluten free pastas lack gluten (obviously) which is the protein that binds the dough to itself. Alternatives have to be used in it's place. The alternatives are often "sticky" which allows the pasta to keep it's shape but also makes the pasta want to stick together when boiled. If your pot is too small, you won't give the pasta ample room to become separated and may end up with a big clumpy mess. The solution is easy, use a bigger pot. For each pound of pasta that you use, your container should be able to hold at least 6 quarts of water. If you are cooking 2 pounds of pasta, your pot needs to hold at least 12 quarts of water.</li>
                <li>Stir Often. As explained in tip #1, gluten free products often like to stick together when boiled. By using a large pot and stirring often you keep the pasta separated for long enough periods of time that it does not "fuse" to itself.</li>
                <li>Use plenty of salt in the water while cooking. Salt can really improve the flavor of even the best gluten free pastas. As the gluten free pasta contains different ingredients from traditional pasta, it may take some time getting accustomed to the new flavor that those ingredients produce. Using a good amount of salt (1 to 1.5 tablespoons of salt per pound of pasta) while cooking will not only cause the water to boil faster, it will also add to the flavor of the pasta. Once the people who are eating the gluten free pasta become accustomed to it's natural flavor, you can gradually remove the salt.</li>
                <li>Find a brand of gluten free pasta you like, and then stick with it. Each maker of gluten free pastas use their own ingredients and methods in order to make them. The variations are much different than traditional pastas and can produce wildly different flavors. As humans, we are creatures of habit and often learn to enjoy different flavors the more accustomed to them that we are. For this reason, if you find a gluten free pasta that you enjoy, stick with it. Over time, you may find that although the flavor is much different than what you were once used to, you really like the brand you have become accustomed to.</li>
                <li>Make sure your water is boiling when you are cooking your pasta. This is important! You want to get the pasta to cook as fast as possible. As it doesn't have the protein that binds it together, it uses alternative ingredients which are "sticky". Water will make that ingredient start to break down and could cause it to stick together or worse, become mush if left in too long. Using boiling water throughout the cooking cycle will help to combat that stickiness.</li>
                <li>Be aware of your altitude and adjust your cooking times accordingly. Water boils quicker at higher altitudes which means it boils at a lower temperature. If you are in a higher altitude environment you must cook the food for longer to compensate for this. Making a larger flame will not increase the temperature because unless the water is in a pressurized vessel it will not get any higher than it's boiling point. Time and temperature are what is responsible for cooking, not whether or not the water is boiling. Boiling water is only important because it indicates to us that the water is as hot as it is going to get. We want the water at it's limit when cooking gluten free pasta.</li>
                <li>For gluten free pastas other than spaghetti (won't fit without breaking), consider using an <JumpTo classes="inline-block" href="/electricPressureCookers" title="electric pressure cooker" />. Electric pressure cookers use high pressure and high temperature to cook foods quickly. With gluten free pastas you want to cook them as fast as possible to avoid them from sticking, but temperature plus time determines when the food is properly cooked. As stated in the above, boiling water won't get any hotter. An electric pressure cooker can get it hotter. If you are using an electric pressure cooker, be sure to reduce the manufacturer recommended  time by half and use low pressure. If the pasta normally takes 7 minutes or less, do not use the electric pressure cooker. Certain gluten free pastas should not be cooked in the electric pressure cooker. Ones that are very small and intended to be used in soups should not be cooked in the electric pressure cooker as they can disable the safety mechanisms of the electric pot by clogging them. Pastas that require visual inspection to determine when they are done should not be used in the electric pressure cooker either are you will not be able to look at them. For instance, certain pastas are done when they float. You would not want to use the electric pressure cooker for these types of pastas.</li>
                <li>Stir the gluten free pasta often. When you stir the pasta, you separate the noodles and don't give them time to stick and bind to one another. Stirring more frequently while cooking can keep your pasta in individual pieces.</li>
                <li>Skip the oil. Many people mistakenly believe that adding oil to the water will keep the gluten free pasta from sticking together. This is not completely true. While some of the oil may coat the pasta, the majority of it won't. As oil is not water soluable, it will float on top while cooking. After cooking, once the pasta is being drained much of that oil will finally transfer to the pasta and coat it. It will create a slippery barrier that makes it difficult for the sauce to stick to the pasta. For better results, skip the oil and stir the gluten free pasta more frequently.</li>
                <li>Sample the pasta before the manufacturers recommended time. When you are first trying a new brand, always be sure to sample the pasta prior to the manufacturer's recommended time. Times may vary depending on your altitude and other factors, and following the manufacturer's time may not be ideal for your gluten free pasta. While cooking, when it appears that it is cooked, cool a piece and then taste it. Overcooked gluten free pasta can become mushy, you want to make certain you maintain the proper texture.</li>
                <li>Save some of the cooking liquid. Prior to draining the water from your pasta, keep at least 1 cup so you can add it to the pasta before the sauce. Gluten free pasta that is not coated in oil seems to soak up the liquid from the sauces. By adding the extra liquid you can keep it from becoming too dry.</li>
                <li>Drain and serve hot. Gluten free pastas tend to taste best when hot. Depending on the brand, cold gluten free pastas can be very unpleasant (not all, but many). Instead of cold rinsing the pasta, drain it, add the extra liquid, add the sauce, and serve. </li>
                <li>When serving, use the hot sauce to reheat the pastas that have cooled.</li>
            </ol>
        </div>
    ],
    links = [
        {
            "id": 1,
            "title": "Our Top Picks",
            "tag": "#picks",
        },
        {
            "id": 2,
            "title": "The Types Of Gluten Free Pasta We Compared",
            "tag": "#types",
        },
        {
            "id": 3,
            "title": "What Do We Feel Makes The Best Gluten Free Pasta",
            "tag": "#features",
        },
        {
            "id": 4,
            "title": "How Is Gluten Free Pasta Different From Normal Pasta?",
            "tag": "#store",
            "extra": "Additional Information",
        },
        {
            "id": 5,
            "title": "How To Cook Gluten Free Pasta",
            "tag": "#tips",
        },
        {
            "id": 100,
            "title": "What Do You Think Makes The Best " + subject + "?",
            "tag": "#discussion",
            "extra": subject + " Discussion",
        },
    ];
class PageDisplayed extends React.Component {
    constructor(props) {
        super(props);
    }

    date() {
        let date = new Date(),
            locale = "en-us",
            month = date.toLocaleString(locale, { month: "long" }),
            day = date.getDate(),
            year = date.getFullYear(),
            newDate = `${month} ${day}, ${year}`;
        return newDate;
    }
    year() {
        let d = new Date,
            thisYear = d.getFullYear();
        return thisYear;
    };

    quantity() {
        if (typeof item10Title !== 'undefined') {
            return (`Let us show you our top 10 picks for the ${title} the reasons why we chose these`);
        } else {
            return (`Let us show you our top 5 picks for the ${title} the reasons why we chose these`);
        }
    };

    render() {
        return (
            <div>
                <MetaTags>
                    <title>{title}</title>
                    <meta id="meta-description" name="description" content={this.quantity()} />
                </MetaTags>
                <FixedHeaderNav extended="true" state={this.props.state} item1Image={item1ImageANav} item2Image={item2ImageANav} item3Image={item3ImageANav} item4Image={item4ImageANav} item5Image={item5ImageANav} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} />
                <Title title={title} year={this.year()} image={image} alt={parallaxAlt} mobileImage={mobileImage} />
                <ComparisonImages item1Image={item1ImageAComp} item2Image={item2ImageAComp} item3Image={item3ImageAComp} item4Image={item4ImageAComp} item5Image={item5ImageAComp} item6Image={item6ImageAComp} item7Image={item7ImageAComp} item8Image={item8ImageAComp} item9Image={item9ImageAComp} item10Image={item10ImageAComp} alt1tag={item1Title} alt2tag={item2Title} alt3tag={item3Title} alt4tag={item4Title} alt5tag={item5Title} alt6tag={item6Title} alt7tag={item7Title} alt8tag={item8Title} alt9tag={item9Title} alt10tag={item10Title} />

                <ItemIntroduction catagoryDescription={catagoryDescription[0]} />
                <PageNavigation links={links} />

                <PageDescription date={this.date()} pageDescription={mainDescription[0]} />

                <Description itemTitle={item1Title} description={item1Description} href={href1} pixel={pixel1} source={item1ImageA} alttag={item1Title} url={item1url} place="1" sup="st" name="image1" />
                <WhatPeopleLike whatPeopleLike={item1WhatPeopleLike} />

                <Description itemTitle={item2Title} description={item2Description} href={href2} pixel={pixel2} source={item2ImageA} alttag={item2Title} url={item2url} place="2" sup="nd" name="image2" />
                <WhatPeopleLike whatPeopleLike={item2WhatPeopleLike} />

                <Description itemTitle={item3Title} description={item3Description} href={href3} pixel={pixel3} source={item3ImageA} alttag={item3Title} url={item3url} place="3" sup="rd" name="image3" />
                <WhatPeopleLike whatPeopleLike={item3WhatPeopleLike} />

                <Description itemTitle={item4Title} description={item4Description} href={href4} pixel={pixel4} source={item4ImageA} alttag={item4Title} url={item4url} place="4" sup="th" name="image4" />
                <WhatPeopleLike whatPeopleLike={item4WhatPeopleLike} />

                <Description itemTitle={item5Title} description={item5Description} href={href5} pixel={pixel5} source={item5ImageA} alttag={item5Title} url={item5url} place="5" sup="th" name="image5" />
                <WhatPeopleLike whatPeopleLike={item5WhatPeopleLike} />

                <Description itemTitle={item6Title} description={item6Description} href={href6} pixel={pixel6} source={item6ImageA} alttag={item6Title} url={item6url} place="6" sup="th" name="image6" />
                <WhatPeopleLike whatPeopleLike={item6WhatPeopleLike} />

                <Description itemTitle={item7Title} description={item7Description} href={href7} pixel={pixel7} source={item7ImageA} alttag={item7Title} url={item7url} place="7" sup="th" name="image7" />
                <WhatPeopleLike whatPeopleLike={item7WhatPeopleLike} />

                <Description itemTitle={item8Title} description={item8Description} href={href8} pixel={pixel8} source={item8ImageA} alttag={item8Title} url={item8url} place="8" sup="th" name="image8" />
                <WhatPeopleLike whatPeopleLike={item8WhatPeopleLike} />

                <Description itemTitle={item9Title} description={item9Description} href={href9} pixel={pixel9} source={item9ImageA} alttag={item9Title} url={item9url} place="9" sup="th" name="image9" />
                <WhatPeopleLike whatPeopleLike={item9WhatPeopleLike} />

                <Description itemTitle={item10Title} description={item10Description} href={href10} pixel={pixel10} source={item10ImageA} alttag={item10Title} url={item10url} place="10" sup="th" name="image10" />
                <WhatPeopleLike whatPeopleLike={item10WhatPeopleLike} />

                <AdditionalInformation about={subject} moreInfo={moreInfo[0]} />
                <AdrienneWheeler />
                <Disqus url={url} id={url + "1"} subject={subject} />

            </div>
        )
    }
}

module.exports = PageDisplayed
