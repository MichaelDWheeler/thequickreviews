import React from 'react';
import { ReactBootstrap, Col, Row } from "react-bootstrap";
import Title from "../components/Title.jsx";

let title = `CONTACT US`,
image = "../js/img/bg-footer.jpg",
parallaxAlt = "Beach near Mission Viejo, CA";

class ContactUs extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div>
                <Title title={title} image={image} alt={parallaxAlt} mobileImage={image}/>
                    <div className="container">
                        <Row>
                            <Col xs={12}>
                                test
                            </Col>
                        </Row>
                    </div>
            </div>

        )
    }
}

module.exports = ContactUs;
