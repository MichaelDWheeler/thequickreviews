import React from "react";
import { ReactBootstrap, Col, Row } from "react-bootstrap";

class WhatPeopleLike extends React.Component{

    render(){
        const listItems = this.props.whatPeopleLike;
        const pros = listItems.map(benefit =>{
            return(
                <li key= {benefit.id}>{benefit.positiveFeature}</li>
            )
        });
        return(
            <div className="container">
                <Row>
                    <Col xs={12}>
                        <h3 className="heading-title">Why We Like This</h3>
                        <ul class="pros">
                            {pros}
                        </ul>
                        <div className="horizontal-separator"></div>
                    </Col>
                </Row>
            </div>
        )
    }
}

module.exports = WhatPeopleLike;
